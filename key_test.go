package ai

import "testing"

func TestDecodeOK(t *testing.T) {
	for raw, k := range map[string]Key{
		"SHA256E-s7217--cca2c267410955230ed716fcd448cda0f4b466e81e725bd2c4bf5ab02e938204":                          {Backend: "SHA256", HasExt: true, Size: 7217, Name: "cca2c267410955230ed716fcd448cda0f4b466e81e725bd2c4bf5ab02e938204"},
		"SHA256E-s31390--f50d7ac4c6b9031379986bc362fcefb65f1e52621ce1708d537e740fefc59cc0.mp3":                     {Backend: "SHA256", HasExt: true, Size: 31390, Name: "f50d7ac4c6b9031379986bc362fcefb65f1e52621ce1708d537e740fefc59cc0.mp3"},
		"SHA256E-s0--e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855":                             {Backend: "SHA256", HasExt: true, Size: 0, Name: "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"},
		"MD5E-s363253--e5190755e0ded033bff6b5184adc2fb2.pkl.gz":                                                    {Backend: "MD5", HasExt: true, Size: 363253, Name: "e5190755e0ded033bff6b5184adc2fb2.pkl.gz"},
		"SHA256E-s6749536256-S1000000000-C7--f76639fa11276b4045844e6110035c15e6803acc38d77847c2e4a2be1b1850ca.iso": {Backend: "SHA256", HasExt: true, Size: 6749536256, ChunkSize: 1000000000, ChunkNum: 7, Name: "f76639fa11276b4045844e6110035c15e6803acc38d77847c2e4a2be1b1850ca.iso"},
		"WORM-s781040400-m1574160315--cd%therootsofsepultura.cdr":                                                  {Backend: "WORM", Size: 781040400, Mtime: 1574160315, Name: "cd%therootsofsepultura.cdr"},
		"URL-s4--dl,43archive:SHA256E-s10240--0d-d5401228f11172416c78bab2f3f31f61":                                 {Backend: "URL", Size: 4, Name: "dl,43archive:SHA256E-s10240--0d-d5401228f11172416c78bab2f3f31f61"},
		"URL--http&c%%www.nitrc.org%frs%downloadlink.php%1637":                                                     {Backend: "URL", Name: "http&c%%www.nitrc.org%frs%downloadlink.php%1637"},
		"BLAKE2B160E-s184642714873--e802cd1a09bf66dc07959b4935116fe1fb5d39a7.mrimg":                                {Backend: "BLAKE2B160", HasExt: true, Size: 184642714873, Name: "e802cd1a09bf66dc07959b4935116fe1fb5d39a7.mrimg"},
		"SHA256-s696745984--14ea602583030b33e91ee8fde8dd76113984e9fac6598f9f609f408137c4cff2":                      {Backend: "SHA256", HasExt: false, Size: 696745984, Name: "14ea602583030b33e91ee8fde8dd76113984e9fac6598f9f609f408137c4cff2"},
	} {
		decoded, err := Decode(raw)
		if err != nil {
			t.Errorf("%s: expected to Decode without error; got %v", raw, err)
			continue
		}
		if decoded != k {
			t.Errorf("%s: expected key to be %#v; got %#v", raw, k, decoded)
		}
	}
}
