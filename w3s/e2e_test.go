//go:build e2e

package w3s

import (
	"context"
	"crypto/rand"
	"io"
	"os"
	"testing"
	"time"

	"ai.camelcase.link/internal/progress"
	"github.com/ipfs/go-cid"
)

func init() {
	if os.Getenv("W3S_TOK") == "" {
		panic("W3S_TOK unset")
	}
}

type loggingRdr struct {
	*os.File
	N int64
}

func (lr *loggingRdr) Read(p []byte) (int, error) {
	n, err := lr.File.Read(p)
	lr.N += int64(n)
	return n, err
}

func uploadRand(t *testing.T, cl *Client, tmpSz int64) cid.Cid {
	t.Helper()
	tmpFh, err := os.CreateTemp(t.TempDir(), "key")
	if err != nil {
		t.Error(err)
	}
	if _, err := io.CopyN(tmpFh, rand.Reader, tmpSz); err != nil {
		t.Error(err)
	}
	if _, err := tmpFh.Seek(0, io.SeekStart); err != nil {
		t.Error(err)
	}
	lr := loggingRdr{File: tmpFh}
	var totalRead int64
	pro := &progress.Tracker{Fn: func(b int64) { totalRead = b }, FileSize: tmpSz}
	c, err := cl.Upload(context.Background(), &lr, pro)
	if err != nil {
		t.Errorf("expected to upload without error; got %v", err)
	}
	if err := tmpFh.Close(); err != nil {
		t.Errorf("expected to close temp file; got %v", err)
	}
	_ = pro.Close()
	if lr.N != tmpSz || totalRead != tmpSz {
		t.Errorf("expected to upload %d bytes; read %d; progress read %d", tmpSz, lr.N, totalRead)
	}
	return c
}

func TestUpload(t *testing.T) {
	t.Parallel()
	cl, err := New(WithToken(os.Getenv("W3S_TOK")), WithCanPin(true))
	if err != nil {
		t.Error(err)
	}
	c := uploadRand(t, cl, 1024*20)
	if c == cid.Undef {
		return
	}
	defer cl.Unpin(context.Background(), c)
}

func TestUploadPresent(t *testing.T) {
	t.Parallel()
	cl, err := New(WithToken(os.Getenv("W3S_TOK")), WithCanPin(true))
	if err != nil {
		t.Error(err)
	}
	c := uploadRand(t, cl, 367)
	if c == cid.Undef {
		return
	}
	defer cl.Unpin(context.Background(), c)
	threeMinCtx, tmcCancel := context.WithTimeout(context.Background(), time.Duration(3)*time.Minute)
	defer tmcCancel()
	ok, err := cl.IsPresent(threeMinCtx, c)
	if err != nil {
		t.Fatalf("expected IsPresent not to error; got %v", err)
	}
	if !ok {
		t.Fatalf("expected %s to be present; it's not", c)
	}
}

func TestPinPresent(t *testing.T) {
	t.Parallel()
	cl, err := New(WithToken(os.Getenv("W3S_TOK")), WithCanPin(true))
	if err != nil {
		t.Error(err)
	}
	// CID used by https://ipfs.github.io/public-gateway-checker/
	c, _ := cid.Decode("bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe")
	if err := cl.Pin(context.Background(), c, "key"); err != nil {
		t.Fatal(err)
	}
	defer cl.Unpin(context.Background(), c)
	twoMinCtx, tmcCancel := context.WithTimeout(context.Background(), time.Duration(2)*time.Minute)
	defer tmcCancel()
	ok, err := cl.IsPresent(twoMinCtx, c)
	if err != nil {
		t.Fatalf("expected IsPresent not to error; got %v", err)
	}
	if !ok {
		t.Fatalf("expected %s to be present; it isn't", c)
	}
}

func TestPresentNegative(t *testing.T) {
	for _, cStr := range []string{
		"QmVuDCTWBhb37RKmUTpQhRiVv7JYm3PTG2Q6CfUPc3cMyu",              // Never on IPFS
		"bafkreicouidydstgdqaeb27msm4oc4e4njmsfx7feo3iys63faxhq7raqy", // Random data uploaded to w3s but then deleted
	} {
		c, err := cid.Decode(cStr)
		if err != nil {
			t.Error(err)
			continue
		}
		t.Run(cStr, func(t *testing.T) {
			// t.Parallel()
			twoMinCtx, tmcCancel := context.WithTimeout(context.Background(), time.Duration(2)*time.Minute)
			defer tmcCancel()
			cl, err := New(WithToken(os.Getenv("W3S_TOK")), WithCanPin(true))
			if err != nil {
				t.Error(err)
				return
			}
			is, err := cl.IsPresent(twoMinCtx, c)
			if err != nil {
				t.Errorf("expected IsPresent(%s) to not return an error; it returned %v", c, err)
				return
			}
			if is {
				t.Errorf("IsPresent(%s) returned true; it should be false", c)
			}
		})
	}
}
