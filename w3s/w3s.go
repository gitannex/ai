// Package w3s provides a client to interact with the web3.storage API in the context of Git Annex remotes.
//
// For a general purpose client see https://github.com/web3-storage/go-w3s-client
package w3s

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"ai.camelcase.link"
	"ai.camelcase.link/internal/progress"

	"github.com/alanshaw/go-carbites"
	bserv "github.com/ipfs/go-blockservice"
	"github.com/ipfs/go-cid"
	ds "github.com/ipfs/go-datastore"
	dssync "github.com/ipfs/go-datastore/sync"
	blockstore "github.com/ipfs/go-ipfs-blockstore"
	"github.com/ipfs/go-merkledag"
	"github.com/ipld/go-car"
	officialClient "github.com/web3-storage/go-w3s-client" // Note: we're using a fork of this via go.mod until upstream patches a bug
	"github.com/web3-storage/go-w3s-client/adder"
)

type config struct {
	ua       *http.Client
	jwt      string
	endpoint string
	canPin   bool
	tmpDir   string
}

// NewOpt is an option for the New() function.
type NewOpt func(options *config) error

// WithToken is an option for the New() function which sets the JSON Web Token to authenticate with the endpoint.
func WithToken(tok string) NewOpt {
	return func(cfg *config) error {
		cfg.jwt = tok
		return nil
	}
}

// WithHTTPClient is an option for the New() function which sets the http.Client with which to access the endpoint. By default, http.DefaultClient is used.
func WithHTTPClient(c *http.Client) NewOpt {
	return func(cfg *config) error {
		if c != nil {
			cfg.ua = c
		}
		return nil
	}
}

// DefaultEndpoint is the default base URL for the web3.storage API. To use a different endpoint see the WithEndpoint function.
const DefaultEndpoint = "https://api.web3.storage"

// WithEndpoint is an option for the New() function which sets the URL of the web3.storage endpoint. By default, DefaultEndpoint is used.
func WithEndpoint(e string) NewOpt {
	return func(cfg *config) error {
		if e != "" {
			cfg.endpoint = e
		}
		return nil
	}
}

// WithCanPin is an option for the New() function which specifies whether the client is authorised to access the web3.storage PSA API; by default, this is false.
func WithCanPin(can bool) NewOpt {
	return func(cfg *config) error {
		cfg.canPin = can
		return nil
	}
}

// WithTempDir is an option for the New() function which specifies the temporary directory Upload() will use. By default, os.TempDir() is used.
func WithTempDir(dir string) NewOpt {
	return func(cfg *config) error {
		if dir != "" {
			cfg.tmpDir = dir
		}
		return nil
	}
}

// Client is a client for the web3.storage API for use by Git Annex remotes.
type Client struct {
	config
	rp       *ai.RemotePinner
	official officialClient.Client
}

// New creates a new Client with the given options. The only mandatory option is WithToken; the others have sensible defaults.
func New(opts ...NewOpt) (*Client, error) {
	cl := Client{config: config{
		endpoint: DefaultEndpoint,

		// TODO: We always overwrite this UA; should we construct it at a higher level?
		ua: http.DefaultClient,
	}}
	for _, opt := range opts {
		if err := opt(&cl.config); err != nil {
			return nil, err
		}
	}
	if cl.jwt == "" {
		return nil, errors.New("JWT not set")
	}
	cl.rp = ai.NewRemotePinner(cl.endpoint, cl.jwt, cl.ua)
	oc, err := officialClient.NewClient(officialClient.WithToken(cl.jwt), officialClient.WithHTTPClient(cl.ua), officialClient.WithEndpoint(cl.endpoint))
	if err != nil {
		return nil, err
	}
	cl.official = oc
	return &cl, nil
}

// TODO: Use /status/$cid endpoint to determine if a CID is present; IsPinned returns false for a while after upload.

type namer interface {
	Name() string
}

// Upload stores the contents of rdr on web3.storage, returning the CID.
// TODO: rdr->fs.File?
// TODO: block until IsPinned is true?
// FIXME: progress tracking is broken because we convert to a CAR before uploading
func (cl *Client) Upload(ctx context.Context, rdr fs.File, pro *progress.Tracker) (cid.Cid, error) {
	// We can't use w.Client.Put directly because it wraps
	// single file uploads in a useless directory. I've requested
	// this behaviour be made configurable in
	// https://github.com/web3-storage/go-w3s-client/issues/20 .
	namedFile, ok := rdr.(namer)
	if !ok {
		// FIXME: Change param type, or fallback filename?
		return cid.Undef, fmt.Errorf("Upload: can't determine filename: rdr doesn't implement Name()")
	}
	var dir string
	if cl.tmpDir == "" {
		dir = os.TempDir()
	}
	tmpF, err := os.CreateTemp(dir, filepath.Base(namedFile.Name()))
	if err != nil {
		tmpF, err = os.CreateTemp(os.TempDir(), filepath.Base(namedFile.Name()))
		if err != nil {
			return cid.Undef, err
		}
	}
	defer os.Remove(tmpF.Name())
	return cl.putSingleFile(ctx, rdr, filepath.Base(namedFile.Name()), tmpF, pro)
}

type nopCloserFile struct {
	fs.File
}

func (ncf nopCloserFile) Close() error { return nil }

func (cl *Client) putSingleFile(ctx context.Context, fsFile fs.File, baseName string, tmpF *os.File, pro *progress.Tracker) (cid.Cid, error) {
	ds := dssync.MutexWrap(ds.NewMapDatastore())
	bsvc := bserv.New(blockstore.NewBlockstore(ds), nil)
	dag := merkledag.NewDAGService(bsvc)
	dagFmtr, err := adder.NewAdder(ctx, dag)
	if err != nil {
		return cid.Undef, err
	}

	_, err = dagFmtr.Add(nopCloserFile{fsFile}, "", nil)
	if err != nil {
		return cid.Undef, fmt.Errorf("putSingleFile: dagFmtr.Add: %w", err)
	}

	mr, err := dagFmtr.MfsRoot()
	if err != nil {
		return cid.Undef, err
	}
	rdir := mr.GetDirectory()
	cdir, err := rdir.Child(baseName)
	if err != nil {
		return cid.Undef, err
	}
	cnode, err := cdir.GetNode()
	if err != nil {
		return cid.Undef, err
	}
	root := cnode.Cid()
	err = car.WriteCar(ctx, dag, []cid.Cid{root}, tmpF)
	if err != nil {
		return cid.Undef, fmt.Errorf("WriteCar: %w", err)
	}
	return cl.PutCar(ctx, tmpF, pro)
}

// targetChunkSize is the maximumn size at which CARs are split. This
// value is based on the suggestion at
// https://github.com/web3-storage/web3.storage/pull/1457/commits/f125d732097f49bbc9953604cdb4deb2a573361a
const targetChunkSize = (1024 * 1024 * 9) + 512

func (cl *Client) PutCar(ctx context.Context, car *os.File, pro *progress.Tracker) (cid.Cid, error) {
	carSz, err := car.Seek(0, io.SeekEnd)
	if err != nil {
		return cid.Undef, err
	}
	pro.SetOverhead(carSz - pro.FileSize)
	if _, err = car.Seek(0, io.SeekStart); err != nil {
		return cid.Undef, err
	}
	spltr, err := carbites.NewTreewalkSplitterFromPath(car.Name(), targetChunkSize)
	if err != nil {
		return cid.Undef, fmt.Errorf("PutCar: NewTreeWalkSplitterFromPath: %w", err)
	}

	var root cid.Cid
	for {
		r, err := spltr.Next()
		if err != nil {
			if err == io.EOF {
				break
			}
			return cid.Undef, err
		}

		// TODO: concurrency
		c, err := cl.sendCar(ctx, pro.NewReader(r))
		if err != nil {
			return cid.Undef, fmt.Errorf("PutCar: sendCar: %w", err)
		}
		root = c
	}
	pro.Close()
	return root, nil
}

// TODO: retry
func (cl *Client) sendCar(ctx context.Context, r io.Reader) (cid.Cid, error) {
	req, err := http.NewRequestWithContext(ctx, "POST", cl.endpoint+"/car", r)
	if err != nil {
		return cid.Undef, err
	}
	req.Header.Add("Content-Type", "application/car")
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", cl.jwt))
	res, err := cl.ua.Do(req)
	if err != nil {
		return cid.Undef, err
	}
	if res.StatusCode != 200 {
		return cid.Undef, fmt.Errorf("unexpected response status: %d", res.StatusCode)
	}
	body, err := io.ReadAll(res.Body)
	if err != nil {
		return cid.Undef, err
	}
	var out struct {
		Cid string `json:"cid"`
	}
	if err := json.Unmarshal(body, &out); err != nil {
		return cid.Undef, err
	}
	return cid.Parse(out.Cid)
}

func (cl *Client) IsPresent(ctx context.Context, c cid.Cid) (bool, error) {
	return cl.isPresent(ctx, c, cl.isPinned, cl.isUploaded)
}

type isFn func(context.Context, cid.Cid) (bool, error)

func (cl *Client) isPresent(ctx context.Context, c cid.Cid, pinnedFn, uploadedFn isFn) (bool, error) {
	errors := make([]string, 0, 2)
	for _, isFn := range []isFn{pinnedFn, uploadedFn} {
		if is, err := isFn(ctx, c); err != nil {
			errors = append(errors, err.Error())
		} else if is {
			return true, nil
		}
	}
	if len(errors) == 0 {
		return false, nil
	}
	return false, fmt.Errorf("IsPresent(%s): %s", c, strings.Join(errors, "; "))
}

func (cl *Client) isPinned(ctx context.Context, c cid.Cid) (bool, error) {
	if !cl.canPin {
		return false, nil
	}
	is, err := cl.rp.IsPinned(ctx, c)
	if err != nil {
		return false, fmt.Errorf("isPinned: %w", err)
	}
	return is, nil
}

// isUploaded returns true if the given CID has been uploaded to w3s.

// The /user/uploads endpoint which this method calls doesn't allow
// querying by CID, so we potentially need to page through every
// upload the user has made to determine whether the given CID is
// present.

// TODO: If there is no movement on
// https://github.com/web3-storage/web3.storage/issues/1481 , we'll
// need to maintain a cache of uploaded CIDs, and maybe consume this
// list with parallel workers

// BUG: Will occassionally crash due to
// https://github.com/web3-storage/web3.storage/issues/1512 . Awaiting
// upstream fix.
func (cl *Client) isUploaded(ctx context.Context, c cid.Cid) (bool, error) {
	ui, err := cl.official.List(ctx, officialClient.WithMaxResults(1_000))
	if err != nil {
		return false, fmt.Errorf("isUploaded: %w", err)
	}
	for {
		st, err := ui.Next()
		if err != nil {
			if errors.Is(err, io.EOF) {
				return false, nil
			}
			return false, fmt.Errorf("isUploaded: %w", err)
		}
		if st.Cid.Equals(c) {
			return true, nil
		}
	}
}

func (cl *Client) Pin(ctx context.Context, c cid.Cid, key string) error {
	if !cl.canPin {
		return ai.ErrCantPin
	}
	return cl.rp.Pin(ctx, c, key)
}

func (cl *Client) Unpin(ctx context.Context, c cid.Cid) error {
	if !cl.canPin {
		return ai.ErrCantPin
	}
	err := cl.rp.Unpin(ctx, c)
	if err == nil {
		return nil
	}
	return fmt.Errorf("Unpin: %w", err)
}

func (cl *Client) Equal(other *Client) bool {
	if cl == nil || other == nil {
		return false
	}
	return cl.config == other.config
}
