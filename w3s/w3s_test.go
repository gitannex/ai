package w3s

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"ai.camelcase.link"
	"ai.camelcase.link/internal/mockannex"
	"ai.camelcase.link/internal/progress"

	"github.com/dzhu/go-git-annex-external/remote"
	"github.com/ipfs/go-cid"
)

func setupClient(t *testing.T, opts ...NewOpt) *Client {
	t.Helper()
	cl, err := New(append([]NewOpt{WithToken("jwt")}, opts...)...)
	if err != nil {
		t.Fatal(err)
	}
	return cl
}

func TestUploadRequest(t *testing.T) {
	t.Parallel()
	var req *http.Request
	var body []byte
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if req != nil {
			t.Errorf("expected only one request; also saw: %v", req)
		}
		req = r
		var err error
		body, err = io.ReadAll(r.Body)
		if err != nil {
			panic(err)
		}
		fmt.Fprint(w, `{"cid":"bafkreigoamtzilpekuolvght3fkaceorudpdrfsqkhnii7dv3coqxi6en4"}`)
	}))
	defer ts.Close()
	cl := setupClient(t, WithEndpoint(ts.URL), WithCanPin(true))
	const fixtureFn = "../testdata/upload.txt"
	fh, err := os.Open(fixtureFn)
	if err != nil {
		t.Fatal(err)
	}
	defer fh.Close()

	_, _ = cl.Upload(context.Background(), fh, &progress.Tracker{})
	if req.URL.Path != "/car" {
		t.Errorf("expected URL path to be /content/add; got %s", req.URL.Path)
	}
	if req.Method != "POST" {
		t.Errorf("expected method to be POST; got %s", req.Method)
	}
	if ct := req.Header.Get("Content-Type"); ct != "application/car" {
		t.Errorf("expected Content-Type to be application/car; got %s", ct)
	}
	fixBody, err := os.ReadFile(fixtureFn)
	if err != nil {
		t.Fatal(err)
	}
	if !bytes.Contains(body, fixBody) {
		t.Errorf("expected request body to contain uploaded file; got %s", body)
	}
}

type requestBody struct {
	R *http.Request
	B []byte
}

func TestUploadLargerRequest(t *testing.T) {
	t.Parallel()
	reqs := []requestBody{}
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		req := r
		var err error
		body, err := io.ReadAll(r.Body)
		if err != nil {
			panic(err)
		}
		reqs = append(reqs, requestBody{R: req, B: body})
		fmt.Fprint(w, `{"cid":"bafybeib45e6kcu6ymln77yjvdievmegqzuteaqqpshhur7ysipyefttgcu"}`)
	}))
	defer ts.Close()
	cl := setupClient(t, WithEndpoint(ts.URL), WithCanPin(true))
	const fixtureFn = "../testdata/JSB_BWV1047_1.xml"
	fh, err := os.Open(fixtureFn)
	if err != nil {
		t.Fatal(err)
	}
	defer fh.Close()

	_, _ = cl.Upload(context.Background(), fh, &progress.Tracker{})
	if len(reqs) != 1 {
		t.Errorf("expected only 1 CAR; saw %d requests,", len(reqs))
	}
	// TODO: Determine that the entirety of the fixture is sent in the CARs
	for i, r := range reqs {
		if r.R.URL.Path != "/car" {
			t.Errorf("request[%d]: expected URL path to be /car; got %s", i, r.R.URL.Path)
		}
		if r.R.Method != "POST" {
			t.Errorf("request[%d]: expected method to be POST; got %s", i, r.R.Method)
		}
		if ct := r.R.Header.Get("Content-Type"); ct != "application/car" {
			t.Errorf("request[%d]: expected Content-Type to be application/car; got %s", i, ct)
		}
		if !bytes.HasPrefix(r.B, []byte("\x3a\xa2eroots")) {
			t.Errorf("request[%d]: expected request body to begin with CAR header; got %s", i, r.B)
		}
	}
}

func TestUploadMultipleCARs(t *testing.T) {
	t.Skip("unimplemented")
}

func TestUploadResponseOK(t *testing.T) {
	t.Parallel()
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, `{"cid":"bafkreigoamtzilpekuolvght3fkaceorudpdrfsqkhnii7dv3coqxi6en4"}`)
	}))
	defer ts.Close()
	cl := setupClient(t, WithEndpoint(ts.URL), WithCanPin(true))
	const fixtureFn = "../testdata/upload.txt"
	fh, err := os.Open(fixtureFn)
	if err != nil {
		t.Fatal(err)
	}
	defer fh.Close()

	c, err := cl.Upload(context.Background(), fh, &progress.Tracker{})
	if err != nil {
		t.Errorf("expected to upload without error; got %v", err)
	}
	if c.String() != "bafkreigoamtzilpekuolvght3fkaceorudpdrfsqkhnii7dv3coqxi6en4" {
		t.Errorf("expected returned CID to equal 'bafkreigoamtzilpekuolvght3fkaceorudpdrfsqkhnii7dv3coqxi6en4'; got %v", c)
	}
}

func TestUploadJSONError(t *testing.T) {
	t.Parallel()
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(500)
		fmt.Fprint(w, `{"code":"HTTP_ERROR","message":"CBOR decode error: too many terminals, data makes no sense"}`)
	}))
	defer ts.Close()
	cl := setupClient(t, WithEndpoint(ts.URL), WithCanPin(true))
	const fixtureFn = "../testdata/upload.txt"
	fh, err := os.Open(fixtureFn)
	if err != nil {
		t.Fatal(err)
	}
	defer fh.Close()

	_, err = cl.Upload(context.Background(), fh, &progress.Tracker{})
	if err == nil {
		t.Errorf("expected to upload with error; got %v", err)
	}
}

// // TODO: Test Unpin when it needs to recurse
func TestUnpinOK(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafybeieup5om3bueu2lkz5goe53vzbcehzvgvecatxlwsxt6dqk6fn2dg4")
	var reqNum int
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum += 1
		}()
		switch reqNum {
		case 0:
			if r.URL.Path != "/pins" {
				t.Errorf("expected path: /pins ; got %s", r.URL.Path)
			}
			if m := map[string][]string(r.URL.Query()); len(m) != 3 {
				t.Errorf("expected two query params; got %v", m)
			}
			if got := r.URL.Query().Get("cid"); got != testCID.String() {
				t.Errorf("expected cid=%s; got cid=%s", testCID.String(), got)
			}
			if got := r.URL.Query().Get("status"); got != "failed,pinned,pinning,queued" {
				t.Errorf("expected status=failed,pinned,pinning,queued; got status=%s", got)
			}
			if got := r.URL.Query().Get("limit"); got != "1000" {
				t.Errorf("expected limit=1000; got limit=%s", got)
			}
			if r.Method != "GET" {
				t.Errorf("expected method of GET; got %s", r.Method)
			}
			w.Header().Set("Content-Type", "application/json; charset=UTF-8")
			fmt.Fprint(w, `{"count":2,"results":[{"requestid":"d3287831-a7c2-4cbf-9889-d7aaf0534960","status":"pinned","created":"2022-03-25T18:17:17.833+00:00","pin":{"cid":"bafybeieup5om3bueu2lkz5goe53vzbcehzvgvecatxlwsxt6dqk6fn2dg4","_id":"d3287831-a7c2-4cbf-9889-d7aaf0534960","sourceCid":"bafybeieup5om3bueu2lkz5goe53vzbcehzvgvecatxlwsxt6dqk6fn2dg4","contentCid":"bafybeieup5om3bueu2lkz5goe53vzbcehzvgvecatxlwsxt6dqk6fn2dg4","authKey":315378824629963300,"name":"Stuff/","meta":null,"deleted":null,"created":"2022-03-25T18:17:17.833+00:00","updated":"2022-03-25T18:17:17.833+00:00","pins":[{"status":"Pinned","updated":"2022-03-29T16:12:05.715+00:00","peerId":"12D3KooWDdzN3snjaMJEH9zuq3tjKUFpYHeSGNkiAreF6dQSbCiL","peerName":"web3-storage-am6-4","region":null},{"status":"Pinned","updated":"2022-03-29T16:12:05.715+00:00","peerId":"12D3KooWHpE5KiQTkqbn8KbU88ZxwJxYJFaqP4mp9Z9bhNPhym9V","peerName":"web3-storage-dc13-4","region":null},{"status":"Pinned","updated":"2022-03-29T16:12:05.715+00:00","peerId":"12D3KooWKhPb9tSnCqBswVfC5EPE7iSTXhbF4Ywwz2MKg5UCagbr","peerName":"web3-storage-sv15-4","region":null}]},"delegates":[]},{"requestid":"f4d83cef-b8cc-40f7-87e9-8d9da79fc7e0","status":"pinned","created":"2022-03-25T18:03:33.947+00:00","pin":{"cid":"bafybeieup5om3bueu2lkz5goe53vzbcehzvgvecatxlwsxt6dqk6fn2dg4","_id":"f4d83cef-b8cc-40f7-87e9-8d9da79fc7e0","sourceCid":"bafybeieup5om3bueu2lkz5goe53vzbcehzvgvecatxlwsxt6dqk6fn2dg4","contentCid":"bafybeieup5om3bueu2lkz5goe53vzbcehzvgvecatxlwsxt6dqk6fn2dg4","authKey":315378824629963300,"name":"Stuff/","meta":null,"deleted":null,"created":"2022-03-25T18:03:33.947+00:00","updated":"2022-03-25T18:03:33.947+00:00","pins":[{"status":"Pinned","updated":"2022-03-29T16:12:05.715+00:00","peerId":"12D3KooWDdzN3snjaMJEH9zuq3tjKUFpYHeSGNkiAreF6dQSbCiL","peerName":"web3-storage-am6-4","region":null},{"status":"Pinned","updated":"2022-03-29T16:12:05.715+00:00","peerId":"12D3KooWHpE5KiQTkqbn8KbU88ZxwJxYJFaqP4mp9Z9bhNPhym9V","peerName":"web3-storage-dc13-4","region":null},{"status":"Pinned","updated":"2022-03-29T16:12:05.715+00:00","peerId":"12D3KooWKhPb9tSnCqBswVfC5EPE7iSTXhbF4Ywwz2MKg5UCagbr","peerName":"web3-storage-sv15-4","region":null}]},"delegates":[]}]}`)
			return
		case 1:
			if r.URL.Path != "/pins/d3287831-a7c2-4cbf-9889-d7aaf0534960" {
				t.Errorf("expected path: /pins/d3287831-a7c2-4cbf-9889-d7aaf0534960 ; got %s", r.URL.Path)
			}
			if r.Method != "DELETE" {
				t.Errorf("expected method of DELETE; got %s", r.Method)
			}
			w.WriteHeader(http.StatusAccepted)
		case 2:
			if r.URL.Path != "/pins/f4d83cef-b8cc-40f7-87e9-8d9da79fc7e0" {
				t.Errorf("expected path: /pins/f4d83cef-b8cc-40f7-87e9-8d9da79fc7e0 ; got %s", r.URL.Path)
			}
			if r.Method != "DELETE" {
				t.Errorf("expected method of DELETE; got %s", r.Method)
			}
			w.WriteHeader(http.StatusAccepted)
		}
	}))
	defer ts.Close()
	cl := setupClient(t, WithEndpoint(ts.URL), WithCanPin(true))
	err := cl.Unpin(context.Background(), testCID)
	if err != nil {
		t.Errorf("expected nil error; got %v", err)
	}
	if reqNum != 3 {
		t.Errorf("expected 3 requests; got %d", reqNum)
	}
}

func TestUnpinCantPin(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafybeieup5om3bueu2lkz5goe53vzbcehzvgvecatxlwsxt6dqk6fn2dg4")
	cl := setupClient(t, WithEndpoint("https://localhost/"))
	err := cl.Unpin(context.Background(), testCID)
	if !errors.Is(err, ai.ErrCantPin) {
		t.Errorf("expected ErrCantPin error; got %v", err)
	}
}

func TestUnpinNoPins(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafkreifddvxbktiwdckx6scmot3swvy7xcvormsgij7pglxiv524zxxmhq")
	var reqNum int
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum += 1
		}()

		if r.URL.Path != "/pins" {
			t.Errorf("expected path: /pins ; got %s", r.URL.Path)
		}
		if m := map[string][]string(r.URL.Query()); len(m) != 3 {
			t.Errorf("expected two query params; got %v", m)
		}
		if got := r.URL.Query().Get("cid"); got != testCID.String() {
			t.Errorf("expected cid=%s; got cid=%s", testCID.String(), got)
		}
		if got := r.URL.Query().Get("status"); got != "failed,pinned,pinning,queued" {
			t.Errorf("expected status=failed,pinned,pinning,queued; got status=%s", got)
		}
		if got := r.URL.Query().Get("limit"); got != "1000" {
			t.Errorf("expected limit=1000; got limit=%s", got)
		}
		if r.Method != "GET" {
			t.Errorf("expected method of GET; got %s", r.Method)
		}
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		fmt.Fprint(w, `{"count":0,"results":[]}`)
	}))
	defer ts.Close()
	cl := setupClient(t, WithEndpoint(ts.URL), WithCanPin(true))
	err := cl.Unpin(context.Background(), testCID)
	if err != nil {
		t.Errorf("expected nil error; got %v", err)
	}
	if reqNum != 1 {
		t.Errorf("expected 1 request; got %d", reqNum)
	}
}

func TestUnpinListError(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafkreifddvxbktiwdckx6scmot3swvy7xcvormsgij7pglxiv524zxxmhq")
	var reqNum int
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum += 1
		}()

		if r.URL.Path != "/pins" {
			t.Errorf("expected path: /pinning/pins ; got %s", r.URL.Path)
		}
		if m := map[string][]string(r.URL.Query()); len(m) != 3 {
			t.Errorf("expected two query params; got %v", m)
		}
		if got := r.URL.Query().Get("cid"); got != testCID.String() {
			t.Errorf("expected cid=%s; got cid=%s", testCID.String(), got)
		}
		if got := r.URL.Query().Get("status"); got != "failed,pinned,pinning,queued" {
			t.Errorf("expected status=failed,pinned,pinning,queued; got status=%s", got)
		}
		if got := r.URL.Query().Get("limit"); got != "1000" {
			t.Errorf("expected limit=1000; got limit=%s", got)
		}
		if r.Method != "GET" {
			t.Errorf("expected method of GET; got %s", r.Method)
		}
		w.WriteHeader(413)
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		fmt.Fprint(w, `{
  "error": {
    "reason": "CUSTOM_ERROR_CODE_FOR_MACHINES",
    "details": "Optional explanation for humans with more details"
  }
}`)
	}))
	defer ts.Close()
	cl := setupClient(t, WithEndpoint(ts.URL), WithCanPin(true))
	err := cl.Unpin(context.Background(), testCID)
	if err == nil {
		t.Errorf("expected error; got nil")
	}
	if reqNum != 1 {
		t.Errorf("expected 1 request; got %d", reqNum)
	}
}

func TestUnpinDeleteError(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafybeieup5om3bueu2lkz5goe53vzbcehzvgvecatxlwsxt6dqk6fn2dg4")
	var reqNum int
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum += 1
		}()
		switch reqNum {
		case 0:
			if r.URL.Path != "/pins" {
				t.Errorf("expected path: /pinning/pins ; got %s", r.URL.Path)
			}
			if m := map[string][]string(r.URL.Query()); len(m) != 3 {
				t.Errorf("expected two query params; got %v", m)
			}
			if got := r.URL.Query().Get("cid"); got != testCID.String() {
				t.Errorf("expected cid=%s; got cid=%s", testCID.String(), got)
			}
			if got := r.URL.Query().Get("status"); got != "failed,pinned,pinning,queued" {
				t.Errorf("expected status=failed,pinned,pinning,queued; got status=%s", got)
			}
			if got := r.URL.Query().Get("limit"); got != "1000" {
				t.Errorf("expected limit=1000; got limit=%s", got)
			}
			if r.Method != "GET" {
				t.Errorf("expected method of GET; got %s", r.Method)
			}
			w.Header().Set("Content-Type", "application/json; charset=UTF-8")
			fmt.Fprint(w, `{"count":2,"results":[{"requestid":"d3287831-a7c2-4cbf-9889-d7aaf0534960","status":"pinned","created":"2022-03-25T18:17:17.833+00:00","pin":{"cid":"bafybeieup5om3bueu2lkz5goe53vzbcehzvgvecatxlwsxt6dqk6fn2dg4","_id":"d3287831-a7c2-4cbf-9889-d7aaf0534960","sourceCid":"bafybeieup5om3bueu2lkz5goe53vzbcehzvgvecatxlwsxt6dqk6fn2dg4","contentCid":"bafybeieup5om3bueu2lkz5goe53vzbcehzvgvecatxlwsxt6dqk6fn2dg4","authKey":315378824629963300,"name":"Stuff/","meta":null,"deleted":null,"created":"2022-03-25T18:17:17.833+00:00","updated":"2022-03-25T18:17:17.833+00:00","pins":[{"status":"Pinned","updated":"2022-03-29T16:12:05.715+00:00","peerId":"12D3KooWDdzN3snjaMJEH9zuq3tjKUFpYHeSGNkiAreF6dQSbCiL","peerName":"web3-storage-am6-4","region":null},{"status":"Pinned","updated":"2022-03-29T16:12:05.715+00:00","peerId":"12D3KooWHpE5KiQTkqbn8KbU88ZxwJxYJFaqP4mp9Z9bhNPhym9V","peerName":"web3-storage-dc13-4","region":null},{"status":"Pinned","updated":"2022-03-29T16:12:05.715+00:00","peerId":"12D3KooWKhPb9tSnCqBswVfC5EPE7iSTXhbF4Ywwz2MKg5UCagbr","peerName":"web3-storage-sv15-4","region":null}]},"delegates":[]},{"requestid":"f4d83cef-b8cc-40f7-87e9-8d9da79fc7e0","status":"pinned","created":"2022-03-25T18:03:33.947+00:00","pin":{"cid":"bafybeieup5om3bueu2lkz5goe53vzbcehzvgvecatxlwsxt6dqk6fn2dg4","_id":"f4d83cef-b8cc-40f7-87e9-8d9da79fc7e0","sourceCid":"bafybeieup5om3bueu2lkz5goe53vzbcehzvgvecatxlwsxt6dqk6fn2dg4","contentCid":"bafybeieup5om3bueu2lkz5goe53vzbcehzvgvecatxlwsxt6dqk6fn2dg4","authKey":315378824629963300,"name":"Stuff/","meta":null,"deleted":null,"created":"2022-03-25T18:03:33.947+00:00","updated":"2022-03-25T18:03:33.947+00:00","pins":[{"status":"Pinned","updated":"2022-03-29T16:12:05.715+00:00","peerId":"12D3KooWDdzN3snjaMJEH9zuq3tjKUFpYHeSGNkiAreF6dQSbCiL","peerName":"web3-storage-am6-4","region":null},{"status":"Pinned","updated":"2022-03-29T16:12:05.715+00:00","peerId":"12D3KooWHpE5KiQTkqbn8KbU88ZxwJxYJFaqP4mp9Z9bhNPhym9V","peerName":"web3-storage-dc13-4","region":null},{"status":"Pinned","updated":"2022-03-29T16:12:05.715+00:00","peerId":"12D3KooWKhPb9tSnCqBswVfC5EPE7iSTXhbF4Ywwz2MKg5UCagbr","peerName":"web3-storage-sv15-4","region":null}]},"delegates":[]}]}`)
			return
		case 1:
			if r.URL.Path != "/pins/d3287831-a7c2-4cbf-9889-d7aaf0534960" {
				t.Errorf("expected path: /pins/d3287831-a7c2-4cbf-9889-d7aaf0534960 ; got %s", r.URL.Path)
			}
			if r.Method != "DELETE" {
				t.Errorf("expected method of DELETE; got %s", r.Method)
			}
			w.WriteHeader(http.StatusBadRequest)
			w.Header().Set("Content-Type", "application/json; charset=UTF-8")
			fmt.Fprint(w, `{"reason":"BAD_REQUEST","details":"Huh?"}`)
		}
	}))
	defer ts.Close()
	cl := setupClient(t, WithEndpoint(ts.URL), WithCanPin(true))
	err := cl.Unpin(context.Background(), testCID)
	if err == nil {
		t.Errorf("expected error; got nil")
	}
	if reqNum != 2 {
		t.Errorf("expected 2 requests; got %d", reqNum)
	}
}

func TestPinCantPin(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafybeieup5om3bueu2lkz5goe53vzbcehzvgvecatxlwsxt6dqk6fn2dg4")
	cl := setupClient(t, WithEndpoint("https://localhost/"))
	err := cl.Pin(context.Background(), testCID, "bar")
	if !errors.Is(err, ai.ErrCantPin) {
		t.Errorf("expected ErrCantPin error; got %v", err)
	}
}

type pinReq struct {
	CID  string `json:"cid"`
	Name string `json:"name"`
}

func TestPinImmediatelyPinned(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafkreifddvxbktiwdckx6scmot3swvy7xcvormsgij7pglxiv524zxxmhq")
	const keyName = "SHA265E-s28276--cca2c267410955230ed716fcd448cda0f4b466e81e725bd2c4bf5ab02e938204"
	var reqNum int
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum++
		}()
		if r.URL.Path != "/pins" {
			t.Errorf("expected path: /pins ; got %s", r.URL.Path)
		}
		if r.Method != "POST" {
			t.Errorf("expected method of POST; got %s", r.Method)
		}
		body, err := io.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			t.Fatal(err)
		}
		pr := new(pinReq)
		if err := json.Unmarshal(body, pr); err != nil {
			t.Fatal(err)
		}
		if pr.CID != testCID.String() {
			t.Errorf("expected request CID field to equal %s; it is %s", testCID, pr.CID)
		}
		if pr.Name != keyName {
			t.Errorf("expected request Name field to equal %s; it is %s", keyName, pr.Name)
		}

		w.Header().Add("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusAccepted)
		fmt.Fprint(w, `{"requestid":"cd20d081-8f51-486d-a9af-636fd6d4e41d","status":"pinned","created":"2022-05-25T07:30:25.464+00:00","pin":{"cid":"`+testCID.String()+`","_id":"cd20d081-8f51-486d-a9af-636fd6d4e41d","sourceCid":"`+testCID.String()+`","contentCid":"`+testCID.String()+`","authKey":"315378824629963332","name":"`+keyName+`","origins":null,"meta":null,"deleted":null,"created":"2022-05-25T07:30:25.464+00:00","updated":"2022-05-25T07:30:25.464+00:00","pins":[{"status":"Pinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWR19qPPiZH4khepNjS3CLXiB7AbrbAD4ZcDjN1UjGUNE1","peerName":"web3-storage-sv15","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWSnniGsyAF663gvHdqhyfJMCjWJv54cGSzcPiEMAfanvU","peerName":"web3-storage-dc13","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWPySxxWQjBgX9Jp6uAHQfVmdq8HG1gVvS1fRawHNSrmqW","peerName":"web3-storage-am6","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWEDMw7oRqQkdCJbyeqS5mUmWGwTp8JJ2tjCzTkHboF6wK","peerName":"web3-storage-sv15-2","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWNuoVEfVLJvU3jWY2zLYjGUaathsecwT19jhByjnbQvkj","peerName":"web3-storage-am6-2","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWKytRAd2ujxhGzaLHKJuje8sVrHXvjGNvHXovpar5KaKQ","peerName":"web3-storage-dc13-2","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWJEfH2MB4RsUoaJPogDPRWbFTi8iehsxsqrQpiJwFNDrP","peerName":"web3-storage-dc13-3","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWQYBPcvxFnnWzPGEx6JuBnrbF1FZq4jTahczuG2teEk1m","peerName":"web3-storage-am6-3","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWDdzN3snjaMJEH9zuq3tjKUFpYHeSGNkiAreF6dQSbCiL","peerName":"web3-storage-am6-4","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWEzCun34s9qpYEnKkG6epx2Ts9oVGRGnzCvM2s2edioLA","peerName":"web3-storage-am6-5","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWHpE5KiQTkqbn8KbU88ZxwJxYJFaqP4mp9Z9bhNPhym9V","peerName":"web3-storage-dc13-4","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWBHvsSSKHeragACma3HUodK5FcPUpXccLu2vHooNsDf9k","peerName":"web3-storage-dc13-5","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWAdxvJCV5KXZ6zveTJmnYGrSzAKuLUKZYkZssLk7UKv4i","peerName":"web3-storage-sv15-5","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWKhPb9tSnCqBswVfC5EPE7iSTXhbF4Ywwz2MKg5UCagbr","peerName":"web3-storage-sv15-4","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWQE3CWA3MJ1YhrYNP8EE3JErGbrCtpKRkFrWgi45nYAMn","peerName":"web3-storage-am6-6","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWSafoW6yrSL7waghFAaiCqGy5mdjpQx4jn4CRNqbG7eqG","peerName":"web3-storage-am6-8","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWDRak1XzURGh9MvGR4EWaP9kcbmdoagAcGMcNxBXXLzTF","peerName":"web3-storage-dc13-8","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWSH5uLrYe7XSFpmnQj1NCsoiGeKSRCV7T5xijpX2Po2aT","peerName":"web3-storage-sv15-8","region":null}]},"delegates":[]}`)
	}))
	defer ts.Close()
	cl := setupClient(t, WithEndpoint(ts.URL), WithCanPin(true))
	err := cl.Pin(context.Background(), testCID, keyName)
	if err != nil {
		t.Errorf("expected nil error; got %v", err)
	}
	if reqNum != 1 {
		t.Errorf("expected only one request; %d were made", reqNum)
	}
}

func TestPinQueuedThenPinned(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe")
	const keyName = "SHA265E-s28276--cca2c267410955230ed716fcd448cda0f4b466e81e725bd2c4bf5ab02e938204"
	var reqNum int
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum++
		}()
		if reqNum == 0 {
			if r.URL.Path != "/pins" {
				t.Errorf("expected path: /pins ; got %s", r.URL.Path)
			}
			if r.Method != "POST" {
				t.Errorf("expected method of POST; got %s", r.Method)
			}
			body, err := io.ReadAll(r.Body)
			defer r.Body.Close()
			if err != nil {
				t.Fatal(err)
			}
			pr := new(pinReq)
			if err := json.Unmarshal(body, pr); err != nil {
				t.Fatal(err)
			}
			if pr.CID != testCID.String() {
				t.Errorf("expected request CID field to equal %s; it is %s", testCID, pr.CID)
			}
			if pr.Name != keyName {
				t.Errorf("expected request Name field to equal %s; it is %s", keyName, pr.Name)
			}

			w.Header().Add("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusAccepted)
			fmt.Fprint(w, `{"requestid":"cd20d081-8f51-486d-a9af-636fd6d4e41d","status":"queued","created":"2022-05-25T07:30:25.464+00:00","pin":{"cid":"bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe","_id":"cd20d081-8f51-486d-a9af-636fd6d4e41d","sourceCid":"bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe","contentCid":"bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe","authKey":"273636128262","name":"`+keyName+`","origins":null,"meta":null,"deleted":null,"created":"2022-05-25T07:30:25.464+00:00","updated":"2022-05-25T07:30:25.464+00:00","pins":[{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWR19qPPiZH4khepNjS3CLXiB7AbrbAD4ZcDjN1UjGUNE1","peerName":"web3-storage-sv15","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWSnniGsyAF663gvHdqhyfJMCjWJv54cGSzcPiEMAfanvU","peerName":"web3-storage-dc13","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWPySxxWQjBgX9Jp6uAHQfVmdq8HG1gVvS1fRawHNSrmqW","peerName":"web3-storage-am6","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWEDMw7oRqQkdCJbyeqS5mUmWGwTp8JJ2tjCzTkHboF6wK","peerName":"web3-storage-sv15-2","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWNuoVEfVLJvU3jWY2zLYjGUaathsecwT19jhByjnbQvkj","peerName":"web3-storage-am6-2","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWKytRAd2ujxhGzaLHKJuje8sVrHXvjGNvHXovpar5KaKQ","peerName":"web3-storage-dc13-2","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWJEfH2MB4RsUoaJPogDPRWbFTi8iehsxsqrQpiJwFNDrP","peerName":"web3-storage-dc13-3","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWQYBPcvxFnnWzPGEx6JuBnrbF1FZq4jTahczuG2teEk1m","peerName":"web3-storage-am6-3","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWDdzN3snjaMJEH9zuq3tjKUFpYHeSGNkiAreF6dQSbCiL","peerName":"web3-storage-am6-4","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWEzCun34s9qpYEnKkG6epx2Ts9oVGRGnzCvM2s2edioLA","peerName":"web3-storage-am6-5","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWHpE5KiQTkqbn8KbU88ZxwJxYJFaqP4mp9Z9bhNPhym9V","peerName":"web3-storage-dc13-4","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWBHvsSSKHeragACma3HUodK5FcPUpXccLu2vHooNsDf9k","peerName":"web3-storage-dc13-5","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWAdxvJCV5KXZ6zveTJmnYGrSzAKuLUKZYkZssLk7UKv4i","peerName":"web3-storage-sv15-5","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWKhPb9tSnCqBswVfC5EPE7iSTXhbF4Ywwz2MKg5UCagbr","peerName":"web3-storage-sv15-4","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWQE3CWA3MJ1YhrYNP8EE3JErGbrCtpKRkFrWgi45nYAMn","peerName":"web3-storage-am6-6","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWSafoW6yrSL7waghFAaiCqGy5mdjpQx4jn4CRNqbG7eqG","peerName":"web3-storage-am6-8","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWDRak1XzURGh9MvGR4EWaP9kcbmdoagAcGMcNxBXXLzTF","peerName":"web3-storage-dc13-8","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWSH5uLrYe7XSFpmnQj1NCsoiGeKSRCV7T5xijpX2Po2aT","peerName":"web3-storage-sv15-8","region":null}]},"delegates":[]}`)
		} else if reqNum < 4 {
			if r.Method != "GET" {
				t.Errorf("expected method GET for request %d; got %s", reqNum, r.Method)
			}
			if expectedPath := "/pins/cd20d081-8f51-486d-a9af-636fd6d4e41d"; r.URL.Path != expectedPath {
				t.Errorf("expected path: %s for request %d; got %s", expectedPath, reqNum, r.URL.Path)
			}
			w.Header().Add("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusOK)
			fmt.Fprint(w, `{"requestid":"cd20d081-8f51-486d-a9af-636fd6d4e41d","status":"queued","created":"2022-05-25T07:30:25.464+00:00","pin":{"cid":"bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe","_id":"cd20d081-8f51-486d-a9af-636fd6d4e41d","sourceCid":"bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe","contentCid":"bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe","authKey":"54209963332","name":"`+keyName+`","origins":null,"meta":null,"deleted":null,"created":"2022-05-25T07:30:25.464+00:00","updated":"2022-05-25T07:30:25.464+00:00","pins":[{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWR19qPPiZH4khepNjS3CLXiB7AbrbAD4ZcDjN1UjGUNE1","peerName":"web3-storage-sv15","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWSnniGsyAF663gvHdqhyfJMCjWJv54cGSzcPiEMAfanvU","peerName":"web3-storage-dc13","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWPySxxWQjBgX9Jp6uAHQfVmdq8HG1gVvS1fRawHNSrmqW","peerName":"web3-storage-am6","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWEDMw7oRqQkdCJbyeqS5mUmWGwTp8JJ2tjCzTkHboF6wK","peerName":"web3-storage-sv15-2","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWNuoVEfVLJvU3jWY2zLYjGUaathsecwT19jhByjnbQvkj","peerName":"web3-storage-am6-2","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWKytRAd2ujxhGzaLHKJuje8sVrHXvjGNvHXovpar5KaKQ","peerName":"web3-storage-dc13-2","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWJEfH2MB4RsUoaJPogDPRWbFTi8iehsxsqrQpiJwFNDrP","peerName":"web3-storage-dc13-3","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWQYBPcvxFnnWzPGEx6JuBnrbF1FZq4jTahczuG2teEk1m","peerName":"web3-storage-am6-3","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWDdzN3snjaMJEH9zuq3tjKUFpYHeSGNkiAreF6dQSbCiL","peerName":"web3-storage-am6-4","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWEzCun34s9qpYEnKkG6epx2Ts9oVGRGnzCvM2s2edioLA","peerName":"web3-storage-am6-5","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWHpE5KiQTkqbn8KbU88ZxwJxYJFaqP4mp9Z9bhNPhym9V","peerName":"web3-storage-dc13-4","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWBHvsSSKHeragACma3HUodK5FcPUpXccLu2vHooNsDf9k","peerName":"web3-storage-dc13-5","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWAdxvJCV5KXZ6zveTJmnYGrSzAKuLUKZYkZssLk7UKv4i","peerName":"web3-storage-sv15-5","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWKhPb9tSnCqBswVfC5EPE7iSTXhbF4Ywwz2MKg5UCagbr","peerName":"web3-storage-sv15-4","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWQE3CWA3MJ1YhrYNP8EE3JErGbrCtpKRkFrWgi45nYAMn","peerName":"web3-storage-am6-6","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWSafoW6yrSL7waghFAaiCqGy5mdjpQx4jn4CRNqbG7eqG","peerName":"web3-storage-am6-8","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWDRak1XzURGh9MvGR4EWaP9kcbmdoagAcGMcNxBXXLzTF","peerName":"web3-storage-dc13-8","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWSH5uLrYe7XSFpmnQj1NCsoiGeKSRCV7T5xijpX2Po2aT","peerName":"web3-storage-sv15-8","region":null}]},"delegates":[]}`)
		} else {
			if r.Method != "GET" {
				t.Errorf("expected method GET for request %d; got %s", reqNum, r.Method)
			}
			if expectedPath := "/pins/cd20d081-8f51-486d-a9af-636fd6d4e41d"; r.URL.Path != expectedPath {
				t.Errorf("expected path: %s for request %d; got %s", expectedPath, reqNum, r.URL.Path)
			}
			w.Header().Add("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusOK)
			fmt.Fprint(w, `{"requestid":"cd20d081-8f51-486d-a9af-636fd6d4e41d","status":"pinned","created":"2022-05-25T07:30:25.464+00:00","pin":{"cid":"bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe","_id":"cd20d081-8f51-486d-a9af-636fd6d4e41d","sourceCid":"bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe","contentCid":"bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe","authKey":"3497828824629963332","name":"`+keyName+`","origins":null,"meta":null,"deleted":null,"created":"2022-05-25T07:30:25.464+00:00","updated":"2022-05-25T07:30:25.464+00:00","pins":[{"status":"Remote","updated":"2022-05-25T07:58:54.990399+00:00","peerId":"12D3KooWR19qPPiZH4khepNjS3CLXiB7AbrbAD4ZcDjN1UjGUNE1","peerName":"web3-storage-sv15","region":null},{"status":"Remote","updated":"2022-05-25T07:58:54.990399+00:00","peerId":"12D3KooWSnniGsyAF663gvHdqhyfJMCjWJv54cGSzcPiEMAfanvU","peerName":"web3-storage-dc13","region":null},{"status":"Remote","updated":"2022-05-25T07:58:54.990399+00:00","peerId":"12D3KooWPySxxWQjBgX9Jp6uAHQfVmdq8HG1gVvS1fRawHNSrmqW","peerName":"web3-storage-am6","region":null},{"status":"Remote","updated":"2022-05-25T07:58:54.990399+00:00","peerId":"12D3KooWEDMw7oRqQkdCJbyeqS5mUmWGwTp8JJ2tjCzTkHboF6wK","peerName":"web3-storage-sv15-2","region":null},{"status":"Remote","updated":"2022-05-25T07:58:54.990399+00:00","peerId":"12D3KooWNuoVEfVLJvU3jWY2zLYjGUaathsecwT19jhByjnbQvkj","peerName":"web3-storage-am6-2","region":null},{"status":"Remote","updated":"2022-05-25T07:58:54.990399+00:00","peerId":"12D3KooWKytRAd2ujxhGzaLHKJuje8sVrHXvjGNvHXovpar5KaKQ","peerName":"web3-storage-dc13-2","region":null},{"status":"Remote","updated":"2022-05-25T07:58:54.990399+00:00","peerId":"12D3KooWJEfH2MB4RsUoaJPogDPRWbFTi8iehsxsqrQpiJwFNDrP","peerName":"web3-storage-dc13-3","region":null},{"status":"Remote","updated":"2022-05-25T07:58:54.990399+00:00","peerId":"12D3KooWQYBPcvxFnnWzPGEx6JuBnrbF1FZq4jTahczuG2teEk1m","peerName":"web3-storage-am6-3","region":null},{"status":"Remote","updated":"2022-05-25T07:58:54.990399+00:00","peerId":"12D3KooWDdzN3snjaMJEH9zuq3tjKUFpYHeSGNkiAreF6dQSbCiL","peerName":"web3-storage-am6-4","region":null},{"status":"Remote","updated":"2022-05-25T07:58:54.990399+00:00","peerId":"12D3KooWEzCun34s9qpYEnKkG6epx2Ts9oVGRGnzCvM2s2edioLA","peerName":"web3-storage-am6-5","region":null},{"status":"Remote","updated":"2022-05-25T07:58:54.990399+00:00","peerId":"12D3KooWHpE5KiQTkqbn8KbU88ZxwJxYJFaqP4mp9Z9bhNPhym9V","peerName":"web3-storage-dc13-4","region":null},{"status":"Remote","updated":"2022-05-25T07:58:54.990399+00:00","peerId":"12D3KooWBHvsSSKHeragACma3HUodK5FcPUpXccLu2vHooNsDf9k","peerName":"web3-storage-dc13-5","region":null},{"status":"Remote","updated":"2022-05-25T07:58:54.990399+00:00","peerId":"12D3KooWAdxvJCV5KXZ6zveTJmnYGrSzAKuLUKZYkZssLk7UKv4i","peerName":"web3-storage-sv15-5","region":null},{"status":"Remote","updated":"2022-05-25T07:58:54.990399+00:00","peerId":"12D3KooWKhPb9tSnCqBswVfC5EPE7iSTXhbF4Ywwz2MKg5UCagbr","peerName":"web3-storage-sv15-4","region":null},{"status":"Pinned","updated":"2022-05-25T07:58:54.990399+00:00","peerId":"12D3KooWQE3CWA3MJ1YhrYNP8EE3JErGbrCtpKRkFrWgi45nYAMn","peerName":"web3-storage-am6-6","region":null},{"status":"Remote","updated":"2022-05-25T07:58:54.990399+00:00","peerId":"12D3KooWSafoW6yrSL7waghFAaiCqGy5mdjpQx4jn4CRNqbG7eqG","peerName":"web3-storage-am6-8","region":null},{"status":"Pinned","updated":"2022-05-25T07:58:54.990399+00:00","peerId":"12D3KooWDRak1XzURGh9MvGR4EWaP9kcbmdoagAcGMcNxBXXLzTF","peerName":"web3-storage-dc13-8","region":null},{"status":"Pinned","updated":"2022-05-25T07:58:54.990399+00:00","peerId":"12D3KooWSH5uLrYe7XSFpmnQj1NCsoiGeKSRCV7T5xijpX2Po2aT","peerName":"web3-storage-sv15-8","region":null}]},"delegates":[]}`)

		}
	}))
	defer ts.Close()
	cl := setupClient(t, WithEndpoint(ts.URL), WithCanPin(true))
	err := cl.Pin(context.Background(), testCID, keyName)
	if err != nil {
		t.Errorf("expected nil error; got %v", err)
	}
	if reqNum != 5 {
		t.Errorf("expected 5 requests; %d were made", reqNum)
	}
}

func TestPinImmediatelyFailed(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafkreifddvxbktiwdckx6scmot3swvy7xcvormsgij7pglxiv524zxxmhq")
	const keyName = "SHA265E-s28276--cca2c267410955230ed716fcd448cda0f4b466e81e725bd2c4bf5ab02e938204"
	var reqNum int
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum++
		}()
		if r.URL.Path != "/pins" {
			t.Errorf("expected path: /pins ; got %s", r.URL.Path)
		}
		if r.Method != "POST" {
			t.Errorf("expected method of POST; got %s", r.Method)
		}
		body, err := io.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			t.Fatal(err)
		}
		pr := new(pinReq)
		if err := json.Unmarshal(body, pr); err != nil {
			t.Fatal(err)
		}
		if pr.CID != testCID.String() {
			t.Errorf("expected request CID field to equal %s; it is %s", testCID, pr.CID)
		}
		if pr.Name != keyName {
			t.Errorf("expected request Name field to equal %s; it is %s", keyName, pr.Name)
		}

		w.Header().Add("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusAccepted)
		// TODO: determine how to trigger this condition so we can check the response
		fmt.Fprint(w, `{"requestid":"cd20d081-8f51-486d-a9af-636fd6d4e41d","status":"failed","created":"2022-05-25T07:30:25.464+00:00","pin":{"cid":"`+testCID.String()+`","_id":"cd20d081-8f51-486d-a9af-636fd6d4e41d","sourceCid":"`+testCID.String()+`","contentCid":"`+testCID.String()+`","authKey":"215378824629963331","name":"`+keyName+`","origins":null,"meta":null,"deleted":null,"created":"2022-05-25T07:30:25.464+00:00","updated":"2022-05-25T07:30:25.464+00:00","pins":[]},"delegates":[]}`)
	}))
	defer ts.Close()
	cl := setupClient(t, WithEndpoint(ts.URL), WithCanPin(true))
	err := cl.Pin(context.Background(), testCID, keyName)
	if err == nil {
		t.Errorf("expected error; got nil")
	}
	if reqNum != 1 {
		t.Errorf("expected only one request; %d were made", reqNum)
	}
}

func TestPinQueuedThenFailed(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafkreifddvxbktiwdckx6scmot3swvy7xcvormsgij7pglxiv524zxxmhq")
	const keyName = "SHA265E-s28276--cca2c267410955230ed716fcd448cda0f4b466e81e725bd2c4bf5ab02e938204"
	var reqNum int
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum++
		}()
		if reqNum == 0 {
			if r.URL.Path != "/pins" {
				t.Errorf("expected path: /pinning/pins ; got %s", r.URL.Path)
			}
			if r.Method != "POST" {
				t.Errorf("expected method of POST; got %s", r.Method)
			}
			body, err := io.ReadAll(r.Body)
			defer r.Body.Close()
			if err != nil {
				t.Fatal(err)
			}
			pr := new(pinReq)
			if err := json.Unmarshal(body, pr); err != nil {
				t.Fatal(err)
			}
			if pr.CID != testCID.String() {
				t.Errorf("expected request CID field to equal %s; it is %s", testCID, pr.CID)
			}
			if pr.Name != keyName {
				t.Errorf("expected request Name field to equal %s; it is %s", keyName, pr.Name)
			}

			w.Header().Add("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusAccepted)
			fmt.Fprint(w, `{"requestid":"cd20d081-8f51-486d-a9af-636fd6d4e41d","status":"queued","created":"2022-05-25T07:30:25.464+00:00","pin":{"cid":"`+testCID.String()+`","_id":"cd20d081-8f51-486d-a9af-636fd6d4e41d","sourceCid":"`+testCID.String()+`","contentCid":"`+testCID.String()+`","authKey":"272636128242","name":"`+keyName+`","origins":null,"meta":null,"deleted":null,"created":"2022-05-25T07:30:25.464+00:00","updated":"2022-05-25T07:30:25.464+00:00","pins":[{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWR19qPPiZH4khepNjS3CLXiB7AbrbAD4ZcDjN1UjGUNE1","peerName":"web3-storage-sv15","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWSnniGsyAF663gvHdqhyfJMCjWJv54cGSzcPiEMAfanvU","peerName":"web3-storage-dc13","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWPySxxWQjBgX9Jp6uAHQfVmdq8HG1gVvS1fRawHNSrmqW","peerName":"web3-storage-am6","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWEDMw7oRqQkdCJbyeqS5mUmWGwTp8JJ2tjCzTkHboF6wK","peerName":"web3-storage-sv15-2","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWNuoVEfVLJvU3jWY2zLYjGUaathsecwT19jhByjnbQvkj","peerName":"web3-storage-am6-2","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWKytRAd2ujxhGzaLHKJuje8sVrHXvjGNvHXovpar5KaKQ","peerName":"web3-storage-dc13-2","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWJEfH2MB4RsUoaJPogDPRWbFTi8iehsxsqrQpiJwFNDrP","peerName":"web3-storage-dc13-3","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWQYBPcvxFnnWzPGEx6JuBnrbF1FZq4jTahczuG2teEk1m","peerName":"web3-storage-am6-3","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWDdzN3snjaMJEH9zuq3tjKUFpYHeSGNkiAreF6dQSbCiL","peerName":"web3-storage-am6-4","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWEzCun34s9qpYEnKkG6epx2Ts9oVGRGnzCvM2s2edioLA","peerName":"web3-storage-am6-5","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWHpE5KiQTkqbn8KbU88ZxwJxYJFaqP4mp9Z9bhNPhym9V","peerName":"web3-storage-dc13-4","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWBHvsSSKHeragACma3HUodK5FcPUpXccLu2vHooNsDf9k","peerName":"web3-storage-dc13-5","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWAdxvJCV5KXZ6zveTJmnYGrSzAKuLUKZYkZssLk7UKv4i","peerName":"web3-storage-sv15-5","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWKhPb9tSnCqBswVfC5EPE7iSTXhbF4Ywwz2MKg5UCagbr","peerName":"web3-storage-sv15-4","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWQE3CWA3MJ1YhrYNP8EE3JErGbrCtpKRkFrWgi45nYAMn","peerName":"web3-storage-am6-6","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWSafoW6yrSL7waghFAaiCqGy5mdjpQx4jn4CRNqbG7eqG","peerName":"web3-storage-am6-8","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWDRak1XzURGh9MvGR4EWaP9kcbmdoagAcGMcNxBXXLzTF","peerName":"web3-storage-dc13-8","region":null},{"status":"Unpinned","updated":"2022-05-25T07:30:25.464+00:00","peerId":"12D3KooWSH5uLrYe7XSFpmnQj1NCsoiGeKSRCV7T5xijpX2Po2aT","peerName":"web3-storage-sv15-8","region":null}]},"delegates":[]}`)
		} else if reqNum < 4 {
			if r.Method != "GET" {
				t.Errorf("expected method GET for request %d; got %s", reqNum, r.Method)
			}
			if expectedPath := "/pins/cd20d081-8f51-486d-a9af-636fd6d4e41d"; r.URL.Path != expectedPath {
				t.Errorf("expected path: %s for request %d; got %s", expectedPath, reqNum, r.URL.Path)
			}
			w.Header().Add("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusOK)
			fmt.Fprint(w, `{"requestid":"cd20d081-8f51-486d-a9af-636fd6d4e41d","status":"queued","created":"2022-05-25T07:30:25.464+00:00","pin":{"cid":"`+testCID.String()+`","_id":"cd20d081-8f51-486d-a9af-636fd6d4e41d","sourceCid":"`+testCID.String()+`","contentCid":"`+testCID.String()+`","authKey":"8897822824629963332","name":"`+keyName+`","origins":null,"meta":null,"deleted":null,"created":"2022-05-25T07:30:25.464+00:00","updated":"2022-05-25T07:30:25.464+00:00","pins":[{"status":"Queued","updated":"2022-05-25T07:58:54.990399+00:00","peerId":"12D3KooWR19qPPiZH4khepNjS3CLXiB7AbrbAD4ZcDjN1UjGUNE1","peerName":"web3-storage-sv15","region":null},{"status":"Queued","updated":"2022-05-25T07:58:54.990399+00:00","peerId":"12D3KooWSnniGsyAF663gvHdqhyfJMCjWJv54cGSzcPiEMAfanvU","peerName":"web3-storage-dc13","region":null}]},"delegates":[]}`)
		} else {
			if r.Method != "GET" {
				t.Errorf("expected method GET for request %d; got %s", reqNum, r.Method)
			}
			if expectedPath := "/pins/cd20d081-8f51-486d-a9af-636fd6d4e41d"; r.URL.Path != expectedPath {
				t.Errorf("expected path: %s for request %d; got %s", expectedPath, reqNum, r.URL.Path)
			}
			w.Header().Add("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusOK)
			fmt.Fprint(w, `{"requestid":"cd20d081-8f51-486d-a9af-636fd6d4e41d","status":"failed","created":"2022-05-25T07:30:25.464+00:00","pin":{"cid":"`+testCID.String()+`","_id":"cd20d081-8f51-486d-a9af-636fd6d4e41d","sourceCid":"`+testCID.String()+`","contentCid":"`+testCID.String()+`","authKey":"8897822824629963332","name":"`+keyName+`","origins":null,"meta":null,"deleted":null,"created":"2022-05-25T07:30:25.464+00:00","updated":"2022-05-25T07:30:25.464+00:00","pins":[]},"delegates":[]}`)
		}
	}))
	defer ts.Close()
	cl := setupClient(t, WithEndpoint(ts.URL), WithCanPin(true))
	err := cl.Pin(context.Background(), testCID, keyName)
	if err == nil {
		t.Errorf("expected error; got nil")
	}
	if reqNum != 5 {
		t.Errorf("expected 5 requests; %d were made", reqNum)
	}
}

func TestPinQueuedTimeout(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafkreifddvxbktiwdckx6scmot3swvy7xcvormsgij7pglxiv524zxxmhq")
	const keyName = "SHA265E-s28276--cca2c267410955230ed716fcd448cda0f4b466e81e725bd2c4bf5ab02e938204"
	var reqNum int
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum++
		}()
		if reqNum == 0 {
			if r.URL.Path != "/pins" {
				t.Errorf("expected path: /pins ; got %s", r.URL.Path)
			}
			if r.Method != "POST" {
				t.Errorf("expected method of POST; got %s", r.Method)
			}
			body, err := io.ReadAll(r.Body)
			defer r.Body.Close()
			if err != nil {
				t.Fatal(err)
			}
			pr := new(pinReq)
			if err := json.Unmarshal(body, pr); err != nil {
				t.Fatal(err)
			}
			if pr.CID != testCID.String() {
				t.Errorf("expected request CID field to equal %s; it is %s", testCID, pr.CID)
			}
			if pr.Name != keyName {
				t.Errorf("expected request Name field to equal %s; it is %s", keyName, pr.Name)
			}

			w.Header().Add("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusAccepted)
			fmt.Fprint(w, `{"requestid":"cd20d081-8f51-486d-a9af-636fd6d4e41d","status":"queued","created":"2022-05-25T07:30:25.464+00:00","pin":{"cid":"`+testCID.String()+`","_id":"cd20d081-8f51-486d-a9af-636fd6d4e41d","sourceCid":"`+testCID.String()+`","contentCid":"`+testCID.String()+`","authKey":"8897822824629963332","name":"`+keyName+`","origins":null,"meta":null,"deleted":null,"created":"2022-05-25T07:30:25.464+00:00","updated":"2022-05-25T07:30:25.464+00:00","pins":[{"status":"Queued","updated":"2022-05-25T07:58:54.990399+00:00","peerId":"12D3KooWR19qPPiZH4khepNjS3CLXiB7AbrbAD4ZcDjN1UjGUNE1","peerName":"web3-storage-sv15","region":null},{"status":"Queued","updated":"2022-05-25T07:58:54.990399+00:00","peerId":"12D3KooWSnniGsyAF663gvHdqhyfJMCjWJv54cGSzcPiEMAfanvU","peerName":"web3-storage-dc13","region":null}]},"delegates":[]}`)
		} else {
			if r.Method != "GET" {
				t.Errorf("expected method GET for request %d; got %s", reqNum, r.Method)
			}
			if expectedPath := "/pins/cd20d081-8f51-486d-a9af-636fd6d4e41d"; r.URL.Path != expectedPath {
				t.Errorf("expected path: %s for request %d; got %s", expectedPath, reqNum, r.URL.Path)
			}
			w.Header().Add("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusOK)
			fmt.Fprint(w, `{"requestid":"cd20d081-8f51-486d-a9af-636fd6d4e41d","status":"queued","created":"2022-05-25T07:30:25.464+00:00","pin":{"cid":"`+testCID.String()+`","_id":"cd20d081-8f51-486d-a9af-636fd6d4e41d","sourceCid":"`+testCID.String()+`","contentCid":"`+testCID.String()+`","authKey":"8897822824629963332","name":"`+keyName+`","origins":null,"meta":null,"deleted":null,"created":"2022-05-25T07:30:25.464+00:00","updated":"2022-05-25T07:30:25.464+00:00","pins":[{"status":"Queued","updated":"2022-05-25T07:58:54.990399+00:00","peerId":"12D3KooWR19qPPiZH4khepNjS3CLXiB7AbrbAD4ZcDjN1UjGUNE1","peerName":"web3-storage-sv15","region":null},{"status":"Queued","updated":"2022-05-25T07:58:54.990399+00:00","peerId":"12D3KooWSnniGsyAF663gvHdqhyfJMCjWJv54cGSzcPiEMAfanvU","peerName":"web3-storage-dc13","region":null}]},"delegates":[]}`)
		}
	}))
	defer ts.Close()
	cl := setupClient(t, WithEndpoint(ts.URL), WithCanPin(true))
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(2))
	defer cancel()
	err := cl.Pin(ctx, testCID, keyName)
	if err == nil {
		t.Errorf("expected error; got nil")
	}
	if !errors.Is(err, context.DeadlineExceeded) {
		t.Errorf("expected DeadlineExceeded error; got %v", err)
	}
	if reqNum < 3 {
		t.Errorf("expected more than 3 requests; %d were made", reqNum)
	}
}

type isPresentTestCase struct {
	isPresent      bool
	expectErr      bool
	isPinned       isFn
	isUploaded     isFn
	callIsUploaded bool
}

func TestIsPresent(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafybeieup5om3bueu2lkz5goe53vzbcehzvgvecatxlwsxt6dqk6fn2dg4")
	for _, tc := range []isPresentTestCase{
		{isPresent: true, isPinned: func(context.Context, cid.Cid) (bool, error) { return true, nil }, isUploaded: func(context.Context, cid.Cid) (bool, error) { return true, nil }},
		{isPresent: true, isPinned: func(context.Context, cid.Cid) (bool, error) { return false, nil }, isUploaded: func(context.Context, cid.Cid) (bool, error) { return true, nil }, callIsUploaded: true},
		{isPresent: false, isPinned: func(context.Context, cid.Cid) (bool, error) { return false, nil }, isUploaded: func(context.Context, cid.Cid) (bool, error) { return false, nil }, callIsUploaded: true},
		{isPresent: false, expectErr: true, isPinned: func(context.Context, cid.Cid) (bool, error) { return false, errors.New("") }, isUploaded: func(context.Context, cid.Cid) (bool, error) { return false, nil }, callIsUploaded: true},
		{isPresent: false, expectErr: true, isPinned: func(context.Context, cid.Cid) (bool, error) { return false, errors.New("") }, isUploaded: func(context.Context, cid.Cid) (bool, error) { return false, errors.New("") }, callIsUploaded: true},
		{isPresent: false, expectErr: true, isPinned: func(context.Context, cid.Cid) (bool, error) { return false, nil }, isUploaded: func(context.Context, cid.Cid) (bool, error) { return false, errors.New("") }, callIsUploaded: true},
		{isPresent: true, isPinned: func(context.Context, cid.Cid) (bool, error) { return false, errors.New("") }, isUploaded: func(context.Context, cid.Cid) (bool, error) { return true, nil }, callIsUploaded: true},
		{isPresent: true, isPinned: func(context.Context, cid.Cid) (bool, error) { return true, nil }, isUploaded: func(context.Context, cid.Cid) (bool, error) { return false, errors.New("") }},
		{isPresent: true, isPinned: func(context.Context, cid.Cid) (bool, error) { return true, nil }, isUploaded: func(context.Context, cid.Cid) (bool, error) { return false, nil }},
	} {
		cl := setupClient(t)
		var uploadedCalled bool
		is, err := cl.isPresent(context.Background(), testCID, tc.isPinned, func(ctx context.Context, c cid.Cid) (bool, error) {
			uploadedCalled = true
			return tc.isUploaded(ctx, c)
		})
		if tc.expectErr {
			if err == nil {
				t.Errorf("expected an error; didn't get one")
			}
		} else if err != nil {
			t.Errorf("didn't expect an error; got %v", err)
		}
		if tc.isPresent != is {
			t.Errorf("expected is to be %t; it wasn't", tc.isPresent)
		}
		if uploadedCalled != tc.callIsUploaded {
			t.Errorf("expected uploadedCalled to be %t; it wasn't", tc.callIsUploaded)
		}

	}
}

func TestIsUploadedNot(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafybeieup5om3bueu2lkz5goe53vzbcehzvgvecatxlwsxt6dqk6fn2dg4")
	var reqNum int
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum++
		}()
		if r.URL.Path != "/user/uploads" {
			t.Errorf("expected path to be /user/uploads; got %s", r.URL)
		}
		w.WriteHeader(http.StatusOK)
		_, err := w.Write([]byte(`[
  {
    "cid": "bafkreidivzimqfqtoqxkrpge6bjyhlvxqs3rhe73owtmdulaxr5do5in7u",
    "dagSize": 132614,
    "created": "2021-03-12T17:03:07.787Z",
    "pins": [
      {
        "peerId": "12D3KooWMbibcXHwkSjgV7VZ8TMfDKi6pZvmi97P83ZwHm9LEsvV",
        "peerName": "web3-storage-dc13",
        "region": "US-DC",
        "status": "PinQueued",
        "updated": "2021-03-12T17:03:07.787Z"
      }
    ],
    "deals": [
      {
        "dealId": 138,
        "storageProvider": "f05678",
        "status": "Queued",
        "pieceCid": "bafkreidivzimqfqtoqxkrpge6bjyhlvxqs3rhe73owtmdulaxr5do5in7u",
        "dataCid": "bafkreidivzimqfqtoqxkrpge6bjyhlvxqs3rhe73owtmdulaxr5do5in7u",
        "dataModelSelector": "Links/100/Hash/Links/0/Hash/Links/0/Hash",
        "activation": "2021-03-18T11:46:50.000Z",
        "created": "2021-03-18T11:46:50.000Z",
        "updated": "2021-03-18T11:46:50.000Z"
      }
    ]
  }
]`))
		if err != nil {
			panic(err)
		}
	}))
	defer ts.Close()
	cl := setupClient(t, WithEndpoint(ts.URL))
	is, err := cl.isUploaded(context.Background(), testCID)
	if err != nil {
		t.Errorf("expected nil error; got %v", err)
	}
	if is {
		t.Errorf("expected is to be false; it's true")
	}
	if reqNum != 1 {
		t.Errorf("expected only 1 request; made %d", reqNum)
	}
}

func TestIsPinnedCantPin(t *testing.T) {
	t.Parallel()
	var reqNum int
	testCID, _ := cid.Decode("bafybeieup5om3bueu2lkz5goe53vzbcehzvgvecatxlwsxt6dqk6fn2dg4")
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum++
		}()
	}))
	defer ts.Close()
	cl := setupClient(t, WithEndpoint(ts.URL), WithCanPin(false))
	is, err := cl.isPinned(context.Background(), testCID)
	if err != nil {
		t.Errorf("expected nil error; got %v", err)
	}
	if is {
		t.Errorf("expected is to be true; got false")
	}
	if reqNum != 0 {
		t.Errorf("expected 0 requests; got %d", reqNum)
	}
}

func TestIsPinned(t *testing.T) {
	t.Parallel()
	var reqNum int
	testCID, _ := cid.Decode("bafybeieup5om3bueu2lkz5goe53vzbcehzvgvecatxlwsxt6dqk6fn2dg4")
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum++
		}()
		if r.URL.Path == "/pins" {
			w.Header().Add("Content-Type", "application/json")
			fmt.Fprintf(w, `{"count":3,"results":[{"requestid":"f18fa634-9f46-43d8-b760-2e67262acb5d","status":"pinned","created":"2022-05-29T14:35:58.091+00:00","pin":{"cid":"`+testCID.String()+`","_id":"f18fa634-9f46-43d8-b760-2e67262acb5d","sourceCid":"bafykbzacecpmpprfzxiqxp5xhhwkdnxxmw4eqrjotbtoksaudau3bgrmdu5ua","contentCid":"bafykbzacecpmpprfzxiqxp5xhhwkdnxxmw4eqrjotbtoksaudau3bgrmdu5ua","authKey":3454629966300,"name":null,"meta":null,"deleted":null,"created":"2022-05-29T14:35:58.091+00:00","updated":"2022-05-29T14:35:58.091+00:00","pins":[{"status":"Pinned","updated":"2022-06-04T02:27:19.923036+00:00","peerId":"12D3KooWQE3CWA3MJ1YhrYNP8EE3JErGbrCtpKRkFrWgi45nYAMn","peerName":"web3-storage-am6-6","region":null},{"status":"Pinned","updated":"2022-06-03T23:58:06.173492+00:00","peerId":"12D3KooWDRak1XzURGh9MvGR4EWaP9kcbmdoagAcGMcNxBXXLzTF","peerName":"web3-storage-dc13-8","region":null},{"status":"Pinned","updated":"2022-06-04T03:52:24.06024+00:00","peerId":"12D3KooWSH5uLrYe7XSFpmnQj1NCsoiGeKSRCV7T5xijpX2Po2aT","peerName":"web3-storage-sv15-8","region":null}]},"delegates":[]},{"requestid":"2e8b3ec2-018a-4c9a-bf30-bf53823eecb7","status":"pinned","created":"2022-05-27T15:42:22.59+00:00","pin":{"cid":"`+testCID.String()+`","_id":"2e8b3ec2-018a-4c9a-bf30-bf53823eecb7","sourceCid":"bafykbzaceb3qwmtpj36fzkbdgx5uyhdbt4p6gdv4hxuraitrw7wtduuupvl56","contentCid":"bafykbzaceb3qwmtpj36fzkbdgx5uyhdbt4p6gdv4hxuraitrw7wtduuupvl56","authKey":3324523302,"name":null,"meta":null,"deleted":null,"created":"2022-05-27T15:42:22.59+00:00","updated":"2022-05-27T15:42:22.59+00:00","pins":[{"status":"Pinned","updated":"2022-05-30T09:44:34.262339+00:00","peerId":"12D3KooWSafoW6yrSL7waghFAaiCqGy5mdjpQx4jn4CRNqbG7eqG","peerName":"web3-storage-am6-8","region":null},{"status":"Pinned","updated":"2022-05-30T05:53:13.826126+00:00","peerId":"12D3KooWDRak1XzURGh9MvGR4EWaP9kcbmdoagAcGMcNxBXXLzTF","peerName":"web3-storage-dc13-8","region":null},{"status":"Pinned","updated":"2022-05-30T09:44:34.262339+00:00","peerId":"12D3KooWSH5uLrYe7XSFpmnQj1NCsoiGeKSRCV7T5xijpX2Po2aT","peerName":"web3-storage-sv15-8","region":null}]},"delegates":[]},{"requestid":"3b4607fc-f0a0-4537-a90d-f03ccbd4f855","status":"pinned","created":"2022-05-27T13:52:30.01+00:00","pin":{"cid":"`+testCID.String()+`","_id":"3b4607fc-f0a0-4537-a90d-f03ccbd4f855","sourceCid":"bafkreickqogdjririk6auclpzionk6f2zyzlej5iykdgh3qcoocdp2f7ry","contentCid":"bafkreickqogdjririk6auclpzionk6f2zyzlej5iykdgh3qcoocdp2f7ry","authKey":3153184629966100,"name":null,"meta":null,"deleted":null,"created":"2022-05-27T13:52:30.01+00:00","updated":"2022-05-27T13:52:30.01+00:00","pins":[{"status":"Pinned","updated":"2022-05-27T13:59:20.654654+00:00","peerId":"12D3KooWQE3CWA3MJ1YhrYNP8EE3JErGbrCtpKRkFrWgi45nYAMn","peerName":"web3-storage-am6-6","region":null},{"status":"Pinned","updated":"2022-05-27T13:59:20.654654+00:00","peerId":"12D3KooWDRak1XzURGh9MvGR4EWaP9kcbmdoagAcGMcNxBXXLzTF","peerName":"web3-storage-dc13-8","region":null},{"status":"Pinned","updated":"2022-05-27T13:59:20.654654+00:00","peerId":"12D3KooWSH5uLrYe7XSFpmnQj1NCsoiGeKSRCV7T5xijpX2Po2aT","peerName":"web3-storage-sv15-8","region":null}]},"delegates":[]}]}`)
		}
	}))
	defer ts.Close()
	cl := setupClient(t, WithEndpoint(ts.URL), WithCanPin(true))
	is, err := cl.isPinned(context.Background(), testCID)
	if err != nil {
		t.Errorf("expected nil error; got %v", err)
	}
	if !is {
		t.Errorf("expected is to be true; got false")
	}
	if reqNum != 1 {
		t.Errorf("expected 1 requests; got %d", reqNum)
	}
}

func TestIsUploadedLinkHeader(t *testing.T) {
	t.Skip("TODO")
}

func TestIsUploaded(t *testing.T) {
	t.Parallel()
	var reqNum int
	testCID, _ := cid.Decode("bafybeieup5om3bueu2lkz5goe53vzbcehzvgvecatxlwsxt6dqk6fn2dg4")
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum++
		}()
		if r.URL.Path == "/user/uploads" {
			fmt.Fprintf(w, `[{"_id":"315318962267107131","type":"Car","name":"Upload at 2022-06-03T15:26:41.661Z","created":"2022-06-03T15:26:41.661+00:00","updated":"2022-06-03T15:26:41.661+00:00","cid":"`+testCID.String()+`","dagSize":367,"pins":[{"status":"Pinned","updated":"2022-06-03T15:51:35.491833+00:00","peerId":"12D3KooWSafoW6yrSL7waghFAaiCqGy5mdjpQx4jn4CRNqbG7eqG","peerName":"web3-storage-am6-8","region":null},{"status":"Pinned","updated":"2022-06-03T15:51:35.491833+00:00","peerId":"12D3KooWDRak1XzURGh9MvGR4EWaP9kcbmdoagAcGMcNxBXXLzTF","peerName":"web3-storage-dc13-8","region":null},{"status":"Pinned","updated":"2022-06-03T15:51:35.491833+00:00","peerId":"12D3KooWSH5uLrYe7XSFpmnQj1NCsoiGeKSRCV7T5xijpX2Po2aT","peerName":"web3-storage-sv15-8","region":null}],"deals":[{"dealId":6762653,"storageProvider":"f01199430","status":"Published","pieceCid":"baga6ea4seaqjhha67jmyw26ccqk2w56t5mmtcursw3ldllbfsd6y7mpe4md52oy","dataCid":"bafybeifjhf7isrrv6eg4h5uzbjkxhxorlhpvs4yhhr7p3bkzom7332kpoi","dataModelSelector":"Links/106/Hash/Links/1/Hash/Links/0/Hash","activation":"2022-06-05T02:54:30+00:00","expiration":"2023-10-28T02:54:30+00:00","created":"2022-06-03T19:01:23.764713+00:00","updated":"2022-06-03T19:01:23.764713+00:00"},{"dealId":6762100,"storageProvider":"f01702940","status":"Active","pieceCid":"baga6ea4seaqjhha67jmyw26ccqk2w56t5mmtcursw3ldllbfsd6y7mpe4md52oy","dataCid":"bafybeifjhf7isrrv6eg4h5uzbjkxhxorlhpvs4yhhr7p3bkzom7332kpoi","dataModelSelector":"Links/106/Hash/Links/1/Hash/Links/0/Hash","activation":"2022-06-05T02:54:30+00:00","expiration":"2023-10-28T02:54:30+00:00","created":"2022-06-03T18:41:58.465599+00:00","updated":"2022-06-04T00:10:42.946232+00:00"},{"dealId":6762421,"storageProvider":"f01832393","status":"Active","pieceCid":"baga6ea4seaqjhha67jmyw26ccqk2w56t5mmtcursw3ldllbfsd6y7mpe4md52oy","dataCid":"bafybeifjhf7isrrv6eg4h5uzbjkxhxorlhpvs4yhhr7p3bkzom7332kpoi","dataModelSelector":"Links/106/Hash/Links/1/Hash/Links/0/Hash","activation":"2022-06-05T02:55:00+00:00","expiration":"2023-10-28T02:55:00+00:00","created":"2022-06-03T19:01:29.452837+00:00","updated":"2022-06-04T02:21:28.29579+00:00"}]}]`)
		}
	}))
	defer ts.Close()
	cl := setupClient(t, WithEndpoint(ts.URL), WithCanPin(true))
	is, err := cl.isUploaded(context.Background(), testCID)
	if err != nil {
		t.Errorf("expected nil error; got %v", err)
	}
	if !is {
		t.Errorf("expected is to be true; got false")
	}
	if reqNum != 1 {
		t.Errorf("expected 1 requests; got %d", reqNum)
	}
}

func TestIsPinnedNoResults(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafkreifddvxbktiwdckx6scmot3swvy7xcvormsgij7pglxiv524zxxmhq")
	var reqNum int
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum++
		}()
		if r.URL.Path == "/pins" {
			w.Header().Add("Content-Type", "application/json")
			fmt.Fprint(w, `{
  "count": 0,
  "results": []}`)
		}
	}))
	defer ts.Close()
	cl := setupClient(t, WithEndpoint(ts.URL), WithCanPin(true))
	is, err := cl.isPinned(context.Background(), testCID)
	if err != nil {
		t.Errorf("expected nil error; got %v", err)
	}
	if is {
		t.Errorf("expected is to be false; got true")
	}
	if reqNum != 1 {
		t.Errorf("expected 2 requests; got %d", reqNum)
	}
}

func TestIsPinnedJSONError(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafybeieup5om3bueu2lkz5goe53vzbcehzvgvecatxlwsxt6dqk6fn2dg2")
	var reqNum int
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum++
		}()
		if r.URL.Path == "/pins" {
			w.WriteHeader(400)
			w.Header().Add("Content-Type", "application/json; charset=UTF-8")
			fmt.Fprint(w, `{"code":"ERROR_INVALID_CID","message":"Invalid CID: bafybeieup5om3bueu2lkz5goe53vzbcehzvgvecatxlwsxt6dqk6fn2dg2"}`)
		}
	}))
	defer ts.Close()
	cl := setupClient(t, WithEndpoint(ts.URL), WithCanPin(true))
	is, err := cl.isPinned(context.Background(), testCID)
	if err == nil {
		t.Errorf("expected error; got nil")
	}
	if is {
		t.Errorf("expected is to be false; it's true")
	}
	if reqNum != 1 {
		t.Errorf("expected to make 2 requests; made %d", reqNum)
	}
}

func TestIsUploadedJSONError(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafybeieup5om3bueu2lkz5goe53vzbcehzvgvecatxlwsxt6dqk6fn2dg2")
	var reqNum int
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum++
		}()
		if r.URL.Path == "/user/uploads" {
			w.WriteHeader(507)
			w.Write([]byte(`
{
  "name": "string",
  "message": "string"
}`))

		}
	}))
	defer ts.Close()
	cl := setupClient(t, WithEndpoint(ts.URL), WithCanPin(true))
	is, err := cl.isUploaded(context.Background(), testCID)
	if err == nil {
		t.Errorf("expected error; got nil")
	}
	if is {
		t.Errorf("expected is to be false; it's true")
	}
	if reqNum != 1 {
		t.Errorf("expected to make 2 requests; made %d", reqNum)
	}
}

type prepareFunc func(remote.Annex, *http.Client) (ai.IPFSRemote, error)

func (pf prepareFunc) Prepare(a remote.Annex, cl *http.Client) (ai.IPFSRemote, error) {
	return pf(a, cl)
}

func TestCanPinEmptyConfig(t *testing.T) {
	t.Skip("rewrite in terms of the prepare() func in main")
	return
	t.Parallel()
	ma := new(mockannex.Annex)
	ma.GetConfigFn = func(name string) string {
		if name == "canpin" {
			return ""
		}
		if name == "endpoint" {
			return "https://example.com/"
		}
		t.Errorf("invalid name for GetConfig(%s)", name)
		return ""
	}
	ma.GetCredsFn = func(name string) (string, string) {
		return "", "jwt"
	}
	ma.GetUUIDFn = func() string {
		return "blah"
	}
	ma.GetGitDirFn = func() string { return ".git" }
	cl := Client{}
	ia := ai.AI{
		I:    &cl,
		Prep: prepareFunc(func(remote.Annex, *http.Client) (ai.IPFSRemote, error) { return &cl, nil }),
	}
	if err := ia.Prepare(ma); err != nil {
		t.Errorf("expected Init to succeed; got %v", err)
	}
	if cl.canPin {
		t.Errorf("expected CanPin to be false; it is %t", cl.canPin)
	}
	if cl.endpoint != "https://example.com/" {
		t.Errorf("expected endpoint to be example.com; it is %s", cl.endpoint)
	}
}

func TestCanPinFalseConfig(t *testing.T) {
	t.Skip("rewrite in terms of the prepare() func in main")
	return
	t.Parallel()
	ma := new(mockannex.Annex)
	ma.GetConfigFn = func(name string) string {
		if name == "canpin" {
			return "false"
		}
		if name == "endpoint" {
			return "https://example.com/"
		}
		t.Errorf("invalid name for GetConfig(%s)", name)
		return ""
	}
	ma.GetCredsFn = func(name string) (string, string) {
		return "", "jwt"
	}
	ma.GetUUIDFn = func() string {
		return "blah"
	}
	ma.GetGitDirFn = func() string { return ".git" }
	cl := Client{}
	ia := ai.AI{
		I:    &cl,
		Prep: prepareFunc(func(remote.Annex, *http.Client) (ai.IPFSRemote, error) { return &cl, nil }),
	}
	if err := ia.Prepare(ma); err != nil {
		t.Errorf("expected Init to succeed; got %v", err)
	}
	if cl.canPin {
		t.Errorf("expected CanPin to be false; it is %t", cl.canPin)
	}
	if cl.endpoint != "https://example.com/" {
		t.Errorf("expected endpoint to be example.com; it is %s", cl.endpoint)
	}
}

func TestCanPinTrueConfig(t *testing.T) {
	t.Skip("rewrite in terms of the prepare() func in main")
	return
	t.Parallel()
	ma := new(mockannex.Annex)
	ma.GetConfigFn = func(name string) string {
		if name == "canpin" {
			return "true"
		}
		if name == "endpoint" {
			return "https://example.com/"
		}
		t.Errorf("invalid name for GetConfig(%s)", name)
		return ""
	}
	ma.GetCredsFn = func(name string) (string, string) {
		return "", "jwt"
	}
	ma.GetUUIDFn = func() string {
		return "blah"
	}
	ma.GetGitDirFn = func() string { return ".git" }
	cl := Client{}
	ia := ai.AI{
		I:    &cl,
		Prep: prepareFunc(func(remote.Annex, *http.Client) (ai.IPFSRemote, error) { return &cl, nil }),
	}
	if err := ia.Prepare(ma); err != nil {
		t.Errorf("expected Init to succeed; got %v", err)
	}
	if !cl.canPin {
		t.Errorf("expected CanPin to be true; it isn't")
	}
	if cl.endpoint != "https://example.com/" {
		t.Errorf("expected endpoint to be example.com; it is %s", cl.endpoint)
	}
}

func TestCanPinGarbageConfig(t *testing.T) {
	t.Skip("rewrite in terms of the prepare() func in main")
	return
	t.Parallel()
	ma := new(mockannex.Annex)
	ma.GetConfigFn = func(name string) string {
		if name == "canpin" {
			return "shs7dggajh"
		}
		if name == "endpoint" {
			return "https://example.com/"
		}
		t.Errorf("invalid name for GetConfig(%s)", name)
		return ""
	}
	ma.GetCredsFn = func(name string) (string, string) {
		return "", "jwt"
	}
	ma.GetUUIDFn = func() string {
		return "blah"
	}
	ma.GetGitDirFn = func() string { return ".git" }
	cl := Client{}
	ia := ai.AI{
		I:    &cl,
		Prep: prepareFunc(func(remote.Annex, *http.Client) (ai.IPFSRemote, error) { return &cl, nil }),
	}
	if err := ia.Prepare(ma); err == nil {
		t.Errorf("expected Init to fail")
	}
}
