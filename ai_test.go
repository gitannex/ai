package ai

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"ai.camelcase.link/internal/mockannex"
	"ai.camelcase.link/internal/progress"
	"ai.camelcase.link/internal/retrieve"
	"github.com/google/go-cmp/cmp"
	"github.com/ipfs/go-cid"
)

type mockIPFSRemote struct {
	IPFSRemote
	uploadFn  func(context.Context, fs.File, *progress.Tracker) (cid.Cid, error)
	pinFn     func(context.Context, cid.Cid, string) error
	presentFn func(context.Context, cid.Cid) (bool, error)
	unpinFn   func(context.Context, cid.Cid) error
	configsFn func() map[string]string
}

func (mir *mockIPFSRemote) Upload(ctx context.Context, fi fs.File, pro *progress.Tracker) (cid.Cid, error) {
	if mir.uploadFn != nil {
		return mir.uploadFn(ctx, fi, pro)
	}
	return cid.Undef, nil
}

func (mir *mockIPFSRemote) Pin(ctx context.Context, c cid.Cid, key string) error {
	if mir.pinFn != nil {
		return mir.pinFn(ctx, c, key)
	}
	return nil
}

func (mir *mockIPFSRemote) IsPresent(ctx context.Context, c cid.Cid) (bool, error) {
	if mir.presentFn != nil {
		return mir.presentFn(ctx, c)
	}
	return false, nil
}

func (mir *mockIPFSRemote) Unpin(ctx context.Context, c cid.Cid) error {
	if mir.unpinFn != nil {
		return mir.unpinFn(ctx, c)
	}
	return nil
}

func (mir *mockIPFSRemote) Configs() map[string]string {
	if mir.configsFn != nil {
		return mir.configsFn()
	}
	return map[string]string{}
}

func TestStoreAlreadyStorred(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled, getURLsCalled int
	expectedCID := "bafybeidyw5kttfefev2jt7bbiwrqsepzb66eklhpsglgyp7lkg3rlopzey"
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return expectedCID
	}
	ma.GetURLsFn = func(key, prefix string) []string {
		getURLsCalled++
		return []string{}
	}
	var newState string
	ma.SetStateFn = func(key, state string) {
		newState = state
	}
	var iaUploadCalled bool
	ai := AI{
		I: &mockIPFSRemote{
			uploadFn: func(context.Context, fs.File, *progress.Tracker) (cid.Cid, error) {
				iaUploadCalled = true
				return cid.Decode(expectedCID)
			},
		},
	}
	key := "SHA256E-s8783566--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	tmpDir := t.TempDir()
	tmp, err := os.CreateTemp(tmpDir, key)
	if err != nil {
		t.Error(err)
	}
	err = ai.Store(ma, key, tmp.Name())
	if err != nil {
		t.Errorf("expected Store to not return an error; got %v", err)
	}
	if getStateCalled != 1 {
		t.Errorf("expected GetState to be called once; it was called %d times", getStateCalled)
	}
	if getURLsCalled != 0 {
		t.Errorf("expected GetURLs not to be called; it was called %d times", getURLsCalled)
	}
	if !iaUploadCalled {
		t.Errorf("expected upload to be called; it wasn't")
	}
	if newState != expectedCID {
		t.Errorf("expected new state to be %s; got %s", expectedCID, newState)
	}
}

func TestStoreNoURLs(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled, getURLsCalled int
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return ""
	}
	ma.GetURLsFn = func(key, prefix string) []string {
		getURLsCalled++
		return []string{}
	}
	var newState string
	ma.SetStateFn = func(key, state string) {
		newState = state
	}
	var iaUploadCalled bool
	ai := AI{
		I: &mockIPFSRemote{
			uploadFn: func(context.Context, fs.File, *progress.Tracker) (cid.Cid, error) {
				iaUploadCalled = true
				return cid.Decode("bafybeidyw5kttfefev2jt7bbiwrqsepzb66eklhpsglgyp7lkg3rloppey")
			},
		},
	}
	key := "SHA256E-s8783566--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	tmpDir := t.TempDir()
	tmp, err := os.CreateTemp(tmpDir, key)
	if err != nil {
		t.Error(err)
	}
	err = ai.Store(ma, key, tmp.Name())
	if err != nil {
		t.Errorf("expected Store to not return an error; got %v", err)
	}
	if getStateCalled == 0 {
		t.Errorf("expected GetState to be called; it wasn't")
	}
	if getURLsCalled != 1 {
		t.Errorf("expected GetURLs to be called once; it was called %d times", getURLsCalled)
	}
	if !iaUploadCalled {
		t.Errorf("expected upload to be called; it wasn't")
	}
	if newState != "bafybeidyw5kttfefev2jt7bbiwrqsepzb66eklhpsglgyp7lkg3rloppey" {
		t.Errorf("expected state to be bafybeidyw5kttfefev2jt7bbiwrqsepzb66eklhpsglgyp7lkg3rloppey; got %s", newState)
	}
}

func TestStoreOneURL(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled, getURLsCalled int
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return ""
	}
	ma.GetURLsFn = func(key, prefix string) []string {
		getURLsCalled++
		return []string{"https://ipfs.io/ipfs/bafybeidyw5kttfefev2jt7bbiwrqsepzb66eklhpsglgyp7lkg3rloppey"}
	}
	ma.InfoFFn = func(fmt string, args ...interface{}) {
	}
	expectedKey := "SHA256E-s8783566--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	var newState string
	ma.SetStateFn = func(key, state string) {
		if key != expectedKey {
			t.Errorf("expected SetState to be called with key %s; got %s", expectedKey, key)
		}
		newState = state
	}
	var iaPinCalled bool
	ai := AI{
		I: &mockIPFSRemote{
			pinFn: func(context.Context, cid.Cid, string) error {
				iaPinCalled = true
				return nil
			},
		},
	}

	tmpDir := t.TempDir()
	tmp, err := os.CreateTemp(tmpDir, expectedKey)
	if err != nil {
		t.Error(err)
	}
	err = ai.Store(ma, expectedKey, tmp.Name())
	if err != nil {
		t.Errorf("expected Store to not return an error; got %v", err)
	}
	if getStateCalled == 0 {
		t.Errorf("expected GetState to be called; it wasn't")
	}
	if getURLsCalled != 1 {
		t.Errorf("expected GetURLs to be called once; it was called %d times", getURLsCalled)
	}
	if !iaPinCalled {
		t.Errorf("expected Pin to be called; it wasn't")
	}
	if newState != "bafybeidyw5kttfefev2jt7bbiwrqsepzb66eklhpsglgyp7lkg3rloppey" {
		t.Errorf("expected state to be bafybeidyw5kttfefev2jt7bbiwrqsepzb66eklhpsglgyp7lkg3rloppey; got %s", newState)
	}
}

func TestStoreOneURLCantPin(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled, getURLsCalled int
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return ""
	}
	ma.GetURLsFn = func(key, prefix string) []string {
		getURLsCalled++
		return []string{"https://ipfs.io/ipfs/bafybeidyw5kttfefev2jt7bbiwrqsepzb66eklhpsglgyp7lkg3rloppey"}
	}
	ma.InfoFFn = func(fmt string, args ...interface{}) {
	}
	expectedKey := "SHA256E-s8783566--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	var newState string
	ma.SetStateFn = func(key, state string) {
		if key != expectedKey {
			t.Errorf("expected SetState to be called with key %s; got %s", expectedKey, key)
		}
		newState = state
	}
	var iaPinCalled, iaUploadCalled bool
	ai := AI{
		I: &mockIPFSRemote{
			pinFn: func(context.Context, cid.Cid, string) error {
				iaPinCalled = true
				return ErrCantPin
			},
			uploadFn: func(context.Context, fs.File, *progress.Tracker) (cid.Cid, error) {
				iaUploadCalled = true
				return cid.Decode("bafybeidyw5kttfefev2jt7bbiwrqsepzb66eklhpsglgyp7lkg3rloppey")
			},
		},
	}

	tmpDir := t.TempDir()
	tmp, err := os.CreateTemp(tmpDir, expectedKey)
	if err != nil {
		t.Error(err)
	}
	err = ai.Store(ma, expectedKey, tmp.Name())
	if err != nil {
		t.Errorf("expected Store to not return an error; got %v", err)
	}
	if getStateCalled == 0 {
		t.Errorf("expected GetState to be called; it wasn't")
	}
	if getURLsCalled != 1 {
		t.Errorf("expected GetURLs to be called once; it was called %d times", getURLsCalled)
	}
	if !iaPinCalled {
		t.Errorf("expected Pin to be called; it wasn't")
	}
	if !iaUploadCalled {
		t.Errorf("expected Upload to be called; it wasn't")
	}
	if newState != "bafybeidyw5kttfefev2jt7bbiwrqsepzb66eklhpsglgyp7lkg3rloppey" {
		t.Errorf("expected state to be bafybeidyw5kttfefev2jt7bbiwrqsepzb66eklhpsglgyp7lkg3rloppey; got %s", newState)
	}
}

func TestStoreOneCIDURLOneOther(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled, getURLsCalled int
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return ""
	}
	ma.GetURLsFn = func(key, prefix string) []string {
		getURLsCalled++
		return []string{"https://example.com/", "https://ipfs.io/ipfs/bafybeidyw5kttfefev2jt7bbiwrqsepzb66eklhpsglgyp7lkg3rloppey"}
	}
	ma.InfoFFn = func(fmt string, args ...interface{}) {
	}
	expectedKey := "SHA256E-s8783566--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	var newState string
	ma.SetStateFn = func(key, state string) {
		if key != expectedKey {
			t.Errorf("expected SetState to be called with key %s; got %s", expectedKey, key)
		}
		newState = state
	}
	var iaPinCalled bool
	ai := AI{
		I: &mockIPFSRemote{
			pinFn: func(context.Context, cid.Cid, string) error {
				iaPinCalled = true
				return nil
			},
		},
	}

	tmpDir := t.TempDir()
	tmp, err := os.CreateTemp(tmpDir, expectedKey)
	if err != nil {
		t.Error(err)
	}
	err = ai.Store(ma, expectedKey, tmp.Name())
	if err != nil {
		t.Errorf("expected Store to not return an error; got %v", err)
	}
	if getStateCalled == 0 {
		t.Errorf("expected GetState to be called; it wasn't")
	}
	if getURLsCalled != 1 {
		t.Errorf("expected GetURLs to be called once; it was called %d times", getURLsCalled)
	}
	if !iaPinCalled {
		t.Errorf("expected Pin to be called; it wasn't")
	}
	if newState != "bafybeidyw5kttfefev2jt7bbiwrqsepzb66eklhpsglgyp7lkg3rloppey" {
		t.Errorf("expected state to be bafybeidyw5kttfefev2jt7bbiwrqsepzb66eklhpsglgyp7lkg3rloppey; got %s", newState)
	}
}

func TestStoreOneCIDURLOneOtherCantPin(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled, getURLsCalled int
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return ""
	}
	ma.GetURLsFn = func(key, prefix string) []string {
		getURLsCalled++
		return []string{"https://example.com/", "https://ipfs.io/ipfs/bafybeidyw5kttfefev2jt7bbiwrqsepzb66eklhpsglgyp7lkg3rloppey"}
	}
	ma.InfoFFn = func(fmt string, args ...interface{}) {
	}
	expectedKey := "SHA256E-s8783566--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	var newState string
	ma.SetStateFn = func(key, state string) {
		if key != expectedKey {
			t.Errorf("expected SetState to be called with key %s; got %s", expectedKey, key)
		}
		newState = state
	}
	var iaPinCalled, iaUploadCalled bool
	ai := AI{
		I: &mockIPFSRemote{
			pinFn: func(context.Context, cid.Cid, string) error {
				iaPinCalled = true
				return ErrCantPin
			},
			uploadFn: func(context.Context, fs.File, *progress.Tracker) (cid.Cid, error) {
				iaUploadCalled = true
				return cid.Decode("bafybeidyw5kttfefev2jt7bbiwrqsepzb66eklhpsglgyp7lkg3rloppey")
			},
		},
	}

	tmpDir := t.TempDir()
	tmp, err := os.CreateTemp(tmpDir, expectedKey)
	if err != nil {
		t.Error(err)
	}
	err = ai.Store(ma, expectedKey, tmp.Name())
	if err != nil {
		t.Errorf("expected Store to not return an error; got %v", err)
	}
	if getStateCalled == 0 {
		t.Errorf("expected GetState to be called; it wasn't")
	}
	if getURLsCalled != 1 {
		t.Errorf("expected GetURLs to be called once; it was called %d times", getURLsCalled)
	}
	if !iaPinCalled {
		t.Errorf("expected Pin to be called; it wasn't")
	}
	if !iaUploadCalled {
		t.Errorf("expected Upload to be called; it wasn't")
	}
	if newState != "bafybeidyw5kttfefev2jt7bbiwrqsepzb66eklhpsglgyp7lkg3rloppey" {
		t.Errorf("expected state to be bafybeidyw5kttfefev2jt7bbiwrqsepzb66eklhpsglgyp7lkg3rloppey; got %s", newState)
	}
}

func TestStoreAllIrrelevantURLs(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled, getURLsCalled int
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return ""
	}
	ma.GetURLsFn = func(key, prefix string) []string {
		getURLsCalled++
		return []string{"https://example.com/", "https://cid.example.net/foo/bar"}
	}
	ma.InfoFFn = func(fmt string, args ...interface{}) {
	}
	expectedKey := "SHA256E-s8783566--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	var newState string
	ma.SetStateFn = func(key, state string) {
		if key != expectedKey {
			t.Errorf("expected SetState to be called with key %s; got %s", expectedKey, key)
		}
		newState = state
	}
	expectedCID := "bafkreigoamtzilpekuolvght2fkaceorudpdrfsqkhnii7dv3coqxi6en4"
	var iaUploadCalled bool
	ai := AI{
		I: &mockIPFSRemote{
			uploadFn: func(context.Context, fs.File, *progress.Tracker) (cid.Cid, error) {
				iaUploadCalled = true
				return cid.Decode(expectedCID)
			},
		},
	}

	tmpDir := t.TempDir()
	tmp, err := os.CreateTemp(tmpDir, expectedKey)
	if err != nil {
		t.Error(err)
	}
	err = ai.Store(ma, expectedKey, tmp.Name())
	if err != nil {
		t.Errorf("expected Store to not return an error; got %v", err)
	}
	if getStateCalled == 0 {
		t.Errorf("expected GetState to be called; it wasn't")
	}
	if getURLsCalled != 1 {
		t.Errorf("expected GetURLs to be called once; it was called %d times", getURLsCalled)
	}
	if !iaUploadCalled {
		t.Errorf("expected Upload to be called; it wasn't")
	}
	if newState != expectedCID {
		t.Errorf("expected state to be %s; got %s", expectedCID, newState)
	}
}

func TestStoreCantPin(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled, getURLsCalled int
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return ""
	}
	ma.GetURLsFn = func(key, prefix string) []string {
		getURLsCalled++
		return []string{
			"https://ipfs.io/ipfs/bafybeidyw5kttfefev2jt7bbiwrqsepzb66eklhpsglgyp7lkg3rloppey",
			"ipfs://bafybeihklim7opjg3cjiyqq37ncg6ml5oqbmgschbqiajojrc5ziij6m3u",
		}
	}
	expectedKey := "SHA256E-s8783566--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	var uriPresent string
	ma.SetURIPresentFn = func(key, uri string) {
		if key != expectedKey {
			t.Errorf("expected SetState to be called with key %s; got %s", expectedKey, key)
		}
		uriPresent = uri
	}
	ma.InfoFFn = func(fmt string, args ...interface{}) {}
	ma.DebugFFn = func(fmt string, args ...interface{}) {}

	var newState string
	ma.SetStateFn = func(key, state string) {
		if key != expectedKey {
			t.Errorf("expected SetState to be called with key %s; got %s", expectedKey, key)
		}
		newState = state
	}
	expectedCID := "bafkreigoamtzilpekuolvght2fkaceorudpdrfsqkhnii7dv3coqxi6en4"
	var iaPinCalled, iaUploadCalled int
	ai := AI{
		I: &mockIPFSRemote{
			pinFn: func(context.Context, cid.Cid, string) error {
				iaPinCalled++
				return fmt.Errorf("can't pin")
			},
			uploadFn: func(context.Context, fs.File, *progress.Tracker) (cid.Cid, error) {
				iaUploadCalled++
				return cid.Decode(expectedCID)
			},
		},
	}

	tmpDir := t.TempDir()
	tmp, err := os.CreateTemp(tmpDir, expectedKey)
	if err != nil {
		t.Error(err)
	}
	err = ai.Store(ma, expectedKey, tmp.Name())
	if err != nil {
		t.Errorf("expected Store to not return an error; got %v", err)
	}
	if getStateCalled == 0 {
		t.Errorf("expected GetState to be called; it wasn't")
	}
	if getURLsCalled != 1 {
		t.Errorf("expected GetURLs to be called once; it was called %d times", getURLsCalled)
	}
	if iaPinCalled != 2 {
		t.Errorf("expected Pin to be called twice; it was called %d times", iaPinCalled)
	}
	if iaUploadCalled != 1 {
		t.Errorf("expected Upload to be called once; it was called %d times", iaUploadCalled)
	}
	if newState != expectedCID {
		t.Errorf("expected state to be %s; got %s", expectedCID, newState)
	}
	if uriPresent != "ipfs://"+expectedCID {
		t.Errorf("expected present URI to be for CID %s; got %s", expectedCID, uriPresent)
	}
}

func TestPresentNoState(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled, getURLsCalled int
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return ""
	}
	ma.GetURLsFn = func(key, prefix string) []string {
		getURLsCalled++
		return []string{}
	}
	var iaPresentCalled bool
	ai := AI{
		I: &mockIPFSRemote{
			presentFn: func(context.Context, cid.Cid) (bool, error) {
				iaPresentCalled = true
				return false, nil
			},
		},
	}
	key := "SHA256E-s8783566--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	is, err := ai.Present(ma, key)
	if err != nil {
		t.Errorf("expected Present to not return an error; got %v", err)
	}
	if is {
		t.Errorf("expected is to be false; it was true")
	}
	if getStateCalled != 1 {
		t.Errorf("expected GetState to be called once; it was called %d times", getStateCalled)
	}
	if getURLsCalled != 0 {
		t.Errorf("expected GetURLs not to be called; it was called %d times", getURLsCalled)
	}
	if iaPresentCalled {
		t.Errorf("expected IsPresent not to be called; it was")
	}
}

func TestPresentOK(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled, getURLsCalled int
	expectedCID := "bafybeihklim7opjg3cjiyqq37ncg6ml5oqbmgschbqjajojrc5ziij6m2u"
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return expectedCID
	}
	ma.GetURLsFn = func(key, prefix string) []string {
		getURLsCalled++
		return []string{}
	}
	var iaPresentCalled bool
	ai := AI{
		I: &mockIPFSRemote{
			presentFn: func(context.Context, cid.Cid) (bool, error) {
				iaPresentCalled = true
				return true, nil
			},
		},
	}
	key := "SHA256E-s8783566--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	is, err := ai.Present(ma, key)
	if err != nil {
		t.Errorf("expected Present to not return an error; got %v", err)
	}
	if !is {
		t.Errorf("expected is to be true; it was false")
	}
	if getStateCalled != 1 {
		t.Errorf("expected GetState to be called once; it was called %d times", getStateCalled)
	}
	if getURLsCalled != 0 {
		t.Errorf("expected GetURLs not to be called; it was called %d times", getURLsCalled)
	}
	if !iaPresentCalled {
		t.Errorf("expected IsPresent to be called; it wasn't")
	}
}

func TestPresentNotOK(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled, getURLsCalled int
	expectedCID := "bafybeihklim7opjg3cjiyqq37ncg6ml5oqbmgschbqjajojrc5ziij6m2u"
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return expectedCID
	}
	ma.GetURLsFn = func(key, prefix string) []string {
		getURLsCalled++
		return []string{}
	}
	var iaPresentCalled bool
	ai := AI{
		I: &mockIPFSRemote{
			presentFn: func(context.Context, cid.Cid) (bool, error) {
				iaPresentCalled = true
				return false, nil
			},
		},
	}
	key := "SHA256E-s8783566--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	is, err := ai.Present(ma, key)
	if err != nil {
		t.Errorf("expected Present to not return an error; got %v", err)
	}
	if is {
		t.Errorf("expected is to be false; it was true")
	}
	if getStateCalled != 1 {
		t.Errorf("expected GetState to be called once; it was called %d times", getStateCalled)
	}
	if getURLsCalled != 0 {
		t.Errorf("expected GetURLs not to be called; it was called %d times", getURLsCalled)
	}
	if !iaPresentCalled {
		t.Errorf("expected IsPresent to be called; it wasn't")
	}
}

func TestPresentError(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled, getURLsCalled int
	expectedCID := "bafybeihklim7opjg3cjiyqq37ncg6ml5oqbmgschbqjajojrc5ziij6m2u"
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return expectedCID
	}
	ma.GetURLsFn = func(key, prefix string) []string {
		getURLsCalled++
		return []string{}
	}
	var iaPresentCalled bool
	ai := AI{
		I: &mockIPFSRemote{
			presentFn: func(context.Context, cid.Cid) (bool, error) {
				iaPresentCalled = true
				return false, fmt.Errorf("network error")
			},
		},
	}
	key := "SHA256E-s8783566--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	is, err := ai.Present(ma, key)
	if err == nil {
		t.Errorf("expected Present to return an error; got nil")
	}
	if is {
		t.Errorf("expected is to be false; it was true")
	}
	if getStateCalled != 1 {
		t.Errorf("expected GetState to be called once; it was called %d times", getStateCalled)
	}
	if getURLsCalled != 0 {
		t.Errorf("expected GetURLs not to be called; it was called %d times", getURLsCalled)
	}
	if !iaPresentCalled {
		t.Errorf("expected IsPresent to be called; it wasn't")
	}
}

func TestRemoveNoState(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled, getURLsCalled int
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return ""
	}
	ma.GetURLsFn = func(key, prefix string) []string {
		getURLsCalled++
		return []string{}
	}
	var unpinCalled bool
	ai := AI{
		I: &mockIPFSRemote{
			unpinFn: func(context.Context, cid.Cid) error {
				unpinCalled = true
				return nil
			},
		},
	}
	key := "SHA256E-s8783566--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	err := ai.Remove(ma, key)
	if err != nil {
		t.Errorf("expected Remove to return no error; got %v", err)
	}
	if getStateCalled != 1 {
		t.Errorf("expected GetState to be called once; it was called %d times", getStateCalled)
	}
	if getURLsCalled != 0 {
		t.Errorf("expected GetURLs not to be called; it was called %d times", getURLsCalled)
	}
	if unpinCalled {
		t.Errorf("expected Unpin not to be called; it was")
	}
}

func TestRemoveOK(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled, getURLsCalled, setURLMissingCalled, setURIMissingCalled int
	expectedCID := "bafybeifwo73adcu34smfg6cksrio7b3g34nb6uxtipxjd3p6fmyvtq4hzu"
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return expectedCID
	}
	ma.GetURLsFn = func(key, prefix string) []string {
		getURLsCalled++
		return []string{}
	}
	ma.SetURLMissingFn = func(string, string) {
		setURLMissingCalled++
	}
	ma.SetURIMissingFn = func(string, string) {
		setURIMissingCalled++
	}
	var unpinCalled bool
	ai := AI{
		I: &mockIPFSRemote{
			unpinFn: func(ctx context.Context, c cid.Cid) error {
				unpinCalled = true
				if c.String() != expectedCID {
					t.Errorf("expected Unpin to be called with CID %s; got %s", expectedCID, c)
				}
				return nil
			},
		},
	}
	key := "SHA256E-s8783566--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	err := ai.Remove(ma, key)
	if err != nil {
		t.Errorf("expected Remove to return no error; got %v", err)
	}
	if getStateCalled != 1 {
		t.Errorf("expected GetState to be called once; it was called %d times", getStateCalled)
	}
	if getURLsCalled != 0 {
		t.Errorf("expected GetURLs not to be called; it was called %d times", getURLsCalled)
	}
	if setURIMissingCalled != 1 {
		t.Errorf("expected SetURIMissing to be called once; it was called %d times", setURIMissingCalled)
	}
	if setURLMissingCalled != 1 {
		t.Errorf("expected SetURLMissing to be called once; it was called %d times", setURLMissingCalled)
	}
	if !unpinCalled {
		t.Errorf("expected Unpin to be called; it wasn't")
	}
}

func TestRemoveOKCantPin(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled, getURLsCalled, setURLMissingCalled, setURIMissingCalled int
	expectedCID := "bafybeifwo73adcu34smfg6cksrio7b3g34nb6uxtipxjd3p6fmyvtq4hzu"
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return expectedCID
	}
	ma.GetURLsFn = func(key, prefix string) []string {
		getURLsCalled++
		return []string{}
	}
	ma.SetURLMissingFn = func(string, string) {
		setURLMissingCalled++
	}
	ma.SetURIMissingFn = func(string, string) {
		setURIMissingCalled++
	}
	var unpinCalled bool
	ai := AI{
		I: &mockIPFSRemote{
			unpinFn: func(ctx context.Context, c cid.Cid) error {
				unpinCalled = true
				if c.String() != expectedCID {
					t.Errorf("expected Unpin to be called with CID %s; got %s", expectedCID, c)
				}
				return ErrCantPin
			},
		},
	}
	key := "SHA256E-s8783566--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	err := ai.Remove(ma, key)
	if err != nil {
		t.Errorf("expected Remove to return no error; got %v", err)
	}
	if getStateCalled != 1 {
		t.Errorf("expected GetState to be called once; it was called %d times", getStateCalled)
	}
	if getURLsCalled != 0 {
		t.Errorf("expected GetURLs not to be called; it was called %d times", getURLsCalled)
	}
	if !unpinCalled {
		t.Errorf("expected Unpin to be called; it wasn't")
	}
	if setURIMissingCalled != 1 {
		t.Errorf("expected SetURIMissing to be called once; it was called %d times", setURIMissingCalled)
	}
	if setURLMissingCalled != 1 {
		t.Errorf("expected SetURLMissing to be called once; it was called %d times", setURLMissingCalled)
	}
}

func TestRemoveError(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled, getURLsCalled, setURLMissingCalled, setURIMissingCalled int
	expectedCID := "bafybeifwo73adcu34smfg6cksrio7b3g34nb6uxtipxjd3p6fmyvtq4hzu"
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return expectedCID
	}
	ma.GetURLsFn = func(key, prefix string) []string {
		getURLsCalled++
		return []string{}
	}
	ma.SetURLMissingFn = func(string, string) {
		setURLMissingCalled++
	}
	ma.SetURIMissingFn = func(string, string) {
		setURIMissingCalled++
	}
	var unpinCalled bool
	ai := AI{
		I: &mockIPFSRemote{
			unpinFn: func(ctx context.Context, c cid.Cid) error {
				unpinCalled = true
				if c.String() != expectedCID {
					t.Errorf("expected Unpin to be called with CID %s; got %s", expectedCID, c)
				}
				return fmt.Errorf("some error")
			},
		},
	}

	key := "SHA256E-s8783566--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	err := ai.Remove(ma, key)
	if err == nil {
		t.Errorf("expected Remove to return an error; got nil")
	}
	if getStateCalled != 1 {
		t.Errorf("expected GetState to be called once; it was called %d times", getStateCalled)
	}
	if getURLsCalled != 0 {
		t.Errorf("expected GetURLs not to be called; it was called %d times", getURLsCalled)
	}
	if setURIMissingCalled != 0 {
		t.Errorf("expected SetURIMissing not to be called; it was called %d times", setURIMissingCalled)
	}
	if setURLMissingCalled != 0 {
		t.Errorf("expected SetURLMissing not to be called; it was called %d times", setURLMissingCalled)
	}
	if !unpinCalled {
		t.Errorf("expected Unpin to be called; it wasn't")
	}
}

func TestRetrieveNoGetters(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled int
	expectedCID := "bafybeifwo73adcu34smfg6cksrio7b3g34nb6uxtipxjd3p6fmyvtq4hzu"
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return expectedCID
	}
	ai := AI{
		I: &mockIPFSRemote{},
		R: &retrieve.R{},
	}
	key := "SHA256E-s8783566--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	tmpFh, err := os.CreateTemp(t.TempDir(), key)
	if err != nil {
		t.Fatal(err)
	}
	defer tmpFh.Close()
	err = ai.Retrieve(ma, key, tmpFh.Name())
	if err == nil {
		t.Errorf("expected Retrieve to return an error; got nil")
	}
	if getStateCalled != 1 {
		t.Errorf("expected GetState to be called once; it was called %d times", getStateCalled)
	}
}

type mockGetter func(context.Context, cid.Cid, int64) (io.ReadCloser, error)

func (mg mockGetter) Get(ctx context.Context, c cid.Cid, pos int64) (io.ReadCloser, error) {
	return mg(ctx, c, pos)
}
func (mg mockGetter) String() string { return "mockGetter" }

func newMockRC(bs []byte) io.ReadCloser {
	return io.NopCloser(bytes.NewReader(bs))
}

func TestRetrieveNoState(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled, getCalled int
	expectedCID := "bafybeifwo73adcu34smfg6cksrio7b3g34nb6uxtipxjd3p6fmyvtq4hzu"
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return ""
	}
	getter := func(_ context.Context, c cid.Cid, pos int64) (io.ReadCloser, error) {
		if c.String() != expectedCID {
			t.Errorf("Get: expected to receive CID %s; got %s", expectedCID, c)
		}
		getCalled++
		return newMockRC([]byte("foo")), nil
	}
	ai := AI{
		R: &retrieve.R{Getters: []retrieve.Getter{mockGetter(getter)}},
	}
	key := "SHA256E-s8783566--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	tmpFh, err := os.CreateTemp(t.TempDir(), key)
	if err != nil {
		t.Fatal(err)
	}
	defer tmpFh.Close()
	err = ai.Retrieve(ma, key, tmpFh.Name())
	if err == nil {
		t.Errorf("expected Retrieve to return an error; got nil")
	}
	if getStateCalled != 1 {
		t.Errorf("expected GetState to be called once; it was called %d times", getStateCalled)
	}
	if getCalled != 0 {
		t.Errorf("expected Get not to be called; it was called %d times", getCalled)
	}
}

func TestRetrieveSingleGetter(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled, getCalled int
	expectedCID := "bafybeifwo73adcu34smfg6cksrio7b3g34nb6uxtipxjd3p6fmyvtq4hzu"
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return expectedCID
	}
	getter := func(_ context.Context, c cid.Cid, pos int64) (io.ReadCloser, error) {
		if c.String() != expectedCID {
			t.Errorf("Get: expected to receive CID %s; got %s", expectedCID, c)
		}
		getCalled++
		return newMockRC([]byte("bar")), nil
	}
	ai := AI{
		R: &retrieve.R{
			Getters: []retrieve.Getter{
				mockGetter(getter),
			},
		},
	}
	key := "SHA256E-s3--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	tmpFh, err := os.CreateTemp(t.TempDir(), key)
	if err != nil {
		t.Fatal(err)
	}
	defer tmpFh.Close()
	err = ai.Retrieve(ma, key, tmpFh.Name())
	if err != nil {
		t.Errorf("expected Remove not to return an error; got %v", err)
	}
	if getStateCalled != 1 {
		t.Errorf("expected GetState to be called once; it was called %d times", getStateCalled)
	}
	if getCalled != 1 {
		t.Errorf("expected Get to be called once; it was called %d times", getCalled)
	}
}

func TestRetrieveSingleGetterFails(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled, getCalled int
	expectedCID := "bafybeifwo73adcu34smfg6cksrio7b3g34nb6uxtipxjd3p6fmyvtq4hzu"
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return expectedCID
	}
	getter := func(_ context.Context, c cid.Cid, pos int64) (io.ReadCloser, error) {
		if c.String() != expectedCID {
			t.Errorf("Get: expected to receive CID %s; got %s", expectedCID, c)
		}
		getCalled++
		return nil, fmt.Errorf("nope")
	}
	ai := AI{
		R: &retrieve.R{Getters: []retrieve.Getter{mockGetter(getter)}},
	}
	key := "SHA256E-s8783566--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	tmpFh, err := os.CreateTemp(t.TempDir(), key)
	if err != nil {
		t.Fatal(err)
	}
	defer tmpFh.Close()
	err = ai.Retrieve(ma, key, tmpFh.Name())
	if err == nil {
		t.Errorf("expected Remove to return an error; got nil")
	}
	if getStateCalled != 1 {
		t.Errorf("expected GetState to be called once; it was called %d times", getStateCalled)
	}
	if getCalled != 1 {
		t.Errorf("expected Get to be called once; it was called %d times", getCalled)
	}
}

func TestRetrieveSingleGetterFailsSucceeds(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled, get1Called, get2Called, get3Called int
	expectedCID := "bafybeifwo73adcu34smfg6cksrio7b3g34nb6uxtipxjd3p6fmyvtq4hzu"
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return expectedCID
	}
	getter1 := func(_ context.Context, c cid.Cid, pos int64) (io.ReadCloser, error) {
		if c.String() != expectedCID {
			t.Errorf("Get: expected to receive CID %s; got %s", expectedCID, c)
		}
		get1Called++
		return nil, fmt.Errorf("nope")
	}
	getter2 := func(_ context.Context, c cid.Cid, pos int64) (io.ReadCloser, error) {
		if c.String() != expectedCID {
			t.Errorf("Get: expected to receive CID %s; got %s", expectedCID, c)
		}
		get2Called++
		return io.NopCloser(bytes.NewReader([]byte("aosdidh"))), nil
	}
	getter3 := func(_ context.Context, c cid.Cid, pos int64) (io.ReadCloser, error) {
		if c.String() != expectedCID {
			t.Errorf("Get: expected to receive CID %s; got %s", expectedCID, c)
		}
		get3Called++
		return io.NopCloser(bytes.NewReader([]byte{})), nil
	}
	ai := AI{
		R: &retrieve.R{
			Getters: []retrieve.Getter{
				mockGetter(getter1),
				mockGetter(getter2),
				mockGetter(getter3),
			},
		},
	}
	key := "SHA256E-s7--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	tmpFh, err := os.CreateTemp(t.TempDir(), key)
	if err != nil {
		t.Fatal(err)
	}
	defer tmpFh.Close()
	err = ai.Retrieve(ma, key, tmpFh.Name())
	if err != nil {
		t.Errorf("expected Retrieve not to return an error; got %v", err)
	}
	if getStateCalled != 1 {
		t.Errorf("expected GetState to be called once; it was called %d times", getStateCalled)
	}
	if get1Called != 1 {
		t.Errorf("expected Get[0] to be called once; it was called %d times", get1Called)
	}
	if get2Called != 1 {
		t.Errorf("expected Get[1] to be called once; it was called %d times", get2Called)
	}
	if get3Called != 0 {
		t.Errorf("expected Get[2] not to be called; it was called %d times", get3Called)
	}
}

func TestRetrieveAllGettersFail(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled, get1Called, get2Called, get3Called int
	expectedCID := "bafybeifwo73adcu34smfg6cksrio7b3g34nb6uxtipxjd3p6fmyvtq4hzu"
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return expectedCID
	}
	getter1 := func(_ context.Context, c cid.Cid, pos int64) (io.ReadCloser, error) {
		if c.String() != expectedCID {
			t.Errorf("Get: expected to receive CID %s; got %s", expectedCID, c)
		}
		get1Called++
		return nil, fmt.Errorf("nope 1")
	}
	getter2 := func(_ context.Context, c cid.Cid, pos int64) (io.ReadCloser, error) {
		if c.String() != expectedCID {
			t.Errorf("Get: expected to receive CID %s; got %s", expectedCID, c)
		}
		get2Called++
		return nil, fmt.Errorf("nope 2")
	}
	getter3 := func(_ context.Context, c cid.Cid, pos int64) (io.ReadCloser, error) {
		if c.String() != expectedCID {
			t.Errorf("Get: expected to receive CID %s; got %s", expectedCID, c)
		}
		get3Called++
		return nil, fmt.Errorf("nope 3")
	}
	ai := AI{
		R: &retrieve.R{
			Getters: []retrieve.Getter{
				mockGetter(getter1),
				mockGetter(getter2),
				mockGetter(getter3),
			},
		},
	}
	key := "SHA256E-s8783566--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	tmpFh, err := os.CreateTemp(t.TempDir(), key)
	if err != nil {
		t.Fatal(err)
	}
	defer tmpFh.Close()
	err = ai.Retrieve(ma, key, tmpFh.Name())
	if err == nil {
		t.Errorf("expected Retrieve to return an error; got %v", err)
	}
	if getStateCalled != 1 {
		t.Errorf("expected GetState to be called once; it was called %d times", getStateCalled)
	}
	if get1Called != 1 {
		t.Errorf("expected Get[0] to be called once; it was called %d times", get1Called)
	}
	if get2Called != 1 {
		t.Errorf("expected Get[1] to be called once; it was called %d times", get2Called)
	}
	if get3Called != 1 {
		t.Errorf("expected Get[2] to be called once; it was called %d times", get3Called)
	}
}

func TestRetrieveNonExistentFile(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled, getCalled int
	expectedCID := "bafybeifwo73adcu34smfg6cksrio7b3g34nb6uxtipxjd3p6fmyvtq4hzu"
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return expectedCID
	}
	getter := func(_ context.Context, c cid.Cid, pos int64) (io.ReadCloser, error) {
		if c.String() != expectedCID {
			t.Errorf("Get: expected to receive CID %s; got %s", expectedCID, c)
		}
		getCalled++
		return io.NopCloser(bytes.NewReader([]byte("hy7"))), nil
	}
	ai := AI{
		R: &retrieve.R{
			Getters: []retrieve.Getter{
				mockGetter(getter),
			},
		},
	}
	key := "SHA256E-s3--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	tmpFh, err := os.CreateTemp(t.TempDir(), key)
	if err != nil {
		t.Fatal(err)
	}
	tmpFhName := tmpFh.Name()
	if err := os.Remove(tmpFhName); err != nil {
		t.Fatal(err)
	}
	defer tmpFh.Close()
	err = ai.Retrieve(ma, key, tmpFhName)
	if err != nil {
		t.Errorf("expected Retrieve not to return an error; got %v", err)
	}
	if getStateCalled != 1 {
		t.Errorf("expected GetState to be called once; it was called %d times", getStateCalled)
	}
	if getCalled != 1 {
		t.Errorf("expected Get[0] to be called once; it was called %d times", getCalled)
	}
	if _, err := os.Stat(tmpFhName); err != nil {
		t.Errorf("expected %s to have been created; stat sez %v", tmpFhName, err)
	}
}

func TestRetrievePartiallyDownloadedFile(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled, getCalled int
	expectedCID := "bafybeifwo73adcu34smfg6cksrio7b3g34nb6uxtipxjd3p6fmyvtq4hzu"
	expectedContents := "foobar"
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return expectedCID
	}
	getter := func(_ context.Context, c cid.Cid, pos int64) (io.ReadCloser, error) {
		if c.String() != expectedCID {
			t.Errorf("Get: expected to receive CID %s; got %s", expectedCID, c)
		}
		if pos != 3 {
			t.Errorf("expected pos to be 3; it was %d", pos)
		}
		getCalled++
		return io.NopCloser(strings.NewReader(expectedContents[3:])), nil
	}
	ai := AI{
		R: &retrieve.R{
			Getters: []retrieve.Getter{
				mockGetter(getter),
			},
		},
	}
	key := "SHA256E-s6--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	tmpFh, err := os.CreateTemp(t.TempDir(), key)
	if err != nil {
		t.Fatal(err)
	}
	if _, err := io.WriteString(tmpFh, expectedContents[:3]); err != nil {
		t.Fatal(err)
	}
	err = ai.Retrieve(ma, key, tmpFh.Name())
	if err != nil {
		t.Errorf("expected Retrieve not to return an error; got %v", err)
	}
	if getStateCalled != 1 {
		t.Errorf("expected GetState to be called once; it was called %d times", getStateCalled)
	}
	if getCalled != 1 {
		t.Errorf("expected Get[0] to be called once; it was called %d times", getCalled)
	}
}

func TestRetrieveVerificationFail(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled, srvCalled, verifierCalled int
	expectedCID := "bafybeifwo73adcu34smfg6cksrio7b3g34nb6uxtipxjd3p6fmyvtq4hzu"
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return expectedCID
	}
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		srvCalled++
		io.WriteString(w, "fooBar")
	}))
	defer srv.Close()
	gu := GatewayURL{Base: srv.URL}
	ai := AI{
		R: &retrieve.R{
			Getters: []retrieve.Getter{
				&Gateway{URL: gu},
			},
		},
		verifier: func(ctx context.Context, bg retrieve.BlockGetter, c cid.Cid, rsa readerSeekerAt) (int64, error) {
			verifierCalled++
			return 0, fmt.Errorf("verification failed")
		},
	}
	key := "MD5E-s6--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	tmpFh, err := os.CreateTemp(t.TempDir(), key)
	if err != nil {
		t.Fatal(err)
	}
	err = ai.Retrieve(ma, key, tmpFh.Name())
	if err == nil {
		t.Errorf("expected Retrieve to return an error; got nil")
	}
	if getStateCalled != 1 {
		t.Errorf("expected GetState to be called once; it was called %d times", getStateCalled)
	}
	if srvCalled != 1 {
		t.Errorf("expected %s to be called once; it was called %d times", srv.URL, srvCalled)
	}
	if verifierCalled != 1 {
		t.Errorf("expected verifier to be called once; it was called %d times", verifierCalled)
	}
}

func TestRetrieveProgress(t *testing.T) {
	t.Parallel()
	ma := new(mockannex.Annex)
	var getStateCalled, get1Called, get2Called, get3Called int
	expectedCID := "bafybeifwo73adcu34smfg6cksrio7b3g34nb6uxtipxjd3p6fmyvtq4hzu"
	ma.GetStateFn = func(key string) string {
		getStateCalled++
		return expectedCID
	}
	var progress int
	ma.ProgressFn = func(n int) {
		progress = n
	}
	getter1 := func(_ context.Context, c cid.Cid, pos int64) (io.ReadCloser, error) {
		if c.String() != expectedCID {
			t.Errorf("Get: expected to receive CID %s; got %s", expectedCID, c)
		}
		get1Called++
		if progress > 0 {
			t.Errorf("Get: expected progress to be 0 before first Getter returns; it is %d", progress)
		}
		return io.NopCloser(bytes.NewReader([]byte("abc"))), nil
	}
	getter2 := func(_ context.Context, c cid.Cid, pos int64) (io.ReadCloser, error) {
		if c.String() != expectedCID {
			t.Errorf("Get: expected to receive CID %s; got %s", expectedCID, c)
		}
		get2Called++
		if progress > 2 && progress < 4 {
			t.Errorf("Get: expected progress to be between 2 and 4 before second Getter returns; it is %d", progress)
		}
		return nil, fmt.Errorf("blagh")
	}
	getter3 := func(_ context.Context, c cid.Cid, pos int64) (io.ReadCloser, error) {
		if c.String() != expectedCID {
			t.Errorf("Get: expected to receive CID %s; got %s", expectedCID, c)
		}
		get3Called++
		if progress > 2 && progress < 4 {
			t.Errorf("Get: expected progress to be between 2 and 4 before second Getter returns; it is %d", progress)
		}
		return io.NopCloser(bytes.NewReader([]byte("abcdefghi"))), nil
	}
	ai := AI{
		R: &retrieve.R{
			Getters: []retrieve.Getter{
				mockGetter(getter1),
				mockGetter(getter2),
				mockGetter(getter3),
			},
		},
	}
	key := "SHA256E-s9--c6640f901fb5766abf2f6a5fd33b1ebaf6ade16544d068a2c92c4eb0426f33b3.mp4"
	tmpFh, err := os.CreateTemp(t.TempDir(), key)
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(tmpFh.Name())
	err = ai.Retrieve(ma, key, tmpFh.Name())
	if err != nil {
		t.Errorf("expected Retrieve not to return an error; got %v", err)
	}
	if getStateCalled != 1 {
		t.Errorf("expected GetState to be called once; it was called %d times", getStateCalled)
	}
	if get1Called != 1 {
		t.Errorf("expected Get[0] to be called once; it was called %d times", get1Called)
	}
	if get2Called != 1 {
		t.Errorf("expected Get[1] to be called once; it was called %d times", get2Called)
	}
	if get3Called != 1 {
		t.Errorf("expected Get[2] to be called once; it was called %d times", get3Called)
	}
	if progress != 9 {
		t.Errorf("expected progress to be 9 when Retrieve returns; it is %d", progress)
	}
	if _, err := tmpFh.Seek(0, io.SeekStart); err != nil {
		t.Error(err)
	}
	bs, err := io.ReadAll(tmpFh)
	if err != nil {
		t.Error(err)
	}
	if len(bs) != 9 {
		t.Errorf("expected to have written 9 bytes to temp file; wrote %s", bs)
	}
}

func TestExtractCID(t *testing.T) {
	for u, c := range map[string]string{
		"https://bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe.ipfs.dweb.link/": "bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe",
		"https://dweb.link/ipfs/bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe":  "bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe",
		"tel:bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe":                     "",
		"ipfs:bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe":                    "bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe",
		"ipfs://bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe":                  "bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe",
		"ipfs://bafybeibwzifw52ttrkqlikfz-ext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe":                 "",
		"shsd8fhs8hs8hakl": "",
		"":                 "",
		"bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe":        "",
		"http://ipfs.io/ipfs/QmbWqxBEKC3P8tqsKc98xmWNzrzDtRLMiMPL8wBuTGsMnR": "QmbWqxBEKC3P8tqsKc98xmWNzrzDtRLMiMPL8wBuTGsMnR",
	} {
		gotCID, err := extractCID(u)
		if c == "" {
			if err == nil {
				t.Errorf("expected error for %s; got nil", u)
			}
			continue
		}
		if err != nil {
			t.Errorf("expected no error when parsing %s; got %v", u, err)
		}
		if gotCID != c {
			t.Errorf("expected CID %s from %s; got %s", c, u, gotCID)
		}
	}
}

type urlsCids struct {
	urls []string
	cids []string
}

func TestEachCID(t *testing.T) {
	for _, tc := range []urlsCids{
		{urls: []string{}, cids: []string{}},
		{urls: []string{"https://example.net", "https://bbc.co.uk/news"}, cids: []string{}},
		{urls: []string{"https://bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe.ipfs.dweb.link/"}, cids: []string{"bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe"}},
		{urls: []string{"https://bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe.ipfs.dweb.link/", "https://ipfs.io/ipfs/bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe"}, cids: []string{"bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe"}},
		{urls: []string{"ipfs://bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe", "ipfs://bafybeibwzifw52ttrkqlikfz-ext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe", "https://example.com/", "http://ipfs.io"}, cids: []string{"bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe"}},
		{urls: []string{"http://ipfs.io/ipfs/QmbWqxBEKC3P8tqsKc98xmWNzrzDtRLMiMPL8wBuTGsMnR", "https://bafybeiemxf5abjwjbikoz4mc3a3dla6ual3jsgpdr4cjr3oz3evfyavhwq.ipfs.cf-ipfs.com/", "https://cloudflare-ipfs.com/ipfs/QmXoypizjW3WknFiJnKLwHCnL72vedxjQkDDP1mXWo6uco"}, cids: []string{"QmbWqxBEKC3P8tqsKc98xmWNzrzDtRLMiMPL8wBuTGsMnR", "bafybeiemxf5abjwjbikoz4mc3a3dla6ual3jsgpdr4cjr3oz3evfyavhwq", "QmXoypizjW3WknFiJnKLwHCnL72vedxjQkDDP1mXWo6uco"}},
		{urls: []string{"https://cloudflare-ipfs.com/ipfs/QmXoypizjW3WknFiJnKLwHCnL72vedxjQkDDP1mXWo6uco/wiki/"}, cids: []string{"QmXoypizjW3WknFiJnKLwHCnL72vedxjQkDDP1mXWo6uco"}},
	} {
		got := eachCID(tc.urls)
		if !cmp.Equal(got, tc.cids) {
			t.Errorf("expected %s to yield %s; got %s", tc.urls, tc.cids, got)
		}
	}
}

type client struct{}

var _ IPFSRemote = (*client)(nil)

func (c *client) Upload(context.Context, fs.File, *progress.Tracker) (cid.Cid, error) {
	return cid.Undef, nil
}
func (c *client) IsPresent(context.Context, cid.Cid) (bool, error) { return false, nil }
func (c *client) Unpin(context.Context, cid.Cid) error             { return nil }
func (c *client) Pin(context.Context, cid.Cid, string) error       { return nil }

func TestCheckPrepared(t *testing.T) {
	for ai, err := range map[*AI]error{
		{I: nil}:         ErrUnprepared,
		{}:               ErrUnprepared,
		{I: new(client)}: nil,
	} {
		gotErr := ai.checkPrepared()
		if gotErr != err {
			t.Errorf("expected gotErr == %v; gotErr == %v", err, gotErr)
		}
	}
}
