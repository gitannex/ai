package estuary

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"strings"

	"ai.camelcase.link/internal/multiconc"
	"ai.camelcase.link/internal/progress"

	"github.com/ipfs/go-cid"
)

// TODO: A lot of this code duplicates that in ../pinata/upload.go,
// intentionally. I've yet to merge it because I'm not sure
// exactly how to.

type uploader struct {
	endpoint   string
	ua         *http.Client
	jwt        string
	mcr        multiconc.Reader
	pipeReader io.ReadCloser
}

// fileFieldName is the name of the field in the multipart request
// whose contents is the file to be uploaded
const fileFieldName = "data"

// TODO: Warn if CID=bafkqaaa (0-length file)?
func (up *uploader) Upload(ctx context.Context, fi fs.File, pro *progress.Tracker) (cid.Cid, error) {
	st, err := fi.Stat()
	if err != nil {
		return cid.Undef, fmt.Errorf("can't stat fi: %w", err)
	}
	if !st.Mode().IsRegular() {
		return cid.Undef, fmt.Errorf("bad file mode, %s; only regular files can be uploaded", st.Mode())
	}
	up.mcr = multiconc.Reader{
		File:      fi,
		FileInfo:  st,
		FileField: fileFieldName,
		Progress:  pro,
	}
	up.pipeReader = up.mcr.Start()
	// the error from up.pipeReader.Close() is checked in readResponse; this
	// defer is a failsafe
	defer up.pipeReader.Close()
	c, err := up.doUpload(ctx)
	if err != nil {
		return cid.Undef, fmt.Errorf("doUpload: %w", err)
	}
	return c, nil
}

func (up *uploader) doUpload(ctx context.Context) (cid.Cid, error) {
	req, err := up.buildRequest(ctx)
	if err != nil {
		return cid.Undef, fmt.Errorf("buildRequest: can't construct HTTP request: %w", err)
	}
	res, err := up.ua.Do(req)
	if err != nil {
		return cid.Undef, fmt.Errorf("ua.Do: making request failed: %w", err)
	}
	// the error from res.Body.Close() is checked in readResponse();
	// this defer is a failsafe
	defer res.Body.Close()
	c, err := up.readResponse(res)
	if err != nil {
		return cid.Undef, fmt.Errorf("readResponse: %w", err)
	}
	return c, nil
}

func (up *uploader) endpointURL() string {
	return strings.TrimSuffix(up.endpoint, "/") + "/content/add"
}

func (up *uploader) buildRequest(ctx context.Context) (*http.Request, error) {
	req, err := http.NewRequestWithContext(ctx, "POST", up.endpointURL(), up.pipeReader)
	if err != nil {
		return nil, fmt.Errorf("can't construct HTTP request: %w", err)
	}
	req.Header.Set("Transfer-Encoding", "chunked")
	req.Header.Set("Content-Type", up.mcr.ContentType())
	req.Header.Set("Authorization", "Bearer "+up.jwt)
	req.Header.Set("Accept", "application/json")
	return req, nil
}

const maxResBodySz = 1024 * 1024

func (up *uploader) readResponse(res *http.Response) (cid.Cid, error) {
	body, err := io.ReadAll(&io.LimitedReader{N: maxResBodySz, R: res.Body})
	if err != nil {
		return cid.Undef, fmt.Errorf("io.ReadAll: can't read response body: %w", err)
	}
	if err := res.Body.Close(); err != nil {
		return cid.Undef, fmt.Errorf("can't close HTTP response body: %w", err)
	}
	if err := up.pipeReader.Close(); err != nil {
		return cid.Undef, fmt.Errorf("can't close multipart pipe: %w", err)
	}
	c, err := up.handleResponse(body, res)
	if err != nil {
		return cid.Undef, fmt.Errorf("handleResponse: %w", err)
	}
	return c, nil
}

// uploadError is the JSON response from the Estuary endpoint on error.
type uploadError struct {
	JSON    []byte
	Status  string
	Details string `json:"details"`
	ErrorID string `json:"error"`
}

func (ue *uploadError) Error() string {
	if err := json.Unmarshal(ue.JSON, ue); err != nil {
		return fmt.Sprintf("uploadError: status: %s (can't unmarshal error message: %v)", ue.Status, err)
	}
	return fmt.Sprintf("%s: %s (%s)", ue.ErrorID, ue.Details, ue.Status)
}

func (up *uploader) handleResponse(body []byte, res *http.Response) (cid.Cid, error) {
	if res.StatusCode != http.StatusOK {
		if res.Header.Get("Content-Type") == "application/json" {
			return cid.Undef, &uploadError{JSON: body, Status: res.Status}
		}
		return cid.Undef, fmt.Errorf("bad status (%s): %s", res.Status, body)
	}
	c, err := up.parseResponse(body)
	if err != nil {
		return cid.Undef, fmt.Errorf("parseResponse: %w", err)
	}
	return c, nil
}

// uploadResponse is the JSON returned on success
type uploadResponse struct {
	CID string `json:"cid"`
}

func (up *uploader) parseResponse(body []byte) (cid.Cid, error) {
	ur := new(uploadResponse)
	if err := json.Unmarshal(body, ur); err != nil {
		return cid.Undef, fmt.Errorf("can't unmarshal JSON from response: %w", err)
	}
	c, err := cid.Decode(ur.CID)
	if err != nil {
		return cid.Undef, fmt.Errorf("can't decode CID from %s: %w", body, err)
	}
	return c, nil
}
