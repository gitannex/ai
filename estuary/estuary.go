// estuary allows Git Annex remotes to work with the Estuary service
package estuary

import (
	"context"
	"errors"
	"fmt"
	"io/fs"
	"net/http"

	"ai.camelcase.link"
	"ai.camelcase.link/internal/progress"
	"github.com/ipfs/go-cid"
)

// TODO: Consider https://api.estuary.tech/gw/ipfs/$cid for retrievals

// Client is a client for the Estuary API for use by Git Annex remotes.
type Client struct {
	rp *ai.RemotePinner
	config
}

type config struct {
	ua             *http.Client
	jwt            string
	endpoint       string
	psaEndpoint    string
	uploadEndpoint string
	repoUUID       string
}

// NewOpt is an option for the New() function.
type NewOpt func(options *config) error

// WithToken is an option for the New() function which sets the JSON Web Token to authenticate with the endpoint.
func WithToken(tok string) NewOpt {
	return func(cfg *config) error {
		cfg.jwt = tok
		return nil
	}
}

// WithHTTPClient is an option for the New() function which sets the http.Client with which to access the endpoint. By default, a new retryablehttp.Client is used.
func WithHTTPClient(c *http.Client) NewOpt {
	return func(cfg *config) error {
		if c != nil {
			cfg.ua = c
		}
		return nil
	}
}

// DefaultEndpoint is the default base URL for the Estuary API.
const DefaultEndpoint = "https://api.estuary.tech/"

// WithEndpoint is an option for the New() function which sets the HTTP endpoint for the Estuary API; by default DefaultEndpoint is used
func WithEndpoint(ep string) NewOpt {
	return func(cfg *config) error {
		if ep != "" {
			cfg.endpoint = ep
		}
		return nil
	}
}

// DefaultPSAEndpoint is the default base URL for the Estuary Pinning Service API.
const DefaultPSAEndpoint = DefaultEndpoint + "pinning"

// WithPSAEndpoint is an option for the New() function which sets the HTTP endpoint for the Estuary PSA API; by default DefaultPSAEndpoint is used
func WithPSAEndpoint(ep string) NewOpt {
	return func(cfg *config) error {
		if ep != "" {
			cfg.psaEndpoint = ep
		}
		return nil
	}
}

// DefaultUploadEndpoint is the default base URL for the Estuary upload endpoint
const DefaultUploadEndpoint = "https://upload.estuary.tech/"

// WithUploadEndpoint is an option for the New() function which sets the HTTP endpoint for the Estuary upload API; by default DefaultUploadEndpoint is used
func WithUploadEndpoint(ep string) NewOpt {
	return func(cfg *config) error {
		if ep != "" {
			cfg.uploadEndpoint = ep
		}
		return nil
	}
}

func New(opts ...NewOpt) (*Client, error) {
	cl := Client{config: config{
		endpoint:       DefaultEndpoint,
		psaEndpoint:    DefaultPSAEndpoint,
		uploadEndpoint: DefaultUploadEndpoint,
		// TODO: We always overwrite this UA; should we construct it at a higher level?
		ua: http.DefaultClient,
	}}
	for _, opt := range opts {
		if err := opt(&cl.config); err != nil {
			return nil, err
		}
	}
	if cl.config.jwt == "" {
		return nil, errors.New("JWT not set")
	}
	cl.rp = ai.NewRemotePinner(cl.psaEndpoint, cl.jwt, cl.ua)
	return &cl, nil
}

// Upload stores the contents of fi on Estuary, returning the
// CID. fi & pro should be closed by the caller.
func (cl *Client) Upload(ctx context.Context, fi fs.File, pro *progress.Tracker) (cid.Cid, error) {
	up := &uploader{
		jwt:      cl.jwt,
		ua:       cl.ua,
		endpoint: cl.uploadEndpoint,
	}
	c, err := up.Upload(ctx, fi, pro)
	if err != nil {
		return c, fmt.Errorf("Upload: %w", err)
	}
	return c, nil
}

func (cl *Client) IsPresent(ctx context.Context, c cid.Cid) (bool, error) {
	return cl.rp.IsPinned(ctx, c)
}

func (cl *Client) Pin(ctx context.Context, c cid.Cid, key string) error {
	return cl.rp.Pin(ctx, c, key)
}

func (cl *Client) Unpin(ctx context.Context, c cid.Cid) error {
	return cl.rp.Unpin(ctx, c)
}

func (cl *Client) Equal(other *Client) bool {
	if cl == nil || other == nil {
		return false
	}
	return cl.config == other.config
}
