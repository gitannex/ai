package estuary

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
	"time"

	"ai.camelcase.link/internal/progress"
	"github.com/ipfs/go-cid"
)

func setupClient(t *testing.T, opts ...NewOpt) *Client {
	t.Helper()
	cl, err := New(append([]NewOpt{WithToken("jwt")}, opts...)...)
	if err != nil {
		t.Fatal(err)
	}
	return cl
}

func TestUploadRequest(t *testing.T) {
	t.Parallel()
	var req *http.Request
	var body []byte
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if req != nil {
			t.Errorf("expected only one request; also saw: %v", req)
		}
		req = r
		var err error
		body, err = io.ReadAll(r.Body)
		if err != nil {
			panic(err)
		}
		fmt.Fprint(w, `{"cid":"bafkqaetfon2hkylspexhizldnaqhizltoqfa","estuaryId":28730592,"providers":["/ip4/3.134.223.177/tcp/6745/p2p/12D3KooWN8vAoGd6eurUSidcpLYguQiGZwt4eVgDvbgaS7kiGTup"]}`)
	}))
	defer ts.Close()
	es := setupClient(t, WithUploadEndpoint(ts.URL))
	const fixtureFn = "../testdata/estuary"
	fh, err := os.Open(fixtureFn)
	if err != nil {
		t.Fatal(err)
	}
	defer fh.Close()

	_, _ = es.Upload(context.Background(), fh, &progress.Tracker{})
	if req.URL.Path != "/content/add" {
		t.Errorf("expected URL path to be /content/add; got %s", req.URL.Path)
	}
	if req.Method != "POST" {
		t.Errorf("expected method to be POST; got %s", req.Method)
	}
	ct := req.Header.Get("Content-Type")
	if !strings.HasPrefix(ct, "multipart/form-data") {
		t.Errorf("expected Content-Type to be multipart/form-data; got %s", ct)
	}
	_, boundary, foundBoundary := strings.Cut(ct, "=")
	if !foundBoundary {
		t.Errorf("expected Content-Type to contain multipart boundary; got %s", ct)
	}
	if bytes.Count(body, []byte(boundary)) != 2 {
		t.Errorf("expected 2 occurrences of boundary, %s, in body; body=%s", boundary, body)
	}
	if !bytes.Contains(body, []byte(`form-data; name="data"; filename="estuary"`)) {
		t.Errorf("expected request body to contain correct filename and field name; got %s", body)
	}
	fixBody, err := os.ReadFile(fixtureFn)
	if err != nil {
		t.Fatal(err)
	}
	if !bytes.Contains(body, fixBody) {
		t.Errorf("expected request body to contain uploaded file; got %s", body)
	}
}

func TestUploadLargerRequest(t *testing.T) {
	t.Parallel()
	var req *http.Request
	var body []byte
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if req != nil {
			t.Errorf("expected only one request; also saw: %v", req)
		}
		req = r
		var err error
		body, err = io.ReadAll(r.Body)
		if err != nil {
			panic(err)
		}
		fmt.Fprint(w, `{"cid":"bafybeib45e6kcu6ymln77yjvdievmegqzuteaqqpshhur7ysipyefttgcu","estuaryId":280592,"providers":["/ip4/3.134.223.177/tcp/6745/p2p/12D3KooWN8vAoGd6eurUSidcpLYguQiGZwt4eVgDvbgaS7kiGTup"]}`)
	}))
	defer ts.Close()
	es := setupClient(t, WithUploadEndpoint(ts.URL))
	const fixtureFn = "../testdata/JSB_BWV1047_1.xml"
	fh, err := os.Open(fixtureFn)
	if err != nil {
		t.Fatal(err)
	}
	defer fh.Close()

	_, _ = es.Upload(context.Background(), fh, &progress.Tracker{})
	if req.URL.Path != "/content/add" {
		t.Errorf("expected URL path to be /content/add; got %s", req.URL.Path)
	}
	if req.Method != "POST" {
		t.Errorf("expected method to be POST; got %s", req.Method)
	}
	ct := req.Header.Get("Content-Type")
	if !strings.HasPrefix(ct, "multipart/form-data") {
		t.Errorf("expected Content-Type to be multipart/form-data; got %s", ct)
	}
	_, boundary, foundBoundary := strings.Cut(ct, "=")
	if !foundBoundary {
		t.Errorf("expected Content-Type to contain multipart boundary; got %s", ct)
	}
	if bytes.Count(body, []byte(boundary)) != 2 {
		t.Errorf("expected 2 occurrences of boundary, %s, in body; body=%s", boundary, body)
	}
	if !bytes.Contains(body, []byte(`form-data; name="data"; filename="JSB_BWV1047_1.xml"`)) {
		t.Errorf("expected request body to contain correct filename and field name; got %s", body)
	}
	fixBody, err := os.ReadFile(fixtureFn)
	if err != nil {
		t.Fatal(err)
	}
	if !bytes.Contains(body, fixBody) {
		t.Errorf("expected request body to contain uploaded file; got %s", body)
	}
}

func TestUploadResponseOK(t *testing.T) {
	t.Parallel()
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, `{"cid":"bafkqaetfon2hkylspexhizldnaqhizltoqfa","estuaryId":28730592,"providers":["/ip4/3.134.223.177/tcp/6745/p2p/12D3KooWN8vAoGd6eurUSidcpLYguQiGZwt4eVgDvbgaS7kiGTup"]}`)
	}))
	defer ts.Close()
	es := setupClient(t, WithUploadEndpoint(ts.URL))
	const fixtureFn = "../testdata/estuary"
	fh, err := os.Open(fixtureFn)
	if err != nil {
		t.Fatal(err)
	}
	defer fh.Close()

	c, err := es.Upload(context.Background(), fh, &progress.Tracker{})
	if err != nil {
		t.Errorf("expected to upload without error; got %v", err)
	}
	if c.String() != "bafkqaetfon2hkylspexhizldnaqhizltoqfa" {
		t.Errorf("expected returned CID to equal 'bafkqaetfon2hkylspexhizldnaqhizltoqfa'; got %v", c)
	}
}

func TestUploadHTMLResponse(t *testing.T) {
	t.Parallel()
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "text/html")
		w.WriteHeader(http.StatusBadGateway)
		fmt.Fprint(w, `<html>
<head><title>502 Bad Gateway</title></head>
<body>
<center><h1>502 Bad Gateway</h1></center>
<hr><center>nginx/1.18.0 (Ubuntu)</center>
</body>
</html>`)
	}))
	defer ts.Close()
	es := setupClient(t, WithUploadEndpoint(ts.URL))
	const fixtureFn = "../testdata/estuary"
	fh, err := os.Open(fixtureFn)
	if err != nil {
		t.Fatal(err)
	}
	defer fh.Close()

	c, err := es.Upload(context.Background(), fh, &progress.Tracker{})
	if err == nil {
		t.Errorf("expected to upload with error; got %v", err)
	}
	if c != cid.Undef {
		t.Errorf("expected returned CID to be undefined; got %v", c)
	}
}

func TestUploadJSONError(t *testing.T) {
	t.Parallel()
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprint(w, `{"details":"token for user 3410 expired 2022-02-05 16:32:54.459043 +0000 UTC","error":"ERR_TOKEN_EXPIRED"}`)
	}))
	defer ts.Close()
	es := setupClient(t, WithUploadEndpoint(ts.URL))
	const fixtureFn = "../testdata/estuary"
	fh, err := os.Open(fixtureFn)
	if err != nil {
		t.Fatal(err)
	}
	defer fh.Close()

	c, err := es.Upload(context.Background(), fh, &progress.Tracker{})
	if err == nil {
		t.Errorf("expected to upload with error; got %v", err)
	}
	if c != cid.Undef {
		t.Errorf("expected returned CID to be undefined; got %v", c)
	}
}

func TestIsPresentOK(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafkreifddvxbktiwdckx6scmot3swvy7xcvormsgij7pglxiv524zxxmhq")
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/pinning/pins" {
			t.Errorf("expected path: /pinning/pins ; got %s", r.URL.Path)
		}
		if m := map[string][]string(r.URL.Query()); len(m) != 3 {
			t.Errorf("expected three query params; got %v", m)
		}
		if got := r.URL.Query().Get("cid"); got != testCID.String() {
			t.Errorf("expected cid=%s; got cid=%s", testCID.String(), got)
		}
		if got := r.URL.Query().Get("status"); got != "pinned" {
			t.Errorf("expected status=pinned; got status=%s", got)
		}
		if got := r.URL.Query().Get("limit"); got != "1" {
			t.Errorf("expected limit=1; got limit=%s", got)
		}
		if r.Method != "GET" {
			t.Errorf("expected method of GET; got %s", r.Method)
		}
		w.Header().Add("Content-Type", "application/json; charset=UTF-8")
		fmt.Fprint(w, `{"count":1,"results":[{"requestid":"28837281","status":"pinned","created":"2022-05-09T08:32:25.047461Z","pin":{"cid":"bafkreifddvxbktiwdckx6scmot3swvy7xcvormsgij7pglxiv524zxxmhq","name":"SHA256E-s1048575--a31d6e154d1618957f484c74f72b571fb8aae8b246427ef32ee8af75ccdeec3c.this-is-a-test-key","origins":null,"meta":null},"delegates":["/ip4/3.134.223.177/tcp/6745/p2p/12D3KooWN8vAoGd6eurUSidcpLYguQiGZwt4eVgDvbgaS7kiGTup"],"info":null}]}`)
	}))
	defer ts.Close()
	es := setupClient(t, WithPSAEndpoint(ts.URL+"/pinning"))
	is, err := es.IsPresent(context.Background(), testCID)
	if err != nil {
		t.Errorf("expected nil error; got %v", err)
	}
	if !is {
		t.Errorf("expected is to be true; got false")
	}
}

func TestIsPresentNot(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafkreifddvxbktiwdckx6scmot3swvy7xcvormsgij7pglxiv524zxxmhq")
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/pinning/pins" {
			t.Errorf("expected path: /pinning/pins ; got %s", r.URL.Path)
		}
		if got := r.URL.Query().Get("cid"); got != testCID.String() {
			t.Errorf("expected cid=%s; got cid=%s", testCID.String(), got)
		}
		if got := r.URL.Query().Get("status"); got != "pinned" {
			t.Errorf("expected status=pinned; got status=%s", got)
		}
		if got := r.URL.Query().Get("limit"); got != "1" {
			t.Errorf("expected limit=1; got limit=%s", got)
		}
		if r.Method != "GET" {
			t.Errorf("expected method of GET; got %s", r.Method)
		}
		w.Header().Add("Content-Type", "application/json; charset=UTF-8")
		fmt.Fprint(w, `{"count":0,"results":[]}`)
	}))
	defer ts.Close()
	es := setupClient(t, WithPSAEndpoint(ts.URL+"/pinning"))
	is, err := es.IsPresent(context.Background(), testCID)
	if err != nil {
		t.Errorf("expected nil error; got %v", err)
	}
	if is {
		t.Errorf("expected is to be false; got true")
	}
}

func TestIsPresentJSONError(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafkreifddvxbktiwdckx6scmot3swvy7xcvormsgij7pglxiv524zxxmhq")
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusForbidden)
		w.Header().Add("Content-Type", "application/json; charset=UTF-8")
		fmt.Fprint(w, `{"error":{"reason":"ERR_INVALID_TOKEN"}}`)
	}))
	defer ts.Close()
	es := setupClient(t, WithPSAEndpoint(ts.URL+"/pinning"))
	_, err := es.IsPresent(context.Background(), testCID)
	if err == nil {
		t.Error("expected error; got nil")
	}
}

// TODO: Test Unpin when it needs to recurse
func TestUnpinOK(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafkreifddvxbktiwdckx6scmot3swvy7xcvormsgij7pglxiv524zxxmhq")
	var reqNum int
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum += 1
		}()
		switch reqNum {
		case 0:
			if r.URL.Path != "/pinning/pins" {
				t.Errorf("expected path: /pinning/pins ; got %s", r.URL.Path)
			}
			if m := map[string][]string(r.URL.Query()); len(m) != 3 {
				t.Errorf("expected two query params; got %v", m)
			}
			if got := r.URL.Query().Get("cid"); got != testCID.String() {
				t.Errorf("expected cid=%s; got cid=%s", testCID.String(), got)
			}
			if got := r.URL.Query().Get("status"); got != "failed,pinned,pinning,queued" {
				t.Errorf("expected status=failed,pinned,pinning,queued; got status=%s", got)
			}
			if got := r.URL.Query().Get("limit"); got != "1000" {
				t.Errorf("expected limit=1000; got limit=%s", got)
			}
			if r.Method != "GET" {
				t.Errorf("expected method of GET; got %s", r.Method)
			}
			w.Header().Set("Content-Type", "application/json; charset=UTF-8")
			fmt.Fprint(w, `{"count":2,"results":[{"requestid":"28863223","status":"pinning","created":"2022-05-11T03:05:32.538405252Z","pin":{"cid":"bafkreifddvxbktiwdckx6scmot3swvy7xcvormsgij7pglxiv524zxxmhq","name":"fae13c5af7fbbd2f591c069aa920b056aeff2c4bcdfac115440d134f5977ed80.mp3","origins":null,"meta":null},"delegates":["/ip4/172.31.32.185/tcp/6745/p2p/12D3KooWLV128pddyvoG6NBvoZw7sSrgpMTPtjnpu3mSmENqhtL7","/ip4/127.0.0.1/tcp/6745/p2p/12D3KooWLV128pddyvoG6NBvoZw7sSrgpMTPtjnpu3mSmENqhtL7","/ip4/172.31.32.185/udp/6746/quic/p2p/12D3KooWLV128pddyvoG6NBvoZw7sSrgpMTPtjnpu3mSmENqhtL7","/ip4/127.0.0.1/udp/6746/quic/p2p/12D3KooWLV128pddyvoG6NBvoZw7sSrgpMTPtjnpu3mSmENqhtL7","/ip4/35.74.45.12/udp/6746/quic/p2p/12D3KooWLV128pddyvoG6NBvoZw7sSrgpMTPtjnpu3mSmENqhtL7","/ip4/35.74.45.12/tcp/6745/p2p/12D3KooWLV128pddyvoG6NBvoZw7sSrgpMTPtjnpu3mSmENqhtL7"],"info":null},{"requestid":"28863214","status":"pinned","created":"2022-05-11T03:05:19.827049011Z","pin":{"cid":"bafkreifddvxbktiwdckx6scmot3swvy7xcvormsgij7pglxiv524zxxmhq","name":"8389d2f4742308efdc5160d1d1b1c567e5ce60d35bf0f86057034cfdeaa314e5.mp3","origins":null,"meta":null},"delegates":["/ip4/172.31.32.185/tcp/6745/p2p/12D3KooWLV128pddyvoG6NBvoZw7sSrgpMTPtjnpu3mSmENqhtL7","/ip4/127.0.0.1/tcp/6745/p2p/12D3KooWLV128pddyvoG6NBvoZw7sSrgpMTPtjnpu3mSmENqhtL7","/ip4/172.31.32.185/udp/6746/quic/p2p/12D3KooWLV128pddyvoG6NBvoZw7sSrgpMTPtjnpu3mSmENqhtL7","/ip4/127.0.0.1/udp/6746/quic/p2p/12D3KooWLV128pddyvoG6NBvoZw7sSrgpMTPtjnpu3mSmENqhtL7","/ip4/35.74.45.12/udp/6746/quic/p2p/12D3KooWLV128pddyvoG6NBvoZw7sSrgpMTPtjnpu3mSmENqhtL7","/ip4/35.74.45.12/tcp/6745/p2p/12D3KooWLV128pddyvoG6NBvoZw7sSrgpMTPtjnpu3mSmENqhtL7"],"info":null}]}`)
			return
		case 1:
			if r.URL.Path != "/pinning/pins/28863223" {
				t.Errorf("expected path: /pinning/pins/28863223 ; got %s", r.URL.Path)
			}
			if r.Method != "DELETE" {
				t.Errorf("expected method of DELETE; got %s", r.Method)
			}
			w.WriteHeader(http.StatusAccepted)
		case 2:
			if r.URL.Path != "/pinning/pins/28863214" {
				t.Errorf("expected path: /pinning/pins/28863214 ; got %s", r.URL.Path)
			}
			if r.Method != "DELETE" {
				t.Errorf("expected method of DELETE; got %s", r.Method)
			}
			w.WriteHeader(http.StatusAccepted)
		}
	}))
	defer ts.Close()
	es := setupClient(t, WithPSAEndpoint(ts.URL+"/pinning"))
	err := es.Unpin(context.Background(), testCID)
	if err != nil {
		t.Errorf("expected nil error; got %v", err)
	}
	if reqNum != 3 {
		t.Errorf("expected 3 requests; got %d", reqNum)
	}
}

func TestUnpinNoPins(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafkreifddvxbktiwdckx6scmot3swvy7xcvormsgij7pglxiv524zxxmhq")
	var reqNum int
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum += 1
		}()

		if r.URL.Path != "/pinning/pins" {
			t.Errorf("expected path: /pinning/pins ; got %s", r.URL.Path)
		}
		if m := map[string][]string(r.URL.Query()); len(m) != 3 {
			t.Errorf("expected two query params; got %v", m)
		}
		if got := r.URL.Query().Get("cid"); got != testCID.String() {
			t.Errorf("expected cid=%s; got cid=%s", testCID.String(), got)
		}
		if got := r.URL.Query().Get("status"); got != "failed,pinned,pinning,queued" {
			t.Errorf("expected status=failed,pinned,pinning,queued; got status=%s", got)
		}
		if got := r.URL.Query().Get("limit"); got != "1000" {
			t.Errorf("expected limit=1000; got limit=%s", got)
		}
		if r.Method != "GET" {
			t.Errorf("expected method of GET; got %s", r.Method)
		}
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		fmt.Fprint(w, `{"count":0,"results":[]}`)
	}))
	defer ts.Close()
	es := setupClient(t, WithPSAEndpoint(ts.URL+"/pinning"))
	err := es.Unpin(context.Background(), testCID)
	if err != nil {
		t.Errorf("expected nil error; got %v", err)
	}
	if reqNum != 1 {
		t.Errorf("expected 1 request; got %d", reqNum)
	}
}

func TestUnpinListError(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafkreifddvxbktiwdckx6scmot3swvy7xcvormsgij7pglxiv524zxxmhq")
	var reqNum int
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum += 1
		}()

		if r.URL.Path != "/pinning/pins" {
			t.Errorf("expected path: /pinning/pins ; got %s", r.URL.Path)
		}
		if m := map[string][]string(r.URL.Query()); len(m) != 3 {
			t.Errorf("expected two query params; got %v", m)
		}
		if got := r.URL.Query().Get("cid"); got != testCID.String() {
			t.Errorf("expected cid=%s; got cid=%s", testCID.String(), got)
		}
		if got := r.URL.Query().Get("status"); got != "failed,pinned,pinning,queued" {
			t.Errorf("expected status=failed,pinned,pinning,queued; got status=%s", got)
		}
		if got := r.URL.Query().Get("limit"); got != "1000" {
			t.Errorf("expected limit=1000; got limit=%s", got)
		}
		if r.Method != "GET" {
			t.Errorf("expected method of GET; got %s", r.Method)
		}
		w.WriteHeader(413)
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		fmt.Fprint(w, `{
  "error": {
    "reason": "CUSTOM_ERROR_CODE_FOR_MACHINES",
    "details": "Optional explanation for humans with more details"
  }
}`)
	}))
	defer ts.Close()
	es := setupClient(t, WithPSAEndpoint(ts.URL+"/pinning"))
	err := es.Unpin(context.Background(), testCID)
	if err == nil {
		t.Errorf("expected error; got nil")
	}
	if reqNum != 1 {
		t.Errorf("expected 1 request; got %d", reqNum)
	}
}

func TestUnpinDeleteError(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafkreifddvxbktiwdckx6scmot3swvy7xcvormsgij7pglxiv524zxxmhq")
	var reqNum int
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum += 1
		}()
		switch reqNum {
		case 0:
			if r.URL.Path != "/pinning/pins" {
				t.Errorf("expected path: /pinning/pins ; got %s", r.URL.Path)
			}
			if m := map[string][]string(r.URL.Query()); len(m) != 3 {
				t.Errorf("expected two query params; got %v", m)
			}
			if got := r.URL.Query().Get("cid"); got != testCID.String() {
				t.Errorf("expected cid=%s; got cid=%s", testCID.String(), got)
			}
			if got := r.URL.Query().Get("status"); got != "failed,pinned,pinning,queued" {
				t.Errorf("expected status=failed,pinned,pinning,queued; got status=%s", got)
			}
			if got := r.URL.Query().Get("limit"); got != "1000" {
				t.Errorf("expected limit=1000; got limit=%s", got)
			}
			if r.Method != "GET" {
				t.Errorf("expected method of GET; got %s", r.Method)
			}
			w.Header().Set("Content-Type", "application/json; charset=UTF-8")
			fmt.Fprint(w, `{"count":2,"results":[{"requestid":"28863223","status":"pinning","created":"2022-05-11T03:05:32.538405252Z","pin":{"cid":"bafkreifddvxbktiwdckx6scmot3swvy7xcvormsgij7pglxiv524zxxmhq","name":"fae13c5af7fbbd2f591c069aa920b056aeff2c4bcdfac115440d134f5977ed80.mp3","origins":null,"meta":null},"delegates":["/ip4/172.31.32.185/tcp/6745/p2p/12D3KooWLV128pddyvoG6NBvoZw7sSrgpMTPtjnpu3mSmENqhtL7","/ip4/127.0.0.1/tcp/6745/p2p/12D3KooWLV128pddyvoG6NBvoZw7sSrgpMTPtjnpu3mSmENqhtL7","/ip4/172.31.32.185/udp/6746/quic/p2p/12D3KooWLV128pddyvoG6NBvoZw7sSrgpMTPtjnpu3mSmENqhtL7","/ip4/127.0.0.1/udp/6746/quic/p2p/12D3KooWLV128pddyvoG6NBvoZw7sSrgpMTPtjnpu3mSmENqhtL7","/ip4/35.74.45.12/udp/6746/quic/p2p/12D3KooWLV128pddyvoG6NBvoZw7sSrgpMTPtjnpu3mSmENqhtL7","/ip4/35.74.45.12/tcp/6745/p2p/12D3KooWLV128pddyvoG6NBvoZw7sSrgpMTPtjnpu3mSmENqhtL7"],"info":null},{"requestid":"28863214","status":"pinned","created":"2022-05-11T03:05:19.827049011Z","pin":{"cid":"bafkreifddvxbktiwdckx6scmot3swvy7xcvormsgij7pglxiv524zxxmhq","name":"8389d2f4742308efdc5160d1d1b1c567e5ce60d35bf0f86057034cfdeaa314e5.mp3","origins":null,"meta":null},"delegates":["/ip4/172.31.32.185/tcp/6745/p2p/12D3KooWLV128pddyvoG6NBvoZw7sSrgpMTPtjnpu3mSmENqhtL7","/ip4/127.0.0.1/tcp/6745/p2p/12D3KooWLV128pddyvoG6NBvoZw7sSrgpMTPtjnpu3mSmENqhtL7","/ip4/172.31.32.185/udp/6746/quic/p2p/12D3KooWLV128pddyvoG6NBvoZw7sSrgpMTPtjnpu3mSmENqhtL7","/ip4/127.0.0.1/udp/6746/quic/p2p/12D3KooWLV128pddyvoG6NBvoZw7sSrgpMTPtjnpu3mSmENqhtL7","/ip4/35.74.45.12/udp/6746/quic/p2p/12D3KooWLV128pddyvoG6NBvoZw7sSrgpMTPtjnpu3mSmENqhtL7","/ip4/35.74.45.12/tcp/6745/p2p/12D3KooWLV128pddyvoG6NBvoZw7sSrgpMTPtjnpu3mSmENqhtL7"],"info":null}]}`)
			return
		case 1:
			if r.URL.Path != "/pinning/pins/28863223" {
				t.Errorf("expected path: /pinning/pins/28863223 ; got %s", r.URL.Path)
			}
			if r.Method != "DELETE" {
				t.Errorf("expected method of DELETE; got %s", r.Method)
			}
			w.WriteHeader(http.StatusInternalServerError)
			w.Header().Set("Content-Type", "application/json; charset=UTF-8")
			fmt.Fprint(w, `{
  "error": {
    "reason": "INTERNAL_SERVER_ERROR",
    "details": "Oops"
  }
}`)
		}
	}))
	defer ts.Close()
	es := setupClient(t, WithPSAEndpoint(ts.URL+"/pinning"))
	err := es.Unpin(context.Background(), testCID)
	if err == nil {
		t.Errorf("expected error; got nil")
	}
	if reqNum != 2 {
		t.Errorf("expected 2 requests; got %d", reqNum)
	}
}

type pinReq struct {
	CID  string `json:"cid"`
	Name string `json:"name"`
}

func TestPinImmediatelyPinned(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafkreifddvxbktiwdckx6scmot3swvy7xcvormsgij7pglxiv524zxxmhq")
	const keyName = "SHA265E-s28276--cca2c267410955230ed716fcd448cda0f4b466e81e725bd2c4bf5ab02e938204"
	var reqNum int
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum++
		}()
		if r.URL.Path != "/pinning/pins" {
			t.Errorf("expected path: /pinning/pins ; got %s", r.URL.Path)
		}
		if r.Method != "POST" {
			t.Errorf("expected method of POST; got %s", r.Method)
		}
		body, err := io.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			t.Fatal(err)
		}
		pr := new(pinReq)
		if err := json.Unmarshal(body, pr); err != nil {
			t.Fatal(err)
		}
		if pr.CID != testCID.String() {
			t.Errorf("expected request CID field to equal %s; it is %s", testCID, pr.CID)
		}
		if pr.Name != keyName {
			t.Errorf("expected request Name field to equal %s; it is %s", keyName, pr.Name)
		}

		w.Header().Add("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusAccepted)
		fmt.Fprint(w, `{
  "requestid": "UniqueIdOfPinRequest",
  "status": "pinned",
  "created": "2020-07-27T17:32:28Z",
  "pin": {
    "cid": "`+testCID.String()+`",
    "name": "`+keyName+`",
    "origins": [
      "/ip4/203.0.113.142/tcp/4001/p2p/QmSourcePeerId",
      "/ip4/203.0.113.114/udp/4001/quic/p2p/QmSourcePeerId"
    ],
    "meta": {
    }
  },
  "delegates": [
    "/ip4/203.0.113.1/tcp/4001/p2p/QmServicePeerId"
  ],
  "info": {
  }
}`)
	}))
	defer ts.Close()
	es := setupClient(t, WithPSAEndpoint(ts.URL+"/pinning"))
	err := es.Pin(context.Background(), testCID, keyName)
	if err != nil {
		t.Errorf("expected nil error; got %v", err)
	}
	if reqNum != 1 {
		t.Errorf("expected only one request; %d were made", reqNum)
	}
}

func TestPinQueuedThenPinned(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafkreifddvxbktiwdckx6scmot3swvy7xcvormsgij7pglxiv524zxxmhq")
	const keyName = "SHA265E-s28276--cca2c267410955230ed716fcd448cda0f4b466e81e725bd2c4bf5ab02e938204"
	var reqNum int
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum++
		}()
		if reqNum == 0 {
			if r.URL.Path != "/pinning/pins" {
				t.Errorf("expected path: /pinning/pins ; got %s", r.URL.Path)
			}
			if r.Method != "POST" {
				t.Errorf("expected method of POST; got %s", r.Method)
			}
			body, err := io.ReadAll(r.Body)
			defer r.Body.Close()
			if err != nil {
				t.Fatal(err)
			}
			pr := new(pinReq)
			if err := json.Unmarshal(body, pr); err != nil {
				t.Fatal(err)
			}
			if pr.CID != testCID.String() {
				t.Errorf("expected request CID field to equal %s; it is %s", testCID, pr.CID)
			}
			if pr.Name != keyName {
				t.Errorf("expected request Name field to equal %s; it is %s", keyName, pr.Name)
			}

			w.Header().Add("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusAccepted)
			fmt.Fprint(w, `{
  "requestid": "UniqueIdOfPinRequest",
  "status": "queued",
  "created": "2020-07-27T17:32:28Z",
  "pin": {
    "cid": "`+testCID.String()+`",
    "name": "`+keyName+`",
    "origins": [
      "/ip4/203.0.113.142/tcp/4001/p2p/QmSourcePeerId",
      "/ip4/203.0.113.114/udp/4001/quic/p2p/QmSourcePeerId"
    ],
    "meta": {
    }
  },
  "delegates": [
    "/ip4/203.0.113.1/tcp/4001/p2p/QmServicePeerId"
  ],
  "info": {
  }
}`)
		} else if reqNum < 4 {
			if r.Method != "GET" {
				t.Errorf("expected method GET for request %d; got %s", reqNum, r.Method)
			}
			if expectedPath := "/pinning/pins/UniqueIdOfPinRequest"; r.URL.Path != expectedPath {
				t.Errorf("expected path: %s for request %d; got %s", expectedPath, reqNum, r.URL.Path)
			}
			w.Header().Add("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusOK)
			fmt.Fprint(w, `{
  "requestid": "UniqueIdOfPinRequest",
  "status": "queued",
  "created": "2020-07-27T17:32:28Z",
  "pin": {
    "cid": "`+testCID.String()+`",
    "name": "`+keyName+`",
    "origins": [
      "/ip4/203.0.113.142/tcp/4001/p2p/QmSourcePeerId",
      "/ip4/203.0.113.114/udp/4001/quic/p2p/QmSourcePeerId"
    ],
    "meta": {
    }
  },
  "delegates": [
    "/ip4/203.0.113.1/tcp/4001/p2p/QmServicePeerId"
  ],
  "info": {
  }
}`)

		} else {
			if r.Method != "GET" {
				t.Errorf("expected method GET for request %d; got %s", reqNum, r.Method)
			}
			if expectedPath := "/pinning/pins/UniqueIdOfPinRequest"; r.URL.Path != expectedPath {
				t.Errorf("expected path: %s for request %d; got %s", expectedPath, reqNum, r.URL.Path)
			}
			w.Header().Add("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusOK)
			fmt.Fprint(w, `{
  "requestid": "UniqueIdOfPinRequest",
  "status": "pinned",
  "created": "2020-07-27T17:32:28Z",
  "pin": {
    "cid": "`+testCID.String()+`",
    "name": "`+keyName+`",
    "origins": [
      "/ip4/203.0.113.142/tcp/4001/p2p/QmSourcePeerId",
      "/ip4/203.0.113.114/udp/4001/quic/p2p/QmSourcePeerId"
    ],
    "meta": {
    }
  },
  "delegates": [
    "/ip4/203.0.113.1/tcp/4001/p2p/QmServicePeerId"
  ],
  "info": {
  }
}`)

		}
	}))
	defer ts.Close()
	es := setupClient(t, WithPSAEndpoint(ts.URL+"/pinning"))
	err := es.Pin(context.Background(), testCID, keyName)
	if err != nil {
		t.Errorf("expected nil error; got %v", err)
	}
	if reqNum != 5 {
		t.Errorf("expected 5 requests; %d were made", reqNum)
	}
}

func TestPinImmediatelyFailed(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafkreifddvxbktiwdckx6scmot3swvy7xcvormsgij7pglxiv524zxxmhq")
	const keyName = "SHA265E-s28276--cca2c267410955230ed716fcd448cda0f4b466e81e725bd2c4bf5ab02e938204"
	var reqNum int
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum++
		}()
		if r.URL.Path != "/pinning/pins" {
			t.Errorf("expected path: /pinning/pins ; got %s", r.URL.Path)
		}
		if r.Method != "POST" {
			t.Errorf("expected method of POST; got %s", r.Method)
		}
		body, err := io.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			t.Fatal(err)
		}
		pr := new(pinReq)
		if err := json.Unmarshal(body, pr); err != nil {
			t.Fatal(err)
		}
		if pr.CID != testCID.String() {
			t.Errorf("expected request CID field to equal %s; it is %s", testCID, pr.CID)
		}
		if pr.Name != keyName {
			t.Errorf("expected request Name field to equal %s; it is %s", keyName, pr.Name)
		}

		w.Header().Add("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusAccepted)
		fmt.Fprint(w, `{
  "requestid": "UniqueIdOfPinRequest",
  "status": "failed",
  "created": "2020-07-27T17:32:28Z",
  "pin": {
    "cid": "`+testCID.String()+`",
    "name": "`+keyName+`",
    "origins": [
      "/ip4/203.0.113.142/tcp/4001/p2p/QmSourcePeerId",
      "/ip4/203.0.113.114/udp/4001/quic/p2p/QmSourcePeerId"
    ],
    "meta": {
    }
  },
  "delegates": [
    "/ip4/203.0.113.1/tcp/4001/p2p/QmServicePeerId"
  ],
  "info": {
  }
}`)
	}))
	defer ts.Close()
	es := setupClient(t, WithPSAEndpoint(ts.URL+"/pinning"))
	err := es.Pin(context.Background(), testCID, keyName)
	if err == nil {
		t.Errorf("expected error; got nil")
	}
	if reqNum != 1 {
		t.Errorf("expected only one request; %d were made", reqNum)
	}
}

func TestPinQueuedThenFailed(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafkreifddvxbktiwdckx6scmot3swvy7xcvormsgij7pglxiv524zxxmhq")
	const keyName = "SHA265E-s28276--cca2c267410955230ed716fcd448cda0f4b466e81e725bd2c4bf5ab02e938204"
	var reqNum int
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum++
		}()
		if reqNum == 0 {
			if r.URL.Path != "/pinning/pins" {
				t.Errorf("expected path: /pinning/pins ; got %s", r.URL.Path)
			}
			if r.Method != "POST" {
				t.Errorf("expected method of POST; got %s", r.Method)
			}
			body, err := io.ReadAll(r.Body)
			defer r.Body.Close()
			if err != nil {
				t.Fatal(err)
			}
			pr := new(pinReq)
			if err := json.Unmarshal(body, pr); err != nil {
				t.Fatal(err)
			}
			if pr.CID != testCID.String() {
				t.Errorf("expected request CID field to equal %s; it is %s", testCID, pr.CID)
			}
			if pr.Name != keyName {
				t.Errorf("expected request Name field to equal %s; it is %s", keyName, pr.Name)
			}

			w.Header().Add("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusAccepted)
			fmt.Fprint(w, `{
  "requestid": "UniqueIdOfPinRequest",
  "status": "queued",
  "created": "2020-07-27T17:32:28Z",
  "pin": {
    "cid": "`+testCID.String()+`",
    "name": "`+keyName+`",
    "origins": [
      "/ip4/203.0.113.142/tcp/4001/p2p/QmSourcePeerId",
      "/ip4/203.0.113.114/udp/4001/quic/p2p/QmSourcePeerId"
    ],
    "meta": {
    }
  },
  "delegates": [
    "/ip4/203.0.113.1/tcp/4001/p2p/QmServicePeerId"
  ],
  "info": {
  }
}`)
		} else if reqNum < 4 {
			if r.Method != "GET" {
				t.Errorf("expected method GET for request %d; got %s", reqNum, r.Method)
			}
			if expectedPath := "/pinning/pins/UniqueIdOfPinRequest"; r.URL.Path != expectedPath {
				t.Errorf("expected path: %s for request %d; got %s", expectedPath, reqNum, r.URL.Path)
			}
			w.Header().Add("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusOK)
			fmt.Fprint(w, `{
  "requestid": "UniqueIdOfPinRequest",
  "status": "queued",
  "created": "2020-07-27T17:32:28Z",
  "pin": {
    "cid": "`+testCID.String()+`",
    "name": "`+keyName+`",
    "origins": [
      "/ip4/203.0.113.142/tcp/4001/p2p/QmSourcePeerId",
      "/ip4/203.0.113.114/udp/4001/quic/p2p/QmSourcePeerId"
    ],
    "meta": {
    }
  },
  "delegates": [
    "/ip4/203.0.113.1/tcp/4001/p2p/QmServicePeerId"
  ],
  "info": {
  }
}`)

		} else {
			if r.Method != "GET" {
				t.Errorf("expected method GET for request %d; got %s", reqNum, r.Method)
			}
			if expectedPath := "/pinning/pins/UniqueIdOfPinRequest"; r.URL.Path != expectedPath {
				t.Errorf("expected path: %s for request %d; got %s", expectedPath, reqNum, r.URL.Path)
			}
			w.Header().Add("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusOK)
			fmt.Fprint(w, `{
  "requestid": "UniqueIdOfPinRequest",
  "status": "failed",
  "created": "2020-07-27T17:32:28Z",
  "pin": {
    "cid": "`+testCID.String()+`",
    "name": "`+keyName+`",
    "origins": [
      "/ip4/203.0.113.142/tcp/4001/p2p/QmSourcePeerId",
      "/ip4/203.0.113.114/udp/4001/quic/p2p/QmSourcePeerId"
    ],
    "meta": {
    }
  },
  "delegates": [
    "/ip4/203.0.113.1/tcp/4001/p2p/QmServicePeerId"
  ],
  "info": {
  }
}`)

		}
	}))
	defer ts.Close()
	es := setupClient(t, WithPSAEndpoint(ts.URL+"/pinning"))
	err := es.Pin(context.Background(), testCID, keyName)
	if err == nil {
		t.Errorf("expected error; got nil")
	}
	if reqNum != 5 {
		t.Errorf("expected 5 requests; %d were made", reqNum)
	}
}

func TestPinQueuedTimeout(t *testing.T) {
	t.Parallel()
	testCID, _ := cid.Decode("bafkreifddvxbktiwdckx6scmot3swvy7xcvormsgij7pglxiv524zxxmhq")
	const keyName = "SHA265E-s28276--cca2c267410955230ed716fcd448cda0f4b466e81e725bd2c4bf5ab02e938204"
	var reqNum int
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum++
		}()
		if reqNum == 0 {
			if r.URL.Path != "/pinning/pins" {
				t.Errorf("expected path: /pinning/pins ; got %s", r.URL.Path)
			}
			if r.Method != "POST" {
				t.Errorf("expected method of POST; got %s", r.Method)
			}
			body, err := io.ReadAll(r.Body)
			defer r.Body.Close()
			if err != nil {
				t.Fatal(err)
			}
			pr := new(pinReq)
			if err := json.Unmarshal(body, pr); err != nil {
				t.Fatal(err)
			}
			if pr.CID != testCID.String() {
				t.Errorf("expected request CID field to equal %s; it is %s", testCID, pr.CID)
			}
			if pr.Name != keyName {
				t.Errorf("expected request Name field to equal %s; it is %s", keyName, pr.Name)
			}

			w.Header().Add("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusAccepted)
			fmt.Fprint(w, `{
  "requestid": "UniqueIdOfPinRequest",
  "status": "queued",
  "created": "2020-07-27T17:32:28Z",
  "pin": {
    "cid": "`+testCID.String()+`",
    "name": "`+keyName+`",
    "origins": [
      "/ip4/203.0.113.142/tcp/4001/p2p/QmSourcePeerId",
      "/ip4/203.0.113.114/udp/4001/quic/p2p/QmSourcePeerId"
    ],
    "meta": {
    }
  },
  "delegates": [
    "/ip4/203.0.113.1/tcp/4001/p2p/QmServicePeerId"
  ],
  "info": {
  }
}`)
		} else {
			if r.Method != "GET" {
				t.Errorf("expected method GET for request %d; got %s", reqNum, r.Method)
			}
			if expectedPath := "/pinning/pins/UniqueIdOfPinRequest"; r.URL.Path != expectedPath {
				t.Errorf("expected path: %s for request %d; got %s", expectedPath, reqNum, r.URL.Path)
			}
			w.Header().Add("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusOK)
			fmt.Fprint(w, `{
  "requestid": "UniqueIdOfPinRequest",
  "status": "queued",
  "created": "2020-07-27T17:32:28Z",
  "pin": {
    "cid": "`+testCID.String()+`",
    "name": "`+keyName+`",
    "origins": [
      "/ip4/203.0.113.142/tcp/4001/p2p/QmSourcePeerId",
      "/ip4/203.0.113.114/udp/4001/quic/p2p/QmSourcePeerId"
    ],
    "meta": {
    }
  },
  "delegates": [
    "/ip4/203.0.113.1/tcp/4001/p2p/QmServicePeerId"
  ],
  "info": {
  }
}`)

		}
	}))
	defer ts.Close()
	es := setupClient(t, WithPSAEndpoint(ts.URL+"/pinning"))
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(2))
	defer cancel()
	err := es.Pin(ctx, testCID, keyName)
	if err == nil {
		t.Errorf("expected error; got nil")
	}
	if !errors.Is(err, context.DeadlineExceeded) {
		t.Errorf("expected DeadlineExceeded error; got %v", err)
	}
	if reqNum < 3 {
		t.Errorf("expected more than 3 requests; %d were made", reqNum)
	}
}
