//go:build e2e

package estuary

import (
	"bytes"
	"context"
	"crypto/rand"
	"io"
	"io/fs"
	"net/http"
	"os"
	"testing"

	"ai.camelcase.link"
	"ai.camelcase.link/internal/progress"
	"github.com/dsnet/golib/memfile"
	"github.com/ipfs/go-cid"
)

func init() {
	if os.Getenv("ESTUARY_TOK") == "" {
		panic("ESTUARY_TOK unset")
	}
}

type loggingRdr struct {
	fs.File
	N int64
}

func (lr *loggingRdr) Read(p []byte) (int, error) {
	n, err := lr.File.Read(p)
	lr.N += int64(n)
	return n, err
}

func uploadRand(t *testing.T, cl *Client, tmpSz int64) cid.Cid {
	t.Helper()
	tmpFh, err := os.CreateTemp(t.TempDir(), "")
	if err != nil {
		t.Error(err)
	}
	if _, err := io.CopyN(tmpFh, rand.Reader, tmpSz); err != nil {
		t.Error(err)
	}
	if _, err := tmpFh.Seek(0, io.SeekStart); err != nil {
		t.Error(err)
	}
	lr := loggingRdr{File: tmpFh}
	var totalRead int64
	pro := &progress.Tracker{Fn: func(b int64) { totalRead = b }, FileSize: tmpSz}
	c, err := cl.Upload(context.Background(), &lr, pro)
	if err != nil {
		t.Errorf("expected to upload without error; got %v", err)
	}
	if err := tmpFh.Close(); err != nil {
		t.Errorf("expected to close temp file; got %v", err)
	}
	_ = pro.Close()
	if lr.N != tmpSz || totalRead != tmpSz {
		t.Errorf("expected to upload %d bytes; read %d; progress read %d", tmpSz, lr.N, totalRead)
	}
	return c
}

func TestUpload(t *testing.T) {
	t.Parallel()
	cl, err := New(WithToken(os.Getenv("ESTUARY_TOK")))
	if err != nil {
		t.Error(err)
	}
	c := uploadRand(t, cl, 1024*20)
	if c == cid.Undef {
		return
	}
	defer cl.Unpin(context.Background(), c)
}

func TestUploadPresentUnpinPresent(t *testing.T) {
	t.Parallel()
	cl, err := New(WithToken(os.Getenv("ESTUARY_TOK")))
	if err != nil {
		t.Error(err)
	}
	c := uploadRand(t, cl, 367)
	if c == cid.Undef {
		return
	}
	defer cl.Unpin(context.Background(), c)
	ok, err := cl.IsPresent(context.Background(), c)
	if err != nil {
		t.Fatalf("expected IsPresent not to error; got %v", err)
	}
	if !ok {
		t.Fatalf("expected %s to be present; it's not", c)
	}
	if err := cl.Unpin(context.Background(), c); err != nil {
		t.Fatalf("expected to unpin without error; got %v", err)
	}
	ok, err = cl.IsPresent(context.Background(), c)
	if err != nil {
		t.Fatalf("expected IsPresent not to error; got %v", err)
	}
	if ok {
		t.Fatalf("expected %s not to be present; it is", c)
	}
}

func TestPinPresentUnpinPresent(t *testing.T) {
	t.Parallel()
	cl, err := New(WithToken(os.Getenv("ESTUARY_TOK")))
	if err != nil {
		t.Error(err)
	}
	// CID used by https://ipfs.github.io/public-gateway-checker/
	c, _ := cid.Decode("bafybeibwzifw52ttrkqlikfzext5akxu7lz4xiwjgwzmqcpdzmp3n5vnbe")
	if err := cl.Unpin(context.Background(), c); err != nil {
		t.Fatal(err)
	}
	ok, err := cl.IsPresent(context.Background(), c)
	if err != nil {
		t.Fatalf("expected IsPresent not to error; got %v", err)
	}
	if ok {
		t.Fatalf("expected %s not to be present; it is", c)
	}
	if err := cl.Pin(context.Background(), c, "some-key"); err != nil {
		t.Fatalf("expected to pin without error; got %v", err)
	}
	ok, err = cl.IsPresent(context.Background(), c)
	if err != nil {
		t.Fatalf("expected IsPresent not to error; got %v", err)
	}
	if !ok {
		t.Fatalf("expected %s to be present; it isn't", c)
	}
	if err := cl.Unpin(context.Background(), c); err != nil {
		t.Fatalf("expected to unpin without error; got %v", err)
	}
}

// TestGatewayURL confirms that the URL of the gateway hasn't been changed, as per https://github.com/application-research/estuary/issues/273
func TestGatewayURL(t *testing.T) {
	gw := ai.Gateway{UA: http.DefaultClient, URL: ai.EstuaryTech}
	c, _ := cid.Decode("bafybeifx7yeb55armcsxwwitkymga5xf53dxiarykms3ygqic223w5sk3m")
	var ws memfile.File
	rc, err := gw.Get(context.Background(), c, 0)
	if err != nil {
		t.Fatalf("expected to Get %s without error; err = %v", c, err)
	}
	if _, err := ws.Seek(0, io.SeekStart); err != nil {
		t.Fatal(err)
	}
	gotContents, err := io.ReadAll(rc)
	if err != nil {
		t.Fatal(err)
	}
	expectedContents := []byte("Hello from IPFS Gateway Checker\n")
	if !bytes.Equal(gotContents, expectedContents) {
		t.Fatalf("expected to receive %s; got %s", expectedContents, gotContents)
	}
}
