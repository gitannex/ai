package ai

import "testing"

func TestTODO(t *testing.T) {
	t.Skipf("Incomplete")
}

// func TestPrepareGettersLocalShell(t *testing.T) {
// 	tmpDir := t.TempDir()
// 	if err := os.WriteFile(filepath.Join(tmpDir, "api"), []byte("/ip4/127.0.0.1/tcp/5001"), os.ModePerm); err != nil {
// 		t.Error(err)
// 	}
// 	t.Setenv("IPFS_PATH", tmpDir)
// 	ma := new(mockannex.Annex)
// 	ia := AI{
// 		I: &mockIPFSRemote{},
// 	}
// 	err := ia.prepareGetters(ma)
// 	if err != nil {
// 		t.Errorf("expected no error; got %v", err)
// 	}
// 	if len(ia.getters) < 1 {
// 		t.Fatalf("expected at least one getter to be present in slice")
// 	}
// 	if _, ok := ia.getters[0].(*LocalNode); !ok {
// 		t.Errorf("expected 1st getter to be a LocalNode; got %v", ia.getters[0])
// 	}
// }

// func TestPrepareGettersNoLocalShell(t *testing.T) {
// 	tmpDir := t.TempDir()
// 	t.Setenv("IPFS_PATH", tmpDir)
// 	ma := new(mockannex.Annex)
// 	ia := AI{
// 		I: &mockIPFSRemote{},
// 	}
// 	err := ia.prepareGetters(ma)
// 	if err != nil {
// 		t.Errorf("expected no error; got %v", err)
// 	}
// 	if len(ia.getters) < 1 {
// 		t.Fatalf("expected at least one getter to be present in slice")
// 	}
// 	for _, getter := range ia.getters {
// 		if _, ok := getter.(*LocalNode); ok {
// 			t.Errorf("didn't expect LocalNode in getters slice; found it")
// 		}
// 	}
// }

// func TestPrepareGetters(t *testing.T) {
// 	ma := new(mockannex.Annex)
// 	ai := AI{
// 		I: &mockIPFSRemote{},
// 	}
// 	err := ai.prepareGetters(ma)
// 	if err != nil {
// 		t.Errorf("expected no error; got %v", err)
// 	}
// 	if len(ai.getters) < 2 {
// 		t.Fatalf("expected at least two getters to be present in slice; found %d", len(ai.getters))
// 	}
// 	var foundGateways int
// 	for _, getter := range ai.getters {
// 		if gw, ok := getter.(*Gateway); ok {
// 			if gw.Verifier != nil {
// 				foundGateways++
// 			}
// 		}
// 	}
// 	if foundGateways < 2 {
// 		t.Errorf("expected to find at least two gateways in the getters slice; found %d", foundGateways)
// 	}
// }
