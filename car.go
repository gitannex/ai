package ai

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"sync"

	blocks "github.com/ipfs/go-block-format"
	"github.com/ipfs/go-cid"
	format "github.com/ipfs/go-ipld-format"
	"github.com/ipfs/go-merkledag"
	"github.com/ipfs/go-unixfs"
	"github.com/multiformats/go-multihash"
	"golang.org/x/sync/errgroup"
	"golang.org/x/sync/semaphore"
)

// blockGetter.Get returns blocks it has verified to match the given CID.
// FIXME: Duplicated in internal/retrieve
type blockGetter interface {
	GetBlock(context.Context, cid.Cid) (blocks.Block, error)
}

type readerSeekerAt interface {
	io.ReaderAt
	io.Seeker
}

type cidVerifier struct {
	bg struct {
		blockGetter
		sync.Mutex
	}
	wantCID cid.Cid
	rat     struct {
		readerSeekerAt
		sync.Mutex
	}
	expectedFileSz int64
	actualFileSz   int64
	eg             *errgroup.Group
	sem            semaphore.Weighted
}

// TODO: Robust handling of the file being longer than the DAG implies. How to check?

func NewCIDVerifier(bg blockGetter) *cidVerifier {
	cv := cidVerifier{}
	cv.bg.blockGetter = bg

	return &cv
}

func (cv *cidVerifier) Verify(ctx context.Context, wantCID cid.Cid, rat readerSeekerAt) error {
	cv.rat.readerSeekerAt = rat
	cv.wantCID = wantCID
	cv.expectedFileSz = 0
	eg, ctx := errgroup.WithContext(ctx)
	cv.eg = eg
	cv.sem = *semaphore.NewWeighted(100)
	pos, err := cv.rat.Seek(0, io.SeekEnd)
	if err != nil {
		return err
	}
	cv.actualFileSz = pos
	if cv.wantCID.Prefix().MhType == multihash.IDENTITY {
		return cv.verifySpan(ctx, cv.wantCID, 0, cv.actualFileSz)
	}
	cv.bg.Lock()
	bl, err := cv.bg.GetBlock(ctx, cv.wantCID)
	cv.bg.Unlock()
	if err != nil {
		return fmt.Errorf("Verify: can't fetch wantCID (%s) block: %w", cv.wantCID, err)
	}
	if !bl.Cid().Equals(cv.wantCID) {
		return fmt.Errorf("Verify: expected block CID to be wantCID (%s); got %s", cv.wantCID, bl.Cid())
	}
	cv.eg.Go(func() error {
		return cv.verifyBlock(ctx, bl, 0)
	})
	if err := cv.eg.Wait(); err != nil {
		return err
	}
	if cv.expectedFileSz < 1 || (cv.expectedFileSz != cv.actualFileSz) {
		return fmt.Errorf("Verify: expected %d bytes; considered %d", cv.actualFileSz, cv.expectedFileSz)
	}
	return nil
}

func (cv *cidVerifier) ExpectedSize() int64 {
	return cv.expectedFileSz
}

func (cv *cidVerifier) verifyBlock(ctx context.Context, bl blocks.Block, parentOffset int64) error {
	if !bl.Cid().Defined() {
		return fmt.Errorf("verifyBlock: undefined CID for block %v", bl)
	}
	// TODO: sync.Once: set expectedsize from first block
	if bl.Cid().Type() == cid.Raw {
		node, err := merkledag.DecodeRawBlock(bl)
		if err != nil {
			return fmt.Errorf("verifyBlock: can't decode raw block: %w", err)
		}
		return cv.verifyRaw(ctx, node, parentOffset)
	}
	if bl.Cid().Type() == cid.DagProtobuf {
		node, err := merkledag.DecodeProtobufBlock(bl)
		if err != nil {
			return fmt.Errorf("verifyBlock: can't decode protobuff block: %w", err)
		}
		return cv.verifyProtoBuff(ctx, node, parentOffset)
	}
	return fmt.Errorf("verifyBlock: unexpected block type %d", bl.Cid().Type())
}

func (cv *cidVerifier) verifyRaw(ctx context.Context, nd format.Node, parentOffset int64) error {
	wantBytes := nd.RawData()
	st, err := nd.Stat()
	if err != nil {
		return err
	}
	if int64(st.DataSize) > cv.expectedFileSz {
		cv.expectedFileSz = int64(st.DataSize)
	}
	return cv.verifyLit(ctx, wantBytes, parentOffset, parentOffset+int64(st.DataSize))
}

func (cv *cidVerifier) verifyProtoBuff(ctx context.Context, nd format.Node, parentOffset int64) error {
	fsNd, err := unixfs.ExtractFSNode(nd)
	if err != nil {
		return fmt.Errorf("can't extract UnixFS node from node %s (CID: %s): %w", nd, nd.Cid(), err)
	}
	// racy?
	if fsNd.FileSize() > uint64(cv.expectedFileSz) {
		cv.expectedFileSz = int64(fsNd.FileSize())
	}
	return cv.verifyFSNode(ctx, fsNd, nd.Links(), parentOffset)
}

// TODO: do we need to track each link encountered then check them off as we process them?

func (cv *cidVerifier) verifyFSNode(ctx context.Context, fsNd *unixfs.FSNode, lns []*format.Link, parentOffset int64) error {
	if len(fsNd.BlockSizes()) != len(lns) {
		return fmt.Errorf("verifyFSNode: expected BlockSizes to be same length as links")
	}
	cv.eg.Go(func() error {
		if err := cv.sem.Acquire(ctx, 1); err != nil {
			return err
		}
		defer cv.sem.Release(1)
		lastOffset := parentOffset
		for i, bs := range fsNd.BlockSizes() {
			start := lastOffset
			end := start + int64(bs)
			i := i
			if err := cv.evalLink(ctx, lns[i], start, end); err != nil {
				return err
			}
			lastOffset = end
		}
		if len(lns) > 0 {
			// TODO: What if it does have data _and_ links? Always an error?
			return nil
		}
		wantBytes := fsNd.Data()
		if int64(len(wantBytes)) != int64(fsNd.FileSize()) {
			return fmt.Errorf("verifyFSNode: node data is %d bytes, file size is %d", len(wantBytes), fsNd.FileSize())
		}
		return cv.verifyLit(ctx, wantBytes, parentOffset, parentOffset+int64(fsNd.FileSize()))
	})
	return nil
}

func (cv *cidVerifier) evalLink(ctx context.Context, ln *format.Link, start, end int64) error {
	if ln.Cid.Prefix().Codec == cid.Raw || cv.wantCID.Prefix().MhType == multihash.IDENTITY {
		// TODO: test case for file with identity hash link
		return cv.verifySpan(ctx, ln.Cid, start, end)
	}
	if ln.Cid.Prefix().Codec != cid.DagProtobuf {
		return fmt.Errorf("evalLink: unexpected link target: %d for CID %s", ln.Cid.Prefix().Codec, ln.Cid)
	}
	err := cv.assumeLeaf(ctx, ln, start, end)
	if err == nil {
		// Correctly assumed leaf
		return nil
	}
	// Ignore the negative error from assumeLeaf
	cv.bg.Lock()
	bl, err := cv.bg.GetBlock(ctx, ln.Cid)
	cv.bg.Unlock()
	if err != nil {
		return fmt.Errorf("evalLink: can't follow link %s: %w", ln.Cid, err)
	}

	// The below could determine the block isn't a protobuff...
	return cv.verifyBlock(ctx, bl, start)
}

func (cv *cidVerifier) assumeLeaf(ctx context.Context, ln *format.Link, start, end int64) error {
	data := make([]byte, end-start)
	cv.rat.Lock()
	if _, err := cv.rat.ReadAt(data, start); err != nil {
		return err
	}
	cv.rat.Unlock()
	fsNd := unixfs.NewFSNode(unixfs.TFile)
	fsNd.SetData(data)
	pbBytes, err := fsNd.GetBytes()
	if err != nil {
		return err
	}
	protoNdRaw, err := merkledag.NodeWithData(pbBytes).Marshal()
	if err != nil {
		return err
	}
	gotMh, err := multihash.Sum(protoNdRaw, ln.Cid.Prefix().MhType, -1)
	if err != nil {
		return err
	}
	// TODO: Convert both CIDs to V1 then Equals?
	if ln.Cid.Hash().HexString() == gotMh.HexString() {
		return nil
	}
	return fmt.Errorf("assumeLeaf: synthesized fsNode had mh %s; link had mh %s", gotMh, ln.Cid.Hash())
}

func (cv *cidVerifier) verifyLit(ctx context.Context, wantBytes []byte, start, end int64) error {
	if int64(len(wantBytes)) != end-start {
		return fmt.Errorf("verifyLit: expected len(wantBytes) = %d - %d; it is %d bytes", end, start, len(wantBytes))
	}
	gotBytes := make([]byte, len(wantBytes))
	cv.rat.Lock()
	n, err := cv.rat.ReadAt(gotBytes, start)
	cv.rat.Unlock()
	if err != nil {
		return fmt.Errorf("verifyLit: couldn't read rat: %w", err)
	}
	if int64(n) != (end - start) {
		return fmt.Errorf("verifyLit: expected to read %d bytes, but actually read %d", end-start, n)
	}
	if bytes.Equal(wantBytes, gotBytes) {
		return nil
	}
	return fmt.Errorf("verifyLit: %s doesn't match %s", wantBytes, gotBytes)
}

func (cv *cidVerifier) verifySpan(ctx context.Context, c cid.Cid, start, end int64) error {
	// TODO: when to do bounds check? i.e. is cv.rat at least as long as end-start?
	// log.Printf("verifySpan[%s]: cid=%s (%d-%d)", cv.wantCID, c, start, end)

	wantMh := c.Hash()
	dMh, err := multihash.Decode([]byte(wantMh))
	if err != nil {
		return fmt.Errorf("verifySpan: can't decode multihash from CID %s: %w", c, err)
	}
	section := make([]byte, end-start)
	cv.rat.Lock()
	if _, err = cv.rat.ReadAt(section, start); err != nil {
		return err
	}
	cv.rat.Unlock()
	gotMh, err := multihash.Sum(section, dMh.Code, -1)
	if err != nil {
		return fmt.Errorf("verifySpan: can't sum section %d-%d: %w", start, end, err)
	}
	if bytes.Equal([]byte(wantMh), []byte(gotMh)) {
		return nil
	}
	return fmt.Errorf("verifySpan: expected multihash of %x; got multihash of %x", wantMh, gotMh)
}
