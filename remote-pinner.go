package ai

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"github.com/ipfs/go-cid"
	pinclient "github.com/ipfs/go-pinning-service-http-client"
)

// RemotePinner is a client for the IPFS Pinning Service API which implements
// the methods necessary for a Git Annex remote.
//
// This API is specified in https://ipfs.github.io/pinning-services-api-spec/
type RemotePinner struct {
	*pinclient.Client
	// PollWait is the time Pin waits before polling for a status change. If nil it is equal to DefaultPollWait.
	PollWait      *time.Duration
	endpoint, jwt string
	ua            *http.Client
}

func NewRemotePinner(endpoint, jwt string, ua *http.Client) *RemotePinner {
	return &RemotePinner{
		Client:   pinclient.NewClient(endpoint, jwt).WithHTTPClient(ua),
		endpoint: endpoint,
		jwt:      jwt,
		ua:       ua,
	}
}

// IsPinned returns true if the given CID has a pinned status, false
// if the CID has another status or is not known to the endpoint. A
// non-nil error is returned if there was a problem communicating with
// the endpoint.
func (rp *RemotePinner) IsPinned(ctx context.Context, c cid.Cid) (bool, error) {
	_, cnt, err := rp.LsBatchSync(ctx, pinclient.PinOpts.FilterCIDs(c),
		pinclient.PinOpts.FilterStatus(pinclient.StatusPinned),
		pinclient.PinOpts.Limit(1))
	if err != nil {
		return false, err
	}
	return cnt > 0, nil
}

// DefaultPollWait is the time waited between requests for a pin's
// status. Its default value is the duration used by go-ipfs for the
// `ipfs pin remote add` command
const DefaultPollWait time.Duration = time.Second / 2

// Pin requests that the remote pinning service pin the given CID, then waits
// for confirmation that the pin has been created, the pin couldn't be created,
// or an error occurred while communicating with the service. Where possible,
// the pin is named with the given Git Annex key. To impose a timeout on this
// operation or otherwise abort it prematurely, cancel the supplied context. In
// the absence of a time-limited Context this method could block indefinitely if
// the remote service misbehaves or is unable to retrieve the CID.
//
// See
// https://ipfs.github.io/pinning-services-api-spec/#section/The-pin-lifecycle
// for background on how the pin status changes. Note that we treat an
// unknown status as pending.
func (rp *RemotePinner) Pin(ctx context.Context, c cid.Cid, key string) error {
	pinPollDuration := DefaultPollWait
	if rp.PollWait != nil {
		pinPollDuration = *rp.PollWait
		if int64(pinPollDuration) < 1 {
			pinPollDuration = time.Duration(1)
		}
	}
	return rp.pinWithWait(ctx, c, key, pinPollDuration)
}

// pinWithWait performs the Pin operation with a configurable time to wait
// between requests. This aids in testing.
//
// TODO: Experiment with a backoff rather than a constant duration
func (rp *RemotePinner) pinWithWait(ctx context.Context, c cid.Cid, key string, wait time.Duration) error {
	meta := map[string]string{"git-annex-key": key}
	// TODO: Handle len(key) > 255
	psg, err := rp.Add(ctx, c, pinclient.PinOpts.WithName(key), pinclient.PinOpts.AddMeta(meta))
	if err != nil {
		return err
	}
	requestID := psg.GetRequestId()
	timer := time.NewTicker(wait)
	defer timer.Stop()
	for {
		if psg.GetRequestId() != requestID {
			return fmt.Errorf("endpoint returned incorrect request ID %s (want: %s)", psg.GetRequestId(), requestID)
		}
		switch psg.GetStatus() {
		case pinclient.StatusPinned:
			return nil
		case pinclient.StatusFailed:
			return errors.New("status: failed")
		}
		select {
		case <-timer.C:
		case <-ctx.Done():
			return fmt.Errorf("pinWithWait: %w", ctx.Err())
		}
		psg, err = rp.GetStatusByID(ctx, requestID)
		if err != nil {
			return err
		}
	}
}

// Unpin removes all pins for the given CID on the remote pinning service
//
// The Pinning Service API permits multiple pins to exist for a
// single CID, and has no facility to remove them in bulk, so the run
// time of this method is proportional to the number of pins.
//
// This method attempts to remove all pins, regardless of their
// status. That is, pins that are in the pinning, queued, or failed
// states will be deleted along with pins in the pinned state.
// TODO: Use metadata to only delete pins made for this repo
func (rp *RemotePinner) Unpin(ctx context.Context, c cid.Cid) error {
	// We create our own context so that if we return an error
	// from the loop, the G spawned by pinclient.Ls is terminated,
	// and therefore closes the two channels it manages.
	ctx, cancelFn := context.WithCancel(ctx)
	defer cancelFn()
	const limit = 1000 // Max as per https://ipfs.github.io/pinning-services-api-spec/#tag/pins/paths/~1pins/get
	psgCh, errCh := rp.Ls(ctx, pinclient.PinOpts.FilterCIDs(c),
		pinclient.PinOpts.FilterStatus(pinclient.StatusFailed,
			pinclient.StatusPinned,
			pinclient.StatusPinning,
			pinclient.StatusQueued),
		pinclient.PinOpts.Limit(limit))
	for psg := range psgCh {
		if err := rp.deleteByID(ctx, psg.GetRequestId()); err != nil {
			if ed, isErrDelete := err.(*errDelete); isErrDelete && ed.httpStatus == 404 {
				continue
			}
			return fmt.Errorf("Unpin: can't delete requestID %s for CID %s: deleteByID: %w", psg.GetRequestId(), c, err)
		}
	}
	if err := <-errCh; err != nil {
		return fmt.Errorf("Unpin: can't fetch list of pins for CID %s: %w", c, err)
	}
	return nil
}

type errReasonDetails struct {
	Reason  string `json:"reason"`
	Details string `json:"details"`
}

type errDelete struct {
	errReasonDetails `json:"error"`
	httpStatus       int
}

func (ed *errDelete) Error() string {
	msg := fmt.Sprintf("errDelete: HTTP status: %d", ed.httpStatus)
	if ed.Reason != "" {
		msg += fmt.Sprintf(", reason: %s, details: %s", ed.Reason, ed.Details)
	}
	return msg
}

func (rp *RemotePinner) deleteByID(ctx context.Context, requestID string) error {
	u := rp.endpoint + "/pins/" + requestID
	req, err := http.NewRequestWithContext(ctx, http.MethodDelete, u, http.NoBody)
	if err != nil {
		return err
	}
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", rp.jwt))
	res, err := rp.ua.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()
	statusCode := res.StatusCode
	// The PSA spec requires a 202 status code to be sent on
	// successful deletions, but some endpoints send a 200
	if statusCode == http.StatusAccepted || statusCode == http.StatusOK {
		return nil
	}
	body, err := io.ReadAll(res.Body)
	if err != nil {
		return err
	}
	ed := errDelete{httpStatus: statusCode}
	// On error, the body should be a JSON error message, but some
	// endpoints serve a text/plain response
	if len(body) > 0 && strings.HasPrefix(res.Header.Get("Content-Type"), "application/json") {
		if err := json.Unmarshal(body, &ed); err != nil {
			return fmt.Errorf("can't parse error JSON (%s): %w", body, err)
		}
	}
	return &ed
}
