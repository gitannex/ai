package ai

import (
	"net/http"

	"ai.camelcase.link/internal/retrieve"
	shell "github.com/ipfs/go-ipfs-api"
)

func DefaultRetriever(ua *http.Client) *retrieve.R {
	r := retrieve.R{}
	if sh := shell.NewLocalShell(); sh != nil {
		r.Getters = append(r.Getters, &LocalNode{Shell: sh})
	}

	r.Getters = append(r.Getters,
		&Gateway{UA: ua, URL: DwebLink},
		&Gateway{UA: ua, URL: CloudflareIPFS},
		&Gateway{UA: ua, URL: IPFSIo},
		&Gateway{UA: ua, URL: InfuraIPFSIo})
	r.BlockGetters = append(r.BlockGetters,
		&Gateway{UA: ua, URL: DwebLink},
		&Gateway{UA: ua, URL: IPFSIo},
	)
	return &r
}
