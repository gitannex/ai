package ai

import (
	"fmt"
	"strings"

	"github.com/ipfs/go-cid"
)

type GatewayURL struct {
	Base             string
	SubDomainPattern string
	API              bool
}

func (gu GatewayURL) Public(c cid.Cid) string {
	cStr := c.String()
	if strings.HasPrefix(cStr, "Qm") || gu.SubDomainPattern == "" {
		path := fmt.Sprintf(DefaultGetPathPattern, cStr)
		return strings.TrimSuffix(gu.Base, "/") + path
	}

	return fmt.Sprintf(gu.SubDomainPattern, cStr)
}

func (gu GatewayURL) Block(c cid.Cid) string {
	if !gu.API {
		return ""
	}
	return strings.TrimSuffix(gu.Base, "/") + "/api/v0/block/get?arg=" + c.String()
}

const DefaultGetPathPattern = "/ipfs/%s"

var (
	DwebLink       = GatewayURL{Base: "https://dweb.link/", SubDomainPattern: "https://%s.ipfs.dweb.link/", API: true}
	IPFSIo         = GatewayURL{Base: "https://ipfs.io/", API: true}
	CloudflareIPFS = GatewayURL{Base: "https://cloudflare-ipfs.com/"}
	InfuraIPFSIo   = GatewayURL{Base: "https://infura-ipfs.io/", SubDomainPattern: "https://%s.ipfs.infura-ipfs.io/"}
	PinataCloud    = GatewayURL{Base: "https://gateway.pinata.cloud/", API: true}
	EstuaryTech    = GatewayURL{Base: "https://api.estuary.tech/gw/"}
)
