package mockannex

import "github.com/dzhu/go-git-annex-external/remote"

type Annex struct {
	GetStateFn      func(key string) (state string)
	ProgressFn      func(bytes int)
	SetStateFn      func(key, val string)
	SetURIPresentFn func(key, uri string)
	SetURLPresentFn func(key, url string)
	SetURIMissingFn func(key, uri string)
	SetURLMissingFn func(key, url string)
	GetURLsFn       func(key, prefix string) []string
	InfoFFn         func(fmt string, args ...interface{})
	DebugFFn        func(fmt string, args ...interface{})
	ErrorFFn        func(fmt string, args ...interface{})
	GetConfigFn     func(key string) string
	GetCredsFn      func(name string) (string, string)
	GetUUIDFn       func() string
	GetGitDirFn     func() string
	remote.Annex
}

func (ma *Annex) GetState(key string) (state string) {
	if ma.GetStateFn == nil {
		return ""
	}
	return ma.GetStateFn(key)
}

func (ma *Annex) SetState(key, val string) {
	if ma.SetStateFn != nil {
		ma.SetStateFn(key, val)
	}
}

func (ma *Annex) Progress(bytes int) {
	if ma.ProgressFn != nil {
		ma.ProgressFn(bytes)
	}
}

func (ma *Annex) SetURIPresent(key, uri string) {
	if ma.SetURIPresentFn != nil {
		ma.SetURIPresentFn(key, uri)
	}
}

func (ma *Annex) SetURLPresent(key, url string) {
	if ma.SetURLPresentFn != nil {
		ma.SetURLPresentFn(key, url)
	}
}

func (ma *Annex) SetURIMissing(key, uri string) {
	if ma.SetURIMissingFn != nil {
		ma.SetURIMissingFn(key, uri)
	}
}

func (ma *Annex) SetURLMissing(key, url string) {
	if ma.SetURLMissingFn != nil {
		ma.SetURLMissingFn(key, url)
	}
}

func (ma *Annex) GetURLs(key, prefix string) []string {
	if ma.GetURLsFn == nil {
		return []string{}
	}
	return ma.GetURLsFn(key, prefix)
}

func (ma *Annex) Infof(fmt string, args ...interface{}) {
	if ma.InfoFFn != nil {
		ma.InfoFFn(fmt, args...)
	}
}

func (ma *Annex) Debugf(fmt string, args ...interface{}) {
	if ma.DebugFFn != nil {
		ma.DebugFFn(fmt, args...)
	}
}

func (ma *Annex) Errorf(fmt string, args ...interface{}) {
	if ma.ErrorFFn != nil {
		ma.ErrorFFn(fmt, args...)
	}
}

func (ma *Annex) GetConfig(k string) string {
	if ma.GetConfigFn != nil {
		return ma.GetConfigFn(k)
	}
	return ""
}

func (ma *Annex) GetCreds(name string) (string, string) {
	if ma.GetCredsFn != nil {
		return ma.GetCredsFn(name)
	}
	return "", ""
}

func (ma *Annex) GetUUID() string {
	if ma.GetUUIDFn != nil {
		return ma.GetUUIDFn()
	}
	return ""
}

func (ma *Annex) GetGitDir() string {
	if ma.GetGitDirFn != nil {
		return ma.GetGitDirFn()
	}
	return ""
}
