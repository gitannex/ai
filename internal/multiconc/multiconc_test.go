package multiconc

import (
	"io"
	"os"
	"testing"

	"ai.camelcase.link/internal/progress"
)

func TestProgress(t *testing.T) {
	fh, err := os.Open("../../testdata/JSB_BWV1047_1.xml")
	if err != nil {
		t.Fatal(err)
	}
	defer fh.Close()
	stat, err := fh.Stat()
	if err != nil {
		t.Fatal(err)
	}
	var culmProgress int64
	var totalRead int64
	proFn := func(n int64) {
		culmProgress = n
	}
	pro := progress.NewTracker(proFn, stat.Size())
	mcRdr := Reader{
		File:      fh,
		FileInfo:  stat,
		Progress:  pro,
		FileField: "foo",
		Fields:    map[string]string{"glark": "2383784646"},
	}
	pipeRC := mcRdr.Start()
	n, err := io.CopyN(io.Discard, pipeRC, 100)
	if err != nil {
		t.Errorf("couldn't read 100 bytes from pipe: %v", err)
	}
	totalRead += n
	// So far we're still reading the multipart header so haven't reported any progress
	if culmProgress != 0 {
		t.Errorf("after reading 100 bytes expected culmProgress to still be 0; it was %d", culmProgress)
	}
	n, err = io.CopyN(io.Discard, pipeRC, 1_000)
	if err != nil {
		t.Errorf("couldn't read 1,000 bytes from pipe: %v", err)
	}
	totalRead += n
	// We've started reading the file now, so we have reported
	// progress. Because of buffer sizing we've likely reported a
	// lot more than we need (i.e. culmProgress > 1,100
	if culmProgress <= 0 || culmProgress >= stat.Size() {
		t.Errorf("after reading 1,000 bytes expected culmProgress to be >0; it was %d", culmProgress)
	}
	n, err = io.Copy(io.Discard, pipeRC)
	if err != nil {
		t.Errorf("expected to read from remainder of pipe without error; got %v", err)
	}
	totalRead += n
	if culmProgress >= stat.Size() {
		t.Errorf("expected culmProgress to be slightly less than the file size; got %d", culmProgress)
	}
	if err := pipeRC.Close(); err != nil {
		t.Fatal(err)
	}
	if totalRead <= stat.Size() {
		t.Errorf("expected to have read more than the file size; read %d for %d byte file", totalRead, stat.Size())
	}
	// We've read the whole file, but we may not have reported every last byte before pro.Close:
	if culmProgress >= stat.Size() {
		t.Errorf("expected culmProgress to be less than the file size (%d); got %d", stat.Size(), culmProgress)
	}
	_ = pro.Close()
	if culmProgress != stat.Size() {
		t.Errorf("expected culmProgress to equal file size (%d); got %d", stat.Size(), culmProgress)
	}
}
