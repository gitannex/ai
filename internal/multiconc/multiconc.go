package multiconc

import (
	"fmt"
	"io"
	"io/fs"
	"mime/multipart"

	"ai.camelcase.link/internal/progress"
)

type Reader struct {
	File      fs.File
	FileInfo  fs.FileInfo
	Progress  *progress.Tracker
	FileField string
	Fields    map[string]string

	pipeReader      *io.PipeReader
	pipeWriter      *io.PipeWriter
	multipartWriter *multipart.Writer
}

func (mcr *Reader) createPipe() {
	pr, pw := io.Pipe()
	mcr.pipeReader = pr
	mcr.pipeWriter = pw
}

func (mcr *Reader) g() {
	defer mcr.pipeWriter.Close()
	for name, value := range mcr.Fields {
		if err := mcr.multipartWriter.WriteField(name, value); err != nil {
			_ = mcr.pipeReader.CloseWithError(fmt.Errorf("couldn't write field %s", name))
			return
		}
	}
	part, err := mcr.multipartWriter.CreateFormFile(mcr.FileField, mcr.FileInfo.Name())
	if err != nil {
		_ = mcr.pipeReader.CloseWithError(err)
		return
	}
	// We originally attempted to track the progress of the whole
	// multipart body upload, but it seems more robust, and
	// possibly more accurate, just to consider the file portion:
	if _, err := io.Copy(part, mcr.Progress.NewReader(mcr.File)); err != nil {
		_ = mcr.pipeReader.CloseWithError(err)
		return
	}
	if err := mcr.multipartWriter.Close(); err != nil {
		_ = mcr.pipeReader.CloseWithError(err)
		return
	}
}

func (mcr *Reader) Start() io.ReadCloser {
	mcr.createPipe()
	mcr.multipartWriter = multipart.NewWriter(mcr.pipeWriter)
	go mcr.g()
	return mcr.pipeReader
}

func (mcr *Reader) ContentType() string { return mcr.multipartWriter.FormDataContentType() }
