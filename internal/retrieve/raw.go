package retrieve

import (
	"context"
	"errors"
	"fmt"
	"io"

	"github.com/hashicorp/go-multierror"
	"github.com/ipfs/go-cid"
)

var (
	ErrNoGetters           = errors.New("no Getters configured")
	ErrPartialContents     = errors.New("target is not empty")
	ErrNothingToResume     = errors.New("nothing to resume; target already the correct size, or is empty")
	ErrCantResume          = errors.New("request can't be resumed")
	ErrShortWrite          = errors.New("Getter returned too few bytes")
	ErrLongWrite           = errors.New("Getter returned too many bytes")
	ErrSizeMismatch        = errors.New("the ExpectedSize and the size returned from the verifier differ")
	ErrUndefinedCID        = errors.New("missing/undefined CID")
	ErrInvalidExpectedSize = errors.New("invalid ExpectedSize")
)

// VerifyFn verifies that raw.Target matches raw.CID, and returns the total size raw.Target is expected to be. If verification fails, a non-nil error is returned.
type VerifyFn func() (expectedSize int64, err error)

// Raw represents a request to fetch, verify, and store a given CID.
type Raw struct {
	Getters      []Getter
	CID          cid.Cid
	ExpectedSize int64
	VerifyFn     VerifyFn
	Target       io.WriteSeeker
}

// tell returns the current file position of raw.Target. It will panic if the underlying Seek call fails, as explained in the documentation for seekOrPanic.
func (raw *Raw) tell() int64 {
	return raw.seekOrPanic(0, io.SeekCurrent)
}

// rewind seeks the file position of raw.Target to 0. It will panic if the underlying Seek call fails, as explained in the documentation for seekOrPanic.
func (raw *Raw) rewind() {
	_ = raw.seekOrPanic(0, io.SeekStart)
}

// seekOrPanic passes its arguments to raw.Target.Seek. If this results in an error, the method panics because we don't know how to recover from this kind of error. raw.Download converts such a panic to an error, which it returns to the caller.
func (raw *Raw) seekOrPanic(off int64, whence int) int64 {
	pos, err := raw.Target.Seek(off, whence)
	if err != nil {
		panic(err)
	}
	return pos
}

// shouldDownload returns true and a non-nil error if raw is configured correctly and the payload is not already present in raw.Target.
func (raw *Raw) shouldDownload(ctx context.Context) (bool, error) {
	if raw.Target == nil {
		return false, fmt.Errorf("target is nil")
	}
	targetSize := raw.tell()
	if err := raw.isSane(targetSize); err != nil {
		return false, err
	}
	if err := raw.fullyRetrievedAndVerified(ctx, targetSize); err == nil {
		return false, nil
	}
	return true, nil
}

// Download fetches the payload of raw.CID with one or more of raw.Getters. If this payload verifies correctly, and can be written to raw.Target, the method returns nil.
func (raw *Raw) Download(ctx context.Context) (err error) {
	defer func() {
		r := recover()
		if r == nil {
			return
		}
		var recErr error
		var isRecErr bool
		recErr, isRecErr = r.(error)
		if !isRecErr {
			recErr = fmt.Errorf("recovered panic %v", r)
		}
		err = multierror.Append(err, recErr)
	}()
	err = raw.downloadMightPanic(ctx)
	return
}

// downloadMightPanic fetches the payload of raw.CID with one or more of raw.Getters, verifies it, then writes it to raw.Target. If an error occurs when attempting to Seek in raw.Target, this method will panic; any other error will be returned.
func (raw *Raw) downloadMightPanic(ctx context.Context) error {
	should, err := raw.shouldDownload(ctx)
	if err != nil {
		return err
	}
	if !should {
		return nil
	}
	var errs *multierror.Error
	for _, getter := range raw.Getters {
		err := raw.downloadWithGetter(ctx, getter)
		if err == nil {
			return nil
		}
		errs = multierror.Append(errs, fmt.Errorf("%s: %w", getter, err))
		if ctx.Err() != nil {
			errs = multierror.Append(errs, fmt.Errorf("%s,%w", getter, ctx.Err()))
			break
		}
	}
	if errs != nil {
		errs.ErrorFormat = func([]error) string {
			return "Download failed"
		}
	}
	return errs
}

// fullyRetrievedAndVerified returns nil if raw.Target has the size raw.ExpectedSize and passes verification.
func (raw *Raw) fullyRetrievedAndVerified(ctx context.Context, targetSize int64) error {
	if !raw.canVerify() {
		return fmt.Errorf("no verifier")
	}
	if targetSize == 0 {
		return fmt.Errorf("no data")
	}

	if raw.ExpectedSize != targetSize {
		return fmt.Errorf("not fully retrieved")
	}
	defer raw.rewind()
	return raw.verify(ctx)
}

// downloadWithGetter attempts to retrieve raw.CID via getter from the current file position of raw.Target, verify it if needed, and copy the payload to raw.Target. If it succeeds, it returns nil. If an error occurs, which is not ErrShortWrite, the file position is reset to the beginning. Any error encountered is returned.
func (raw *Raw) downloadWithGetter(ctx context.Context, getter Getter) error {
	resumeFrom := raw.tell()
	rc, getErr := getter.Get(ctx, raw.CID, resumeFrom)
	if getErr != nil {
		raw.rewind()
		return getErr
	}

	defer func() { _ = rc.Close() }()
	cvErr := raw.copyAndVerify(ctx, getter, rc, resumeFrom)
	if cvErr == nil {
		return nil
	}
	if !errors.Is(cvErr, ErrShortWrite) {
		raw.rewind()
	}
	return cvErr
}

// copyAndVerify copies the contents of rdr to raw.Target, beginning at file position resumeFrom. This payload is then verified unless either raw.VerifyFn is nil or the entire payload was retrieved from a trusted getter.
func (raw *Raw) copyAndVerify(ctx context.Context, getter Getter, rdr io.Reader, resumeFrom int64) error {
	written, err := raw.copyToTarget(ctx, rdr, resumeFrom)
	if err != nil {
		return err
	}

	if written == raw.ExpectedSize {
		if it, ok := getter.(IsTrusteder); ok && it.IsTrusted() {
			return nil
		}
	}
	if !raw.canVerify() {
		return nil
	}
	return raw.verify(ctx)
}

// copyToTarget copies from rdr to raw.Target. rdr is assumed to contain the payload from byte position resumeFrom to raw.ExpectedSize. If rdr contains more or less data than this, or if any errors occur, a non-nil error is returned. On success, the number of bytes copied and a non-nil error are returned.
func (raw *Raw) copyToTarget(ctx context.Context, rdr io.Reader, resumeFrom int64) (int64, error) {
	cap := raw.ExpectedSize - resumeFrom
	if cap < 0 {
		return 0, fmt.Errorf("invalid cap: %d", cap)
	}
	var written int64
	var cnErr error
	if cap > 0 {
		written, cnErr = io.CopyN(raw.Target, rdr, cap)
		if cnErr != nil {
			if errors.Is(cnErr, io.EOF) {
				// TODO: Should we report ErrShortWrite even if no bytes were copied?
				return written, fmt.Errorf("copyToTarget: expectedSize = %d, cap = %d, written = %d: %w", raw.ExpectedSize, cap, written, ErrShortWrite)
			}
			return written, cnErr
		}
	}
	buf := make([]byte, 1)
	read, rdErr := rdr.Read(buf)
	if read == 0 && errors.Is(rdErr, io.EOF) {
		return written, nil
	}
	return written, multierror.Append(rdErr, ErrLongWrite)
}

// canVerify reports whether a VerifyFn has been set; if it hasn't, verification is skipped.
func (raw *Raw) canVerify() bool {
	return raw.VerifyFn != nil
}

// verify uses raw.VerifyFn to verify raw.Target and assumes canVerify() is true. A non-nil error is returned when verification succeeds; if verification fails, or the size reported by the verifier differs from raw.ExpectedSize, an error is returned.
func (raw *Raw) verify(ctx context.Context) error {
	// TODO: pass in raw.Target?
	expectedSize, err := raw.VerifyFn()
	if raw.ExpectedSize == expectedSize || err != nil {
		return err
	}
	return fmt.Errorf("%w: verify reports %d, raw.ExpectedSize = %d", ErrSizeMismatch, expectedSize, raw.ExpectedSize)
}

// isSane reports whether the configuration of raw seems sensible given that the initial size of raw.Target is targetSize. A non-nil error is returned if any problems are discovered.
func (raw *Raw) isSane(targetSize int64) error {
	if len(raw.Getters) == 0 {
		return ErrNoGetters
	}
	if !raw.CID.Defined() {
		return ErrUndefinedCID
	}
	if raw.ExpectedSize <= 0 {
		return ErrInvalidExpectedSize
	}
	if targetSize > raw.ExpectedSize {
		return ErrNothingToResume
	}
	return nil
}
