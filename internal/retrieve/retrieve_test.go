package retrieve

import (
	"bytes"
	"context"
	"errors"
	"io"
	"testing"
	"testing/iotest"

	"github.com/dsnet/golib/memfile"
	blocks "github.com/ipfs/go-block-format"
	"github.com/ipfs/go-cid"
)

type mockGetter func(context.Context, cid.Cid, int64) (io.ReadCloser, error)

func (mg mockGetter) Get(ctx context.Context, c cid.Cid, pos int64) (io.ReadCloser, error) {
	return mg(ctx, c, pos)
}

type cookedGetter struct {
	Bytes []byte
}

func (cg cookedGetter) Get(ctx context.Context, c cid.Cid, pos int64) (io.ReadCloser, error) {
	return io.NopCloser(bytes.NewReader(cg.Bytes)), nil
}

type trustedGetter struct {
	Trusted bool
	Getter
}

func (tg trustedGetter) IsTrusted() bool { return tg.Trusted }

type errorGetter struct {
	Getter
	Err error
}

func (eg errorGetter) Get(ctx context.Context, c cid.Cid, pos int64) (io.ReadCloser, error) {
	_, _ = eg.Getter.Get(ctx, c, pos)
	return nil, eg.Err
}

type errorSeeker struct {
	io.WriteSeeker
	Err    error
	Bypass int
	call   int
}

func (es *errorSeeker) Seek(off int64, whence int) (int64, error) {
	defer func() { (*es).call++ }()
	if (*es).call < es.Bypass {
		return es.WriteSeeker.Seek(off, whence)
	}
	return 0, es.Err
}

type errorTruncator struct {
	*memfile.File
	Err error
}

func (et *errorTruncator) Truncate(pos int64) error {
	return et.Err
}

type statefulVerifier struct {
	Errors       []error
	ExpectedSize int64
}

func (sv *statefulVerifier) verify() (int64, error) {
	err := sv.Errors[0]
	sv.Errors = (*sv).Errors[1:]
	return sv.ExpectedSize, err
}

func newStatefulVerifier(errs []error, expectedSize int64) *statefulVerifier {
	sv := statefulVerifier{Errors: errs, ExpectedSize: expectedSize}
	return &sv
}

type getBlockCase struct {
	name string
	bgs  []BlockGetter
	ctx  context.Context
	c    cid.Cid
	b    blocks.Block
	err  error
}

type bgFn func(context.Context, cid.Cid) (blocks.Block, error)

func (bg bgFn) GetBlock(ctx context.Context, c cid.Cid) (blocks.Block, error) {
	return bg(ctx, c)
}

type cannedBG struct {
	B   blocks.Block
	Err error
}

func (cb cannedBG) GetBlock(context.Context, cid.Cid) (blocks.Block, error) {
	return cb.B, cb.Err
}

func TestGetBlock(t *testing.T) {
	someCID, _ := cid.Decode("QmZvVqUKYJegRUdV4LroNMF4KSGxwpwvfUWbVjUHJ7Vq6y")
	someBytes := []byte("BlockGetter")
	someBlock := blocks.NewBlock(someBytes)
	sentinelErr := errors.New("sentinel")
	canceleableCtx, cccFn := context.WithCancel(context.Background())
	testCases := []getBlockCase{
		{
			name: "no BlockGetters",
			c:    someCID,
			err:  ErrNoBlockGetters,
		},
		{
			name: "one BlockGetter happy path",
			bgs: []BlockGetter{
				cannedBG{B: someBlock},
			},
			c: someCID,
			b: someBlock,
		},
		{
			name: "one BlockGetter, errors",
			bgs: []BlockGetter{
				cannedBG{B: someBlock, Err: sentinelErr},
			},
			c:   someCID,
			err: sentinelErr,
		},
		{
			name: "two BlockGetters, first errors",
			bgs: []BlockGetter{
				cannedBG{B: someBlock, Err: errors.New("blah")},
				cannedBG{B: someBlock},
			},
			c: someCID,
			b: someBlock,
		},
		{
			name: "three BlockGetters, all error",
			bgs: []BlockGetter{
				cannedBG{B: someBlock, Err: sentinelErr},
				cannedBG{B: someBlock, Err: sentinelErr},
				cannedBG{B: someBlock, Err: sentinelErr},
			},
			c:   someCID,
			err: sentinelErr,
		},
		{
			name: "two BlockGetters, context cancelled",
			bgs: []BlockGetter{
				bgFn(func(context.Context, cid.Cid) (blocks.Block, error) {
					cccFn()
					return someBlock, sentinelErr
				}),
				bgFn(func(context.Context, cid.Cid) (blocks.Block, error) {
					panic("shouldn't get here")
				}),
			},
			ctx: canceleableCtx,
			c:   someCID,
			err: context.Canceled,
		},
		{
			name: "BlockGetter returns wrong block",
			bgs: []BlockGetter{
				bgFn(func(context.Context, cid.Cid) (blocks.Block, error) {
					return blocks.NewBlock([]byte("blah")), nil
				}),
			},
			c:   someCID,
			err: ErrBadBlock,
		},
	}
	for _, tc := range testCases {
		r := R{BlockGetters: tc.bgs}
		ctx := context.Background()
		if tc.ctx != nil {
			ctx = tc.ctx
		}
		b, err := r.GetBlock(ctx, tc.c)
		if !errors.Is(err, tc.err) {
			t.Errorf("%s: expected err to be %v; it was %v", tc.name, tc.err, err)
		}
		if tc.err != nil {
			continue
		}
		if !bytes.Equal(b.RawData(), tc.b.RawData()) || !b.Cid().Equals(tc.b.Cid()) {
			t.Errorf("%s: expected block %v; got %v", tc.name, tc.b, b)
		}

	}
}

type rawCase struct {
	name           string
	ctx            context.Context
	getters        []Getter
	c              cid.Cid
	target         io.WriteSeeker
	verifier       VerifyFn
	verifierCalled int
	err            error
}

func TestRaw(t *testing.T) {
	someCID, _ := cid.Decode("bafkreicouidydstgdqaeb27msm4oc4e4njmsfx7feo3iys63faxhq7raqy")
	const expectedContents = "Hello, Gophers."
	canceledCtx, cancelFn := context.WithCancel(context.Background())
	cancelFn()
	canceleableCtx, cccFn := context.WithCancel(context.Background())
	sentinelErr := errors.New("sentinel")

	testCases := []rawCase{
		{
			name:   "no getters",
			c:      someCID,
			target: memfile.New([]byte{}),
			err:    ErrNoGetters,
		},
		{
			name: "single getter, non trusted",
			c:    someCID,
			getters: []Getter{
				cookedGetter{
					Bytes: []byte(expectedContents),
				},
			},
			target:         memfile.New([]byte{}),
			verifier:       func() (int64, error) { return int64(len(expectedContents)), nil },
			verifierCalled: 1,

			err: nil,
		},
		{
			name: "single getter, non trusted, partial contents",
			c:    someCID,
			getters: []Getter{
				cookedGetter{
					Bytes: []byte(expectedContents)[5:],
				},
			},
			target:         memfile.New([]byte(expectedContents)[:5]),
			verifier:       func() (int64, error) { return int64(len(expectedContents)), nil },
			verifierCalled: 1,
			err:            nil,
		},
		{
			name: "single getter, non-trusted IsTrusteder, partial contents",
			c:    someCID,
			getters: []Getter{
				trustedGetter{
					Getter: cookedGetter{
						Bytes: []byte(expectedContents)[3:],
					},
				},
			},
			target:         memfile.New([]byte(expectedContents)[:3]),
			verifier:       func() (int64, error) { return int64(len(expectedContents)), nil },
			verifierCalled: 1,
			err:            nil,
		},
		{
			name: "single getter, non-trusted IsTrusteder",
			c:    someCID,
			getters: []Getter{
				trustedGetter{
					Getter: cookedGetter{
						Bytes: []byte(expectedContents),
					},
				},
			},
			target:         memfile.New([]byte{}),
			verifier:       func() (int64, error) { return int64(len(expectedContents)), nil },
			verifierCalled: 1,

			err: nil,
		},
		{
			name: "single getter, trusted IsTrusteder, partial contents",
			c:    someCID,
			getters: []Getter{
				trustedGetter{
					Getter: cookedGetter{
						Bytes: []byte(expectedContents)[1:],
					},
					Trusted: true,
				},
			},
			target:         memfile.New([]byte(expectedContents)[:1]),
			verifier:       func() (int64, error) { return int64(len(expectedContents)), nil },
			verifierCalled: 1,
			err:            nil,
		},
		{
			name: "single getter, trusted IsTrusteder",
			c:    someCID,
			getters: []Getter{
				trustedGetter{
					Getter: cookedGetter{
						Bytes: []byte(expectedContents),
					},
					Trusted: true,
				},
			},
			target:         memfile.New([]byte{}),
			verifier:       func() (int64, error) { return int64(len(expectedContents)), nil },
			verifierCalled: 0,

			err: nil,
		},
		{
			name: "single, trusted getter with partial contents",
			c:    someCID,
			getters: []Getter{
				trustedGetter{
					Getter: cookedGetter{
						Bytes: []byte(expectedContents)[5:],
					},
					Trusted: true,
				},
			},
			target:         memfile.New([]byte(expectedContents)[:5]),
			verifier:       func() (int64, error) { return int64(len(expectedContents)), nil },
			verifierCalled: 1,
			err:            nil,
		},
		{
			name: "single, trusted getter returning an error",
			c:    someCID,
			getters: []Getter{
				errorGetter{
					Getter: trustedGetter{
						Getter: cookedGetter{
							Bytes: []byte(expectedContents),
						},
						Trusted: true,
					},
					Err: sentinelErr,
				},
			},
			target:         memfile.New([]byte{}),
			verifier:       func() (int64, error) { return int64(len(expectedContents)), nil },
			verifierCalled: 0,
			err:            sentinelErr,
		},
		{
			name: "single, getter returning an error",
			c:    someCID,
			getters: []Getter{
				errorGetter{
					Getter: trustedGetter{
						Getter: cookedGetter{
							Bytes: []byte(expectedContents),
						},
						Trusted: false,
					},
					Err: sentinelErr,
				},
			},
			target:         memfile.New([]byte{}),
			verifier:       func() (int64, error) { return int64(len(expectedContents)), nil },
			verifierCalled: 0,
			err:            sentinelErr,
		},

		{
			name: "single, trusted getter returning an error, partial contents",
			c:    someCID,
			getters: []Getter{
				errorGetter{
					Getter: trustedGetter{
						Getter: cookedGetter{
							Bytes: []byte(expectedContents)[4:],
						},
						Trusted: true,
					},
					Err: sentinelErr,
				},
			},
			target:         memfile.New([]byte(expectedContents)[:4]),
			verifier:       func() (int64, error) { return int64(len(expectedContents)), nil },
			verifierCalled: 0,
			err:            sentinelErr,
		},
		{
			name: "single getter returning an error, partial contents",
			c:    someCID,
			getters: []Getter{
				errorGetter{
					Getter: trustedGetter{
						Getter: cookedGetter{
							Bytes: []byte(expectedContents)[3:],
						},
						Trusted: false,
					},
					Err: sentinelErr,
				},
			},
			target:         memfile.New([]byte(expectedContents)[3:]),
			verifier:       func() (int64, error) { return int64(len(expectedContents)), nil },
			verifierCalled: 0,
			err:            sentinelErr,
		},
		{
			name: "one bad getter, one good",
			c:    someCID,
			getters: []Getter{
				errorGetter{
					Getter: cookedGetter{
						Bytes: []byte{},
					},
					Err: sentinelErr,
				},
				cookedGetter{
					Bytes: []byte(expectedContents),
				},
			},
			target:         memfile.New([]byte{}),
			verifier:       func() (int64, error) { return int64(len(expectedContents)), nil },
			verifierCalled: 1,

			err: nil,
		},
		{
			name: "one bad getter, one good, partial contents",
			c:    someCID,
			getters: []Getter{
				errorGetter{
					Getter: cookedGetter{
						Bytes: []byte{},
					},
					Err: sentinelErr,
				},
				cookedGetter{
					Bytes: []byte(expectedContents),
				},
			},
			target:         memfile.New([]byte(expectedContents)[:3]),
			verifierCalled: 1,
			verifier:       func() (int64, error) { return int64(len(expectedContents)), nil },
			err:            nil,
		},
		{
			name: "two good getters",
			c:    someCID,
			getters: []Getter{
				cookedGetter{
					Bytes: []byte(expectedContents),
				},
				mockGetter(
					func(context.Context, cid.Cid, int64) (io.ReadCloser, error) {
						panic("shouldn't be called")
					},
				),
			},
			target:         memfile.New([]byte{}),
			verifier:       func() (int64, error) { return int64(len(expectedContents)), nil },
			verifierCalled: 1,

			err: nil,
		},
		{
			name: "two good getters, partial contents",
			c:    someCID,
			getters: []Getter{
				cookedGetter{
					Bytes: []byte(expectedContents)[5:],
				},
				mockGetter(
					func(context.Context, cid.Cid, int64) (io.ReadCloser, error) {
						panic("shouldn't be called")
					},
				),
			},
			target:         memfile.New([]byte(expectedContents)[:5]),
			verifier:       func() (int64, error) { return int64(len(expectedContents)), nil },
			verifierCalled: 1,
			err:            nil,
		},
		{
			name: "getter resumes previous",
			c:    someCID,
			getters: []Getter{
				cookedGetter{
					Bytes: []byte(expectedContents[:5]),
				},
				cookedGetter{
					Bytes: []byte(expectedContents)[5:],
				},
			},
			target:         memfile.New([]byte{}),
			verifier:       func() (int64, error) { return int64(len(expectedContents)), nil },
			verifierCalled: 1,
			err:            nil,
		},
		{
			name: "getter resumes previous, partial contents",
			c:    someCID,
			getters: []Getter{
				cookedGetter{
					Bytes: []byte(expectedContents[2:5]),
				},
				cookedGetter{
					Bytes: []byte(expectedContents)[5:],
				},
			},
			target:         memfile.New([]byte(expectedContents[:2])),
			verifier:       func() (int64, error) { return int64(len(expectedContents)), nil },
			verifierCalled: 1,
			err:            nil,
		},
		{
			name: "fully-retrieved",
			c:    someCID,
			getters: []Getter{
				cookedGetter{
					Bytes: []byte{},
				},
			},
			target:         memfile.New([]byte(expectedContents)),
			verifier:       func() (int64, error) { return int64(len(expectedContents)), nil },
			verifierCalled: 1,
			err:            nil,
		},
		{
			name: "trusted, fully-retrieved",
			c:    someCID,
			getters: []Getter{
				trustedGetter{
					Getter: cookedGetter{
						Bytes: []byte{},
					},
					Trusted: true,
				},
			},
			target:   memfile.New([]byte(expectedContents)),
			verifier: func() (int64, error) { return int64(len(expectedContents)), nil },

			verifierCalled: 1,
			err:            nil,
		},
		{
			name: "2 getters, first errors and has cancelled context",
			c:    someCID,
			ctx:  canceledCtx,
			getters: []Getter{
				errorGetter{
					Getter: cookedGetter{
						Bytes: []byte(expectedContents),
					},
					Err: errors.New("errorGetter"),
				},
				mockGetter(
					func(context.Context, cid.Cid, int64) (io.ReadCloser, error) {
						panic("shouldn't be called")
					},
				),
			},
			target:   memfile.New([]byte{}),
			verifier: func() (int64, error) { return int64(len(expectedContents)), nil },

			verifierCalled: 0,
			err:            context.Canceled,
		},
		{
			name: "2 getters, first errors and cancels context",
			ctx:  canceleableCtx,
			c:    someCID,
			getters: []Getter{
				mockGetter(
					func(_ context.Context, _ cid.Cid, pos int64) (io.ReadCloser, error) {
						cccFn()
						return io.NopCloser(bytes.NewReader([]byte(expectedContents))), sentinelErr
					},
				),
				mockGetter(
					func(context.Context, cid.Cid, int64) (io.ReadCloser, error) {
						panic("shouldn't be called")
					},
				),
			},
			target:   memfile.New([]byte{}),
			verifier: func() (int64, error) { return int64(len(expectedContents)), nil },

			verifierCalled: 0,
			err:            context.Canceled,
		},
		{
			name: "2 getters, first errors and has cancelled context, partial contents",
			c:    someCID,
			ctx:  canceledCtx,
			getters: []Getter{
				errorGetter{
					Getter: cookedGetter{
						Bytes: []byte(expectedContents)[3:],
					},
					Err: errors.New("errorGetter"),
				},
				mockGetter(
					func(context.Context, cid.Cid, int64) (io.ReadCloser, error) {
						panic("shouldn't be called")
					},
				),
			},
			target:   memfile.New([]byte(expectedContents)[:3]),
			verifier: func() (int64, error) { return int64(len(expectedContents)), nil },

			verifierCalled: 0,
			err:            context.Canceled,
		},
		{
			name: "2 getters, first errors and cancels context, partial contents",
			ctx:  canceleableCtx,
			c:    someCID,
			getters: []Getter{
				mockGetter(
					func(context.Context, cid.Cid, int64) (io.ReadCloser, error) {
						cccFn()
						return io.NopCloser(bytes.NewReader([]byte(expectedContents)[2:])), sentinelErr
					},
				),
				mockGetter(
					func(context.Context, cid.Cid, int64) (io.ReadCloser, error) {
						panic("shouldn't be called")
					},
				),
			},
			target:   memfile.New([]byte(expectedContents)[2:]),
			verifier: func() (int64, error) { return int64(len(expectedContents)), nil },

			verifierCalled: 0,
			err:            context.Canceled,
		},
		{
			name: "2 trusted getters, Seek on target fails",
			c:    someCID,
			getters: []Getter{
				trustedGetter{
					Trusted: true,
					Getter: mockGetter(
						func(context.Context, cid.Cid, int64) (io.ReadCloser, error) {
							panic("shouldn't be called")
						},
					),
				},
				trustedGetter{
					Trusted: true,
					Getter: mockGetter(
						func(context.Context, cid.Cid, int64) (io.ReadCloser, error) {
							panic("shouldn't be called")
						},
					),
				},
			},
			target: &errorSeeker{
				WriteSeeker: memfile.New([]byte{}),
				Err:         sentinelErr,
				Bypass:      1,
			},
			verifier: func() (int64, error) { return int64(len(expectedContents)), nil },

			verifierCalled: 0,
			err:            sentinelErr,
		},
		{
			name: "2 trusted getters, Seek on target fails, partial contents",
			c:    someCID,
			getters: []Getter{
				trustedGetter{
					Trusted: true,
					Getter: mockGetter(
						func(context.Context, cid.Cid, int64) (io.ReadCloser, error) {
							panic("shouldn't be called")
						},
					),
				},
				trustedGetter{
					Trusted: true,
					Getter: mockGetter(
						func(context.Context, cid.Cid, int64) (io.ReadCloser, error) {
							panic("shouldn't be called")
						},
					),
				},
			},
			target: &errorSeeker{
				WriteSeeker: memfile.New([]byte(expectedContents)[4:]),
				Err:         sentinelErr,
				Bypass:      1,
			},
			verifier: func() (int64, error) { return int64(len(expectedContents)), nil },
			err:      sentinelErr,
		},
		{
			name: "2 getters, first fails verification, partial contents",
			c:    someCID,
			getters: []Getter{
				cookedGetter{Bytes: []byte(expectedContents)[6:]},
				cookedGetter{Bytes: []byte(expectedContents)},
			},
			target:         memfile.New([]byte(expectedContents)[:6]),
			verifier:       newStatefulVerifier([]error{errors.New("first verifier"), nil}, int64(len(expectedContents))).verify,
			verifierCalled: 2,
			err:            nil,
		},
		{
			name: "2 getters, first fails verification",
			c:    someCID,
			getters: []Getter{
				cookedGetter{Bytes: []byte(expectedContents)},
				cookedGetter{Bytes: []byte(expectedContents)},
			},
			target:         memfile.New([]byte{}),
			verifier:       newStatefulVerifier([]error{errors.New("!"), nil}, int64(len(expectedContents))).verify,
			verifierCalled: 2,

			err: nil,
		},
		{
			name: "2 getters, first appends garbage bytes, second OK",
			c:    someCID,
			getters: []Getter{
				cookedGetter{Bytes: append([]byte(expectedContents), byte('a'))},
				cookedGetter{Bytes: []byte(expectedContents)},
			},
			target:         memfile.New([]byte{}),
			verifier:       newStatefulVerifier([]error{nil}, int64(len(expectedContents))).verify,
			verifierCalled: 1,
			err:            nil,
		},
		{
			name: "2 getters, first writes too many bytes",
			c:    someCID,
			getters: []Getter{
				cookedGetter{Bytes: append([]byte("ab"), []byte(expectedContents)...)},
				cookedGetter{
					Bytes: []byte(expectedContents),
				},
			},
			target:         memfile.New([]byte{}),
			verifierCalled: 1,
			err:            nil,
		},
		{
			name: "getter writes too many bytes",
			c:    someCID,
			getters: []Getter{
				cookedGetter{
					Bytes: append(
						[]byte("llo, Gophers."),
						[]byte{'a', 'b', 'c', 'd'}...,
					),
				},
			},
			target:         memfile.New([]byte{}),
			verifierCalled: 0,
			err:            ErrLongWrite,
		},
		{
			name: "getter writes too many bytes, partial contents",
			c:    someCID,
			getters: []Getter{
				cookedGetter{
					Bytes: append(
						[]byte("llo, Gophers."),
						[]byte{'a', 'b', 'c', 'd'}...,
					),
				},
			},
			target:         memfile.New([]byte(expectedContents)[:2]),
			verifierCalled: 0,
			err:            ErrLongWrite,
		},
		{
			name: "getter resumes, writes too few bytes, partial contents",
			c:    someCID,
			getters: []Getter{
				cookedGetter{
					Bytes: []byte(expectedContents)[5:7],
				},
			},
			target:   memfile.New([]byte(expectedContents)[:5]),
			verifier: func() (int64, error) { return int64(len(expectedContents)), sentinelErr },

			verifierCalled: 0,
			err:            ErrShortWrite,
		},
		{
			name: "getter resumes, writes 0 bytes, partial contents",
			c:    someCID,
			getters: []Getter{
				cookedGetter{
					Bytes: []byte{},
				},
			},
			target:   memfile.New([]byte(expectedContents)[:5]),
			verifier: func() (int64, error) { return int64(len(expectedContents)), sentinelErr },

			verifierCalled: 0,
			err:            ErrShortWrite,
		},
		{
			name: "ReadCloser errors",
			c:    someCID,
			getters: []Getter{
				mockGetter(func(context.Context, cid.Cid, int64) (io.ReadCloser, error) {
					return io.NopCloser(iotest.ErrReader(sentinelErr)), nil
				}),
			},
			target: memfile.New([]byte{}),
			err:    sentinelErr,
		},
		{
			name: "partial contents larger than expected total size",
			c:    someCID,
			getters: []Getter{
				cookedGetter{
					Bytes: []byte(expectedContents),
				},
			},
			target:   memfile.New(append([]byte(expectedContents), byte('f'))),
			verifier: func() (int64, error) { return int64(len(expectedContents)), sentinelErr },

			verifierCalled: 0,
			err:            ErrNothingToResume,
		},
		{
			name: "ReadCloser begins writing then errors",
			c:    someCID,
			getters: []Getter{
				mockGetter(func(context.Context, cid.Cid, int64) (io.ReadCloser, error) {
					return io.NopCloser(io.MultiReader(
						bytes.NewReader([]byte(expectedContents[:5])),
						iotest.ErrReader(sentinelErr)),
					), nil
				}),
			},
			target: memfile.New([]byte{}),
			err:    sentinelErr,
		},
		{
			name: "ExpectedSize and size returned from verifier differ",
			c:    someCID,
			getters: []Getter{
				cookedGetter{
					Bytes: []byte(expectedContents),
				},
			},
			target:         memfile.New([]byte{}),
			verifier:       func() (int64, error) { return int64(len(expectedContents)) + 1, nil },
			verifierCalled: 1,
			err:            ErrSizeMismatch,
		},
		{
			name: "size returned from verifier is negative",
			c:    someCID,
			getters: []Getter{
				cookedGetter{
					Bytes: []byte(expectedContents),
				},
			},
			target:         memfile.New([]byte{}),
			verifier:       func() (int64, error) { return -1, nil },
			verifierCalled: 1,
			err:            ErrSizeMismatch,
		},
		{
			name: "initial file appears complete but fails verification",
			c:    someCID,
			getters: []Getter{
				cookedGetter{
					Bytes: []byte(expectedContents),
				},
			},
			target:         memfile.New(append([]byte(expectedContents)[1:], byte('z'))),
			verifier:       newStatefulVerifier([]error{sentinelErr, nil}, int64(len(expectedContents))).verify,
			verifierCalled: 2,
			err:            nil,
		},
	}
	for _, tc := range testCases {
		tc := tc
		r := R{Getters: tc.getters}
		ctx := tc.ctx
		if ctx == nil {
			ctx = context.Background()
		}
		var verifierCalled int
		startPos, err := tc.target.Seek(0, io.SeekEnd)
		if err != nil {
			t.Errorf("%s: can't seek to end: %v", tc.name, err)
		}
		raw := r.Raw(tc.c)
		raw.ExpectedSize = int64(len(expectedContents))

		raw.Target = tc.target
		raw.VerifyFn = func() (int64, error) {
			verifierCalled++
			if tc.verifier == nil {
				return int64(len(expectedContents)), nil
			}
			return tc.verifier()
		}
		err = raw.Download(ctx)
		if verifierCalled != tc.verifierCalled {
			t.Errorf("%s: expected verifierCalled == %d; got %d", tc.name, tc.verifierCalled, verifierCalled)
		}
		if !errors.Is(err, tc.err) {
			t.Errorf("%s: expected err (%#v); err = %#v (%s)", tc.name, tc.err, err, err)
		}
		if tc.err != nil {
			if byter, ok := tc.target.(interface{ Bytes() []byte }); ok && startPos <= int64(len(expectedContents)) && len(byter.Bytes()) > int(len(expectedContents)) {
				t.Errorf("%s: expected target to be at most %d on error; it contains %#v", tc.name, len(expectedContents), byter.Bytes())
			}
			continue
		}
		_, err = tc.target.Seek(0, io.SeekStart)
		if err != nil {
			t.Error(err)
		}
		targetContents, err := io.ReadAll(tc.target.(io.Reader))
		if err != nil {
			t.Error(err)
		}
		if !bytes.Equal(targetContents, []byte(expectedContents)) {
			t.Errorf("%s: expected target to contain <%v>; got <%v>", tc.name, []byte(expectedContents), targetContents)
		}
	}
}

type isSaneCase struct {
	raw        Raw
	err        error
	targetSize int64
}

func TestRawisSane(t *testing.T) {
	someCID, _ := cid.Decode("bafkreicouidydstgdqaeb27msm4oc4e4njmsfx7feo3iys63faxhq7raqy")
	for name, tc := range map[string]isSaneCase{
		"no CID": {
			raw: Raw{
				Getters: []Getter{
					cookedGetter{Bytes: []byte("foo")},
				},
			},
			err:        ErrUndefinedCID,
			targetSize: 3,
		},
		"invalid expected size": {
			raw: Raw{
				Getters: []Getter{
					cookedGetter{Bytes: []byte("foo")},
				},
				CID:          someCID,
				ExpectedSize: -3,
			},
			err:        ErrInvalidExpectedSize,
			targetSize: 3,
		},
		"pos > ExpectedSize": {
			raw: Raw{
				Getters: []Getter{
					cookedGetter{Bytes: []byte("foo")},
				},
				CID:          someCID,
				ExpectedSize: 3,
			},
			err:        ErrNothingToResume,
			targetSize: 6,
		},
	} {
		if err := tc.raw.isSane(tc.targetSize); !errors.Is(err, tc.err) {
			t.Errorf("%s: isSane returned %v; expected %v", name, err, tc.err)
		}
	}
}
