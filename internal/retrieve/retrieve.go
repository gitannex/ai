package retrieve

import (
	"context"
	"errors"
	"fmt"
	"io"

	"github.com/hashicorp/go-multierror"
	blocks "github.com/ipfs/go-block-format"
	"github.com/ipfs/go-cid"
)

// TODO: Document how Get should behave if the target size indicates
// the payload has been completely received. A nil error is
// insufficient because this could cause a Get to be wrongly regarded
// as verified. Perhaps a sentinel error?
type Getter interface {
	Get(context.Context, cid.Cid, int64) (io.ReadCloser, error)
}

type BlockGetter interface {
	GetBlock(context.Context, cid.Cid) (blocks.Block, error)
}

// TODO: IsVerifier / IsVerified
type IsTrusteder interface {
	IsTrusted() bool
}

type R struct {
	Getters      []Getter
	BlockGetters []BlockGetter
}

// TODO: Require ExpectedSize argument?
func (r *R) Raw(c cid.Cid) *Raw {
	return &Raw{Getters: r.Getters, CID: c}
}

var (
	ErrNoBlockGetters = errors.New("no BlockGetters configured")
	ErrBadBlock       = errors.New("invalid block")
)

// GetBlock returns the block with CID c it receives from one of the configured [BlockGetter]s. It confirms that the block's reported CID is equal to c, but assumes the [BlockGetter] has hashed the block.
func (r *R) GetBlock(ctx context.Context, c cid.Cid) (blocks.Block, error) {
	if len(r.BlockGetters) == 0 {
		return nil, ErrNoBlockGetters
	}
	var errs *multierror.Error
	for _, bGetter := range r.BlockGetters {
		bl, err := bGetter.GetBlock(ctx, c)
		if err == nil {
			if bl.Cid().Equals(c) {
				return bl, nil
			}
			err = fmt.Errorf("%w: block had CID of %s; expected CID of %s", ErrBadBlock, bl.Cid(), c)
		}
		errs = multierror.Append(errs, err)
		if ctx.Err() != nil {
			errs = multierror.Append(errs, ctx.Err())
			break
		}
	}
	if errs != nil {
		errs.ErrorFormat = func([]error) string {
			return "no BlockGetters succeeded"
		}
	}
	return nil, errs
}
