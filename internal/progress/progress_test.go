package progress

import (
	"bytes"
	"errors"
	"io"
	"strings"
	"testing"

	"github.com/dsnet/golib/memfile"
)

func TestReaderMultiplier1(t *testing.T) {
	var reported int64
	fn := func(p int64) {
		reported = p
	}
	payload := strings.Repeat("abc", 100)
	payloadRdr := strings.NewReader(payload)
	pr := NewTracker(fn, int64(len(payload)))
	rdr := pr.NewReader(payloadRdr)
	buf := make([]byte, 3)
	for i := 0; i < 100; i++ {
		n, err := rdr.Read(buf)
		if n != 3 || err != nil {
			t.Errorf("expected n==3, err==nil; got %d, %v", n, err)
		}
		expectReported := 3 * (i + 1)
		if i == 99 {
			expectReported -= 1
		}
		if reported != int64(expectReported) {
			t.Errorf("expected reported == %d, reported = %d", expectReported, reported)
		}

	}
	if reported >= int64(len(payload)) {
		t.Errorf("expected reported to be less than payload length; got %d", reported)
	}
	pr.Close()
	if reported != int64(len(payload)) {
		t.Errorf("expected reported to be equal to payload length; got %d", reported)
	}
}

func TestReaderMultiplierHalf(t *testing.T) {
	var reported int64
	fn := func(p int64) {
		reported = p
	}
	payload := strings.Repeat("half", 240)
	payloadRdr := strings.NewReader(payload)
	pr := NewTracker(fn, int64(len(payload)))
	pr.SetOverhead(-int64(len(payload)) / 2)
	rdr := pr.NewReader(payloadRdr)
	buf := make([]byte, 12)
	for i := 0; i < len(payload)/len(buf); i++ {
		n, err := rdr.Read(buf)
		if n != len(buf) || err != nil {
			t.Errorf("expected n==%d, err==nil; got %d, %v", len(buf), n, err)
		}
		expectReported := (len(buf) * (i + 1)) / 2
		if i == len(payload)/len(buf) {
			expectReported -= 1
		}
		if reported != int64(expectReported) {
			t.Errorf("expected reported == %d, reported = %d", expectReported, reported)
		}

	}
	if reported >= int64(len(payload)) {
		t.Errorf("expected reported to be less than payload length; got %d", reported)
	}
	pr.Close()
	if reported != int64(len(payload)) {
		t.Errorf("expected reported to be equal to payload length; got %d", reported)
	}
}

func TestReaderOverread(t *testing.T) {
	var reported int64
	fn := func(p int64) {
		reported = p
	}
	payload := "payload"
	payloadRdr := strings.NewReader(payload)
	pr := NewTracker(fn, int64(len(payload)))
	rdr := pr.NewReader(payloadRdr)
	buf := make([]byte, len(payload)+1)
	n, err := rdr.Read(buf)
	if n != len(payload) && err != io.EOF {
		t.Errorf("expected n = %d, err = io.EOF; got %d, %v", len(payload), n, err)
	}
	if reported != int64(len(payload))-1 {
		t.Errorf("expected reported = %d; got %d", len(payload)-1, reported)
	}
	pr.Close()
	if reported != int64(len(payload)) {
		t.Errorf("expected reported = %d; got %d", len(payload), reported)
	}
}

type errRdr struct {
	i int
}

func (er *errRdr) Read(p []byte) (int, error) {
	er.i += 1
	if er.i < 3 {
		p = bytes.Repeat([]byte{'a'}, len(p))
		return len(p), nil
	}
	p = bytes.Repeat([]byte{'b'}, len(p)-2)
	return len(p), errors.New("some err")
}

func TestReaderErr(t *testing.T) {
	var reported int64
	fn := func(p int64) {
		reported = p
	}
	pr := NewTracker(fn, 28)
	rdr := pr.NewReader(&errRdr{})
	buf := make([]byte, 10)
	for i := 0; i < 2; i++ {
		n, err := rdr.Read(buf)
		if n != 10 && err != nil {
			t.Errorf("expected n = 10, err = nil; got %d, %v", n, err)
		}
		if reported != int64(10*(i+1)) {
			t.Errorf("expected reported = %d; got %d", 10*(i+1), reported)
		}
	}
	n, err := rdr.Read(buf)
	if n != 8 && err != nil {
		t.Errorf("expected final read to yield 8 bytes and an error; got %d, %v", n, err)
	}
	if reported != 27 {
		t.Errorf("expected reported to be 27; got %d", reported)
	}
	pr.Close()
	if reported != 27 {
		t.Errorf("expected reported to be 27 after close; got %d", reported)
	}
}

func TestWriteSeek(t *testing.T) {
	mf := memfile.New([]byte(`goof`))
	var progress int64
	proFn := func(p int64) {
		progress = p
	}
	pro := NewTracker(proFn, 23)
	pw := pro.NewWriter(mf)

	n, err := pw.Write([]byte(`ball`))
	if n != 4 {
		t.Errorf("expected to write 4; wrote %d", n)
	}
	if err != nil {
		t.Errorf("expected to write without error; got %v", err)
	}
	if progress < 3 || progress > 5 {
		t.Errorf("expected progress to be between 3 and 5; got %d", progress)
	}
	pos, err := pw.Seek(0, io.SeekStart)
	if pos != 0 {
		t.Errorf("expected pos to be 0; got %d", pos)
	}
	if err != nil {
		t.Errorf("expected to seek without error; got %v", err)
	}
	n, err = pw.Write([]byte(`ball`))
	if n != 4 {
		t.Errorf("expected to write 4; wrote %d", n)
	}
	if err != nil {
		t.Errorf("expected to write without error; got %v", err)
	}
	if progress < 3 || progress > 5 {
		t.Errorf("expected progress to be between 3 and 5; got %d", progress)
	}
	n, err = pw.Write([]byte(` player`))
	if n != 7 {
		t.Errorf("expected to write 7; wrote %d", n)
	}
	if err != nil {
		t.Errorf("expected to write without error; got %v", err)
	}
	if progress < 10 || progress > 12 {
		t.Errorf("expected progress to be between 10 and 12; got %d", progress)
	}

	pos, err = pw.Seek(2, io.SeekCurrent)
	if pos != 13 {
		t.Errorf("expected pos to be 13; got %d", pos)
	}
	if err != nil {
		t.Errorf("expected to seek without error; got %v", err)
	}
	if progress < 10 || progress > 12 {
		t.Errorf("expected progress to be between 10 and 12; got %d", progress)
	}

	n, err = pw.Write([]byte(`s play ball`))
	if n != 11 {
		t.Errorf("expected to write 11; wrote %d", n)
	}
	if err != nil {
		t.Errorf("expected to write without error; got %v", err)
	}
	if progress < 21 || progress > 22 {
		t.Errorf("expected progress to be between 21 and 22; got %d", progress)
	}

	pos, err = pw.Seek(0, io.SeekEnd)
	if pos != 24 {
		t.Errorf("expected pos to be 24; got %d", pos)
	}
	if err != nil {
		t.Errorf("expected to seek without error; got %v", err)
	}
	n, err = pw.Write([]byte(`.`))
	if n != 1 {
		t.Errorf("expected to write 1; wrote %d", n)
	}
	if err != nil {
		t.Errorf("expected to write without error; got %v", err)
	}
	if progress < 22 || progress > 23 {
		t.Errorf("expected progress to be between 22 and 23; got %d", progress)
	}
	_ = pro.Close()
	if progress != 23 {
		t.Errorf("expected final progress to be 23; got %d", progress)
	}
}

func TestWriteSeekAppend(t *testing.T) {
	mf := memfile.New([]byte(`goof`))
	pos, err := mf.Seek(0, io.SeekEnd)
	if pos != 4 {
		t.Fatalf("expected initial pos to be 4; got %d", pos)
	}
	if err != nil {
		t.Fatalf("expected to seek without error; got %d", err)
	}
	var progress int64
	proFn := func(p int64) {
		progress = p
	}
	pro := NewTracker(proFn, 23)
	pw := pro.NewWriter(mf)

	n, err := pw.Write([]byte(`ball`))
	if n != 4 {
		t.Errorf("expected to write 4; wrote %d", n)
	}
	if err != nil {
		t.Errorf("expected to write without error; got %v", err)
	}
	if progress < 7 || progress > 9 {
		t.Errorf("expected progress to be between 7 and 9; got %d", progress)
	}
	pos, err = pw.Seek(0, io.SeekStart)
	if pos != 0 {
		t.Errorf("expected pos to be 0; got %d", pos)
	}
	if err != nil {
		t.Errorf("expected to seek without error; got %v", err)
	}
	n, err = pw.Write([]byte(`ball`))
	if n != 4 {
		t.Errorf("expected to write 4; wrote %d", n)
	}
	if err != nil {
		t.Errorf("expected to write without error; got %v", err)
	}
	if progress < 7 || progress > 9 {
		t.Errorf("expected progress to be between 7 and 9; got %d", progress)
	}
	n, err = pw.Write([]byte(` player`))
	if n != 7 {
		t.Errorf("expected to write 7; wrote %d", n)
	}
	if err != nil {
		t.Errorf("expected to write without error; got %v", err)
	}
	if progress < 10 || progress > 12 {
		t.Errorf("expected progress to be between 10 and 12; got %d", progress)
	}

	pos, err = pw.Seek(2, io.SeekCurrent)
	if pos != 13 {
		t.Errorf("expected pos to be 13; got %d", pos)
	}
	if err != nil {
		t.Errorf("expected to seek without error; got %v", err)
	}
	if progress < 10 || progress > 12 {
		t.Errorf("expected progress to be between 10 and 12; got %d", progress)
	}

	n, err = pw.Write([]byte(`s play ball`))
	if n != 11 {
		t.Errorf("expected to write 11; wrote %d", n)
	}
	if err != nil {
		t.Errorf("expected to write without error; got %v", err)
	}
	if progress < 21 || progress > 22 {
		t.Errorf("expected progress to be between 21 and 22; got %d", progress)
	}

	pos, err = pw.Seek(0, io.SeekEnd)
	if pos != 24 {
		t.Errorf("expected pos to be 24; got %d", pos)
	}
	if err != nil {
		t.Errorf("expected to seek without error; got %v", err)
	}
	n, err = pw.Write([]byte(`.`))
	if n != 1 {
		t.Errorf("expected to write 1; wrote %d", n)
	}
	if err != nil {
		t.Errorf("expected to write without error; got %v", err)
	}
	if progress < 22 || progress > 23 {
		t.Errorf("expected progress to be between 22 and 23; got %d", progress)
	}
	_ = pro.Close()
	if progress != 23 {
		t.Errorf("expected final progress to be 23; got %d", progress)
	}
}
