package progress

import (
	"errors"
	"io"
)

type WriteSeekCloser interface {
	io.WriteSeeker
	io.Closer
}

func (tra *Tracker) NewWriter(orig io.WriteSeeker) WriteSeekCloser {
	wri := &Writer{Tracker: tra, WriteSeeker: orig}
	cur, err := wri.Seek(0, io.SeekCurrent)
	if err != nil {
		return wri
	}
	report := int64(float64(cur) * wri.mult)
	wri.Fn(report)
	wri.TotalReported = report
	return wri
}

type Writer struct {
	*Tracker
	io.WriteSeeker
	pos int64
}

func (wri *Writer) Write(p []byte) (int, error) {
	n, err := wri.WriteSeeker.Write(p)
	if n > 0 {
		wri.Lock()
		wri.pos += int64(n)
		report := wri.TotalReported + int64((float64(n) * wri.mult))
		if report >= wri.pos {
			// The multiplier may not be exact, so allow
			// for some wiggle room by reporting 1 byte
			// less than the total file size here, then
			// when Close() is called, report any
			// remaining bytes.
			report = wri.pos - 1
		}
		if report > wri.TotalReported && report > 0 {
			wri.Fn(report)
		}
		wri.TotalReported = report
		wri.Unlock()

	}
	if err != nil && !errors.Is(err, io.EOF) {
		wri.Lock()
		if wri.Err == nil {
			wri.Err = err
		}
		wri.Unlock()
	}
	return n, err
}

func (wri *Writer) Seek(off int64, whence int) (int64, error) {
	pos, err := wri.WriteSeeker.Seek(off, whence)
	if err == nil {
		wri.pos = pos
	}
	return pos, err
}
