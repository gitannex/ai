package progress

import (
	"sync"
)

type Tracker struct {
	FileSize      int64
	Fn            func(int64)
	mult          float64
	TotalReported int64
	Err           error
	IsClosed      bool
	sync.Mutex
}

// TODO: panic on FileSize==0?
func NewTracker(fn func(int64), fileSize int64) *Tracker {
	return &Tracker{
		Fn:       fn,
		FileSize: fileSize,
		mult:     1,
	}
}

func (tra *Tracker) SetOverhead(over int64) {
	if tra.FileSize == 0 {
		tra.mult = 1
		return
	}
	tra.mult = float64(tra.FileSize+over) / float64(tra.FileSize)
}

func (tra *Tracker) Close() error {
	tra.Lock()
	defer tra.Unlock()
	if tra.Err != nil {
		return tra.Err
	}
	if tmp := tra.FileSize - tra.TotalReported; tra.Fn != nil && tmp > 0 {
		tra.Fn(tra.FileSize)
		tra.TotalReported = tra.FileSize
	}
	return nil
}
