package progress

import (
	"errors"
	"io"
)

// TODO: deal with initial pos != 0 as Writer does
func (tra *Tracker) NewReader(orig io.Reader) io.ReadCloser {
	return &Reader{Tracker: tra, Reader: orig}
}

type Reader struct {
	*Tracker
	io.Reader
}

// TODO: If TotalReported is set before we start reading, report that figure before the first read.
func (rdr *Reader) Read(p []byte) (int, error) {
	n, err := rdr.Reader.Read(p)
	if n > 0 {
		rdr.Lock()
		report := rdr.TotalReported + int64((float64(n) * rdr.mult))
		if report >= rdr.FileSize {
			// The multiplier may not be exact, so allow
			// for some wiggle room by reporting 1 byte
			// less than the total file size here, then
			// when Close() is called, report any
			// remaining bytes.
			report = rdr.FileSize - 1
		}
		if report > rdr.TotalReported && report > 0 {
			rdr.Fn(report)
		}
		rdr.TotalReported = report
		rdr.Unlock()

	}
	if err != nil && !errors.Is(err, io.EOF) {
		rdr.Lock()
		if rdr.Err == nil {
			rdr.Err = err
		}
		rdr.Unlock()
	}
	return n, err
}
