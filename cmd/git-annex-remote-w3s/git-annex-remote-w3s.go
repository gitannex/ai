package main

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"

	"ai.camelcase.link"
	"ai.camelcase.link/w3s"
	"github.com/dzhu/go-git-annex-external/remote"
)

func main() {
	if len(os.Args) == 2 && os.Args[1] == "-h" {
		fmt.Println("external remote for git-annex to use web3.storage")
		os.Exit(0)
	}
	ua := http.Client{
		Transport: ai.NewTransport(),
	}
	ai := ai.AI{
		I:    nil,
		UA:   &ua,
		Prep: new(unprepared),
		R:    ai.DefaultRetriever(&ua),
	}
	remote.Run(&ai)
}

type unprepared struct{}

func (unp *unprepared) Prepare(a remote.Annex, ua *http.Client) (ai.IPFSRemote, error) {
	cl, err := w3s.New(
		w3s.WithToken(withToken(a)),
		w3s.WithHTTPClient(ua),
		w3s.WithEndpoint(a.GetConfig("endpoint")),
		w3s.WithCanPin(withCanPin(a)),
		w3s.WithTempDir(withTempDir(a)),
	)

	return cl, err
}

func (unp *unprepared) Configs() map[string]string {
	return map[string]string{
		"endpoint": "URL of API endpoint (optional)",
		"canpin":   "Do you have access to the pinning API? (can be 'true' or 'false'; default: 'false')",
	}
}

func withToken(a remote.Annex) string {
	user, pass := a.GetCreds("jwt") // Hardcoded?
	if user != "" {
		a.Errorf("malformed credentials: 'user' isn't empty")
	}
	return pass
}

func withCanPin(a remote.Annex) bool {
	v := a.GetConfig("canpin")
	if v == "true" {
		return true
	} else if v == "false" || v == "" {
		return false
	}
	a.Errorf("invalid value for canpin: %s (should be 'true' or 'false')", v)
	return false
}

func withTempDir(a remote.Annex) string {
	gd := a.GetGitDir()
	if gd != "" {
		gd = filepath.Join(gd, "annex/tmp")
	}
	return gd
}
