.Dd July 3, 2022
.Dt GIT-ANNEX-REMOTE-W3S 1
.Os
.Sh NAME
.Nm git-annex-remote-w3s
.Nd Git Annex special remote for web3.storage
.Sh SYNOPSIS
.Nm git-annex-remote-w3s
.Op Fl h
.Sh DESCRIPTION
.Nm
is an
.Em external special remote
for
.Xr git-annex 1
that enables files to be stored on the IPFS network via
.Lk https://web3.storage/ .
.Pp
You won't typically interact with this program directly;
.Xr git-annex 1
invokes it as needed when a remote has been created with the
.Ar externaltype=w3s
option.
The sole argument is:
.Bl -tag -width Ds
.It Fl h
Print brief help message.
.El
.Pp
When this remote is initialised, with
.Xr git-annex-initremote 1 ,
the environment variable
.Ev AUTH_TOK
should be set to an API key---see
.Lk https://web3.storage/tokens/
for details---associated with your web3.storage account.
This credential is cached by Git Annex and needn't be provided again for the same repository.
To change the API key used, set
.Ev AUTH_TOK
to the new key and invoke
.Xr git-annex-enableremote 1 ,
as shown in the
.Sx EXAMPLES
section.
.Pp
Alternatively, this remote can be used in readonly mode.
If an Annex repository uses this remote to store content on web3.storage, then its clones can download this content without needing an web3.storage API key or even this program.
Just use the
.Ar readonly=true
option with
.Xr git-annex-enableremote 1
in the clone.
.Pp
You can optionally provide additional arguments to
.Xr git-annex-initremote 1 /
.Xr git-annex-enableremote 1
as follows:
.Pp
.Bl -bullet -compact -width 1
.It
.Cm endpoint
- main URL for the web3.storage API endpoint.
Default:
.Lk https://api.web3.storage/ .
.It
.Cm canpin
- whether you have access to the pinning endpoint.
Can be
.Dq true
or
.Dq false .
Default:
.Dq false .
.El
.Pp
The web3.storage pinning API is currently invite-only.
If we have access we can perform some functions more efficiently.
You can apply for access via
.Lk https://web3.storage/docs/how-tos/pinning-services-api/ ,
and if accepted set the
.Cm canpin
config variable to
.Dq true .
.Pp
If Git Annex requests to store a key on this remote, an IPFS CID is known for that key, and we have access to the pinning endpoint, we attempt to pin this CID with web3.storage, rather than uploading the key and potentially storing it under a different CID.
This is particularly useful if the key is already present on another IPFS special remote---reusing the same CID is more efficient and robust.
This approach relies on a URL/URI being associated with a key which contains its CID.
This URL/URI may be set by another remote or manually with
.Xr git-annex-registerurl 1 .
It is the user's responsibility to ensure any CID associated with a key is accurate.
.Pp
When retrieving a key from this remote we attempt to use a local IPFS daemon.
If we can't reach one we use a public gateway, such as
.Lk https://dweb.link/ .
To avoid having to trust this gateway to provide the correct content, we use another gateway to verify what we receive.
This approach isn't foolproof, so running a local IPFS daemon is recommended.
.Pp
Removing content from this remote requires access to the pinning API:
.Cm canpin
must be
.Dq true .
.Pp
Git Annex keys dropped from this remote may remain accessible via other IPFS nodes, due to the nature of IPFS. Keep this principle in mind when deciding whether this remote is appropriate for your data.
.Sh ENVIRONMENT
The following environment variables are used by
.Nm
when set:
.Bl -tag -width Ds
.It Ev AUTH_TOK
The JWT corresponding to the web3.storage account in use.
Only used by the
.Xr git-annex-initremote 1
and
.Xr git-annex-enableremote 1
commands.
.It Ev IPFS_PATH
The location of the IPFS repo.
On Linux this defaults to:
.Pa $HOME/.ipfs .
If set, the file
.Pa $IPFS_PATH/api
is assumed to contain the
.Em multiaddress
of the local IPFS node, e.g.
.Em /ip4/127.0.0.1/tcp/5001 ,
from which IPFS data should be retrieved.
.El
.Sh EXAMPLES
Create a remote named
.Va w3s
for the web3.storage account with API key
.Va api-key :
.Bd -literal
$ AUTH_TOK=api-key git annex initremote w3s \\
                   encryption=none type=external externaltype=w3s
.Ed
.Pp
Modify the existing remote
.Va w3s
to use the API key
.Va key2
and use the API endpoint
.Em https://example.com/ :
.Bd -literal
$ AUTH_TOK=key2 git annex enableremote w3s \\
                endpoint=https://example.com/ encryption=none \\
		type=external externaltype=w3s
.Ed
.Pp
Configure an existing web3.storage remote,
.Va w3s ,
in this repository in order to download via a public IPFS gateway:
.Bd -literal
$ git annex enableremote w3s readonly=true
.Ed
.Sh SEE ALSO
.Xr git-annex 1 ,
.Xr git-annex-enableremote 1 ,
.Xr git-annex-initremote 1 ,
.Xr git-annex-registerurl 1 ,
.Xr ipfs 1
.Ss WWW
.Bl -bullet -compact -width 1
.It
.Lk https://git-annex.branchable.com/special_remotes/ "Special Remotes"
.It
.Lk https://web3.storage/ "web3.storage"
.It
.Lk https://ipfs.github.io/pinning-services-api-spec/ "IPFS Pinning Service API"
.It
.Lk https://multiformats.io/multiaddr/ "multiaddresses"
.El
.Sh BUGS
To report a bug in this software or view the current known bugs see the
.Lk https://gitlab.com/gitannex/ai/-/issues "bug tracker" .
.Sh SECURITY
The default gateways and endpoints are communicated with via HTTPS; if you override these defaults to use HTTP URLS, transport security will be compromised.
.Pp
The JWT provided in the
.Ev AUTH_TOK
environment variable is visible to the root user when
.Xr git-annex-initremote 1
or
.Xr git-annex-enableremote 1
is invoked.
This value is then stored in plaintext in the Git Annex repository, so is visible to root and the user who owns the repository.
