package main

import (
	"net/http"
	"testing"

	"ai.camelcase.link/internal/mockannex"
	"ai.camelcase.link/w3s"
)

func TestWithTempDirGitDir(t *testing.T) {
	ma := new(mockannex.Annex)
	ma.GetGitDirFn = func() string {
		return ".git"
	}
	td := withTempDir(ma)
	if td != ".git/annex/tmp" {
		t.Errorf("expected temp dir = .git/annex/tmp; got %s", td)
	}
}

func TestWithTempDirNoGitDir(t *testing.T) {
	ma := new(mockannex.Annex)
	ma.GetGitDirFn = func() string {
		return ""
	}
	td := withTempDir(ma)
	if td != "" {
		t.Errorf("expected temp dir = ''; got %s", td)
	}
}

func TestWithCanPin(t *testing.T) {
	for str, tc := range map[string]struct {
		B bool
		E int
	}{
		"true":  {B: true},
		"false": {},
		"gahy":  {E: 1},
		"":      {},
	} {
		t.Run(str, func(t *testing.T) {
			ma := new(mockannex.Annex)
			ma.GetConfigFn = func(name string) string {
				if name != "canpin" {
					t.Errorf("expected name=canpin; got %s", name)
					return ""
				}
				return str
			}
			var errorFCalled int
			ma.ErrorFFn = func(fmt string, args ...interface{}) {
				errorFCalled++
			}
			cp := withCanPin(ma)
			if cp != tc.B {
				t.Errorf("expected cp to be %t; it wasn't", tc.B)
			}
			if tc.E != errorFCalled {
				t.Errorf("unexpected errors: expected %d errors; received %d errors", tc.E, errorFCalled)
			}
		})
	}
}

func TestWithTokenOK(t *testing.T) {
	ma := new(mockannex.Annex)
	ma.GetCredsFn = func(name string) (string, string) {
		if name != "jwt" {
			t.Errorf("expected name=jwt; got %s", name)
		}
		return "", "pass"
	}
	tok := withToken(ma)
	if tok != "pass" {
		t.Errorf("expected tok = pass; tok = %s", tok)
	}
}

func TestWithTokenUser(t *testing.T) {
	ma := new(mockannex.Annex)
	ma.GetCredsFn = func(name string) (string, string) {
		if name != "jwt" {
			t.Errorf("expected name=jwt; got %s", name)
		}
		return "user", "pass"
	}
	var errorFCalled int
	ma.ErrorFFn = func(fmt string, args ...interface{}) {
		errorFCalled++
	}

	tok := withToken(ma)
	if tok != "pass" {
		t.Errorf("expected tok = pass; tok = %s", tok)
	}
	if errorFCalled != 1 {
		t.Errorf("expected error message; a.Errorf wasn't called")
	}
}

func TestPrepareOK(t *testing.T) {
	ma := new(mockannex.Annex)
	ma.GetCredsFn = func(name string) (string, string) {
		if name == "jwt" {
			return "", "j"
		}
		return "", ""
	}
	ma.GetConfigFn = func(name string) string {
		if name == "endpoint" {
			return "https://localhost/"
		}
		if name == "canpin" {
			return "true"
		}
		return ""
	}
	ma.GetGitDirFn = func() string { return "/foo/.git" }
	ua := http.DefaultClient
	ir, err := (&unprepared{}).Prepare(ma, ua)
	if err != nil {
		t.Errorf("expected no error; got %v", err)
	}
	pClient, ok := ir.(*w3s.Client)
	if !ok {
		t.Fatal("expected IPFSRemote to be a *w3s.Client; it wasn't")
	}
	other, err := w3s.New(
		w3s.WithToken("j"),
		w3s.WithHTTPClient(ua),
		w3s.WithEndpoint("https://localhost/"),
		w3s.WithCanPin(true),
		w3s.WithTempDir("/foo/.git/annex/tmp"),
	)
	if err != nil {
		t.Error(err)
	}

	if !pClient.Equal(other) {
		t.Errorf("expected %+#v to Equal %+#v", pClient, other)
	}
}

func TestPrepareNotOK(t *testing.T) {
	ma := new(mockannex.Annex)
	ma.GetCredsFn = func(name string) (string, string) {
		// Empty creds are a fatal error for New
		return "", ""
	}
	ma.GetConfigFn = func(name string) string {
		if name == "endpoint" {
			return "https://localhost/"
		}
		if name == "canpin" {
			return "true"
		}
		return ""
	}
	ma.GetGitDirFn = func() string { return "/foo/.git" }
	ua := http.DefaultClient
	_, err := (&unprepared{}).Prepare(ma, ua)
	if err == nil {
		t.Errorf("expected error; got nil")
	}
}
