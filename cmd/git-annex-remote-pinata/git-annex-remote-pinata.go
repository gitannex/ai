package main

import (
	"fmt"
	"net/http"
	"os"

	"ai.camelcase.link"
	"ai.camelcase.link/pinata"
	"github.com/dzhu/go-git-annex-external/remote"
	"github.com/hashicorp/go-retryablehttp"
)

func main() {
	if len(os.Args) == 2 && os.Args[1] == "-h" {
		fmt.Println("external remote for git-annex to use pinata.cloud")
		os.Exit(0)
	}
	rua := retryablehttp.NewClient()
	rua.HTTPClient = &http.Client{
		Transport: ai.NewTransport(),
	}
	ai := ai.AI{
		I:    nil,
		UA:   rua.StandardClient(),
		Prep: new(unprepared),
		R:    ai.DefaultRetriever(http.DefaultClient),
	}

	remote.Run(&ai)
}

func withToken(a remote.Annex) string {
	user, pass := a.GetCreds("jwt") // Hardcoded?
	if user != "" {
		a.Errorf("malformed credentials: 'user' isn't empty")
	}
	return pass
}

type unprepared struct{}

func (unp *unprepared) Prepare(a remote.Annex, ua *http.Client) (ai.IPFSRemote, error) {
	cl, err := pinata.New(
		pinata.WithToken(withToken(a)),
		pinata.WithHTTPClient(ua),
		pinata.WithEndpoint(a.GetConfig("endpoint")),
		pinata.WithPSAEndpoint(a.GetConfig("psaendpoint")),
	)

	return cl, err
}

func (unp *unprepared) Configs() map[string]string {
	return map[string]string{
		"endpoint":    "URL of API endpoint (optional)",
		"psaendpoint": "URL of PSA endpoint (optional)",
	}
}
