package main

import (
	"net/http"
	"testing"

	"ai.camelcase.link/internal/mockannex"
	"ai.camelcase.link/pinata"
)

func TestWithTokenOK(t *testing.T) {
	ma := new(mockannex.Annex)
	ma.GetCredsFn = func(name string) (string, string) {
		if name != "jwt" {
			t.Errorf("expected name=jwt; got %s", name)
		}
		return "", "pass"
	}
	tok := withToken(ma)
	if tok != "pass" {
		t.Errorf("expected tok = pass; tok = %s", tok)
	}
}

func TestWithTokenUser(t *testing.T) {
	ma := new(mockannex.Annex)
	ma.GetCredsFn = func(name string) (string, string) {
		if name != "jwt" {
			t.Errorf("expected name=jwt; got %s", name)
		}
		return "user", "pass"
	}
	var errorFCalled int
	ma.ErrorFFn = func(fmt string, args ...interface{}) {
		errorFCalled++
	}

	tok := withToken(ma)
	if tok != "pass" {
		t.Errorf("expected tok = pass; tok = %s", tok)
	}
	if errorFCalled != 1 {
		t.Errorf("expected error message; a.Errorf wasn't called")
	}
}

func TestPrepareOK(t *testing.T) {
	ma := new(mockannex.Annex)
	ma.GetCredsFn = func(name string) (string, string) {
		if name == "jwt" {
			return "", "j"
		}
		return "", ""
	}
	ma.GetConfigFn = func(name string) string {
		if name == "endpoint" {
			return "https://localhost/"
		}
		if name == "psaendpoint" {
			return "https://psa.example.org/"
		}
		return ""
	}
	ua := http.DefaultClient
	ir, err := (&unprepared{}).Prepare(ma, ua)
	if err != nil {
		t.Errorf("expected no error; got %v", err)
	}
	pClient, ok := ir.(*pinata.Client)
	if !ok {
		t.Fatal("expected IPFSRemote to be a *pinata.Client; it wasn't")
	}
	other, err := pinata.New(
		pinata.WithToken("j"),
		pinata.WithHTTPClient(ua),
		pinata.WithPSAEndpoint("https://psa.example.org/"),
		pinata.WithEndpoint("https://localhost/"),
	)
	if err != nil {
		t.Error(err)
	}

	if !pClient.Equal(other) {
		t.Errorf("expected %+#v to Equal %+#v", pClient, other)
	}
}

func TestPrepareNotOK(t *testing.T) {
	ma := new(mockannex.Annex)
	ma.GetCredsFn = func(name string) (string, string) {
		// Empty creds are a fatal error for New
		return "", ""
	}
	ma.GetConfigFn = func(name string) string {
		if name == "endpoint" {
			return "https://localhost/"
		}
		if name == "psaendpoint" {
			return "https://example.com/psa"
		}
		return ""
	}
	ua := http.DefaultClient
	_, err := (&unprepared{}).Prepare(ma, ua)
	if err == nil {
		t.Errorf("expected error; got nil")
	}
}
