package main

import (
	"fmt"
	"net/http"
	"os"

	"ai.camelcase.link"
	"ai.camelcase.link/estuary"
	"ai.camelcase.link/internal/retrieve"
	"github.com/dzhu/go-git-annex-external/remote"
)

func main() {
	if len(os.Args) == 2 && os.Args[1] == "-h" {
		fmt.Println("external remote for git-annex to use Estuary")
		os.Exit(0)
	}
	ua := http.Client{
		Transport: ai.NewTransport(),
	}
	ret := ai.DefaultRetriever(&ua)
	ret.Getters = append([]retrieve.Getter{&ai.Gateway{UA: &ua, URL: ai.EstuaryTech}}, ret.Getters...)
	ai := ai.AI{
		I:    nil,
		UA:   &ua,
		Prep: new(unprepared),
		R:    ret,
	}

	remote.Run(&ai)
}

func withToken(a remote.Annex) string {
	user, pass := a.GetCreds("jwt") // Hardcoded?
	if user != "" {
		a.Errorf("malformed credentials: 'user' isn't empty")
	}
	return pass
}

type unprepared struct{}

func (unp *unprepared) Prepare(a remote.Annex, ua *http.Client) (ai.IPFSRemote, error) {
	cl, err := estuary.New(
		estuary.WithToken(withToken(a)),
		estuary.WithHTTPClient(ua),
		estuary.WithEndpoint(a.GetConfig("endpoint")),
		estuary.WithPSAEndpoint(a.GetConfig("psaendpoint")),
		estuary.WithUploadEndpoint(a.GetConfig("uploadendpoint")),
	)

	return cl, err
}

func (unp *unprepared) Configs() map[string]string {
	return map[string]string{
		"endpoint":       "URL of API endpoint (optional)",
		"psaendpoint":    "URL of PSA endpoint (optional)",
		"uploadendpoint": "URL of Upload endpoint (optional)",
	}
}
