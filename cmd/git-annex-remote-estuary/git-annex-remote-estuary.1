.Dd July 2, 2022
.Dt GIT-ANNEX-REMOTE-ESTUARY 1
.Os
.Sh NAME
.Nm git-annex-remote-estuary
.Nd Git Annex special remote for Estuary
.Sh SYNOPSIS
.Nm git-annex-remote-estuary
.Op Fl h
.Sh DESCRIPTION
.Nm
is an
.Em external special remote
for
.Xr git-annex 1
that enables files to be stored on the IPFS network via an Estuary-compatible backend such as
.Lk https://estuary.tech/ .
.Pp
You won't typically interact with this program directly;
.Xr git-annex 1
invokes it as needed when a remote has been created with the
.Ar externaltype=estuary
option.
.Pp
The sole argument is:
.Bl -tag -width Ds
.It Fl h
Print brief help message.
.El
.Pp
When this remote is initialised, with
.Xr git-annex-initremote 1 ,
the environment variable
.Ev AUTH_TOK
should be set to an API key associated with your Estuary account. (Your
.Lk https://estuary.tech/
API key can be created/viewed at
.Lk https://estuary.tech/api-admin
).
This credential is cached by Git Annex and needn't be provided again for the same repository.
To change the API key used, set
.Ev AUTH_TOK
to the new key and invoke
.Xr git-annex-enableremote 1 ,
as shown in the
.Sx EXAMPLES
section.
.Pp
Alternatively, this remote can be used in readonly mode.
If an Annex repository uses this remote to store content on Estuary, then its clones can download this content without needing an Estuary API key or even this program.
Just use the
.Ar readonly=true
option with
.Xr git-annex-enableremote 1
in the clone.
.Pp
You can optionally provide additional arguments to
.Xr git-annex-initremote 1 /
.Xr git-annex-enableremote 1
as follows:
.Pp
.Bl -bullet -compact -width 1
.It
.Cm endpoint
- main URL for the Estuary API endpoint.
Default:
.Lk https://api.estuary.tech/
.It
.Cm psaendpoint
- URL of the IPFS Pinning Service API.
Default:
.Lk https://api.estuary.tech/pinning
.It
.Cm uploadendpoint
- URL of the Estuary upload endpoint.
Default:
.Lk https://upload.estuary.tech/
.El
.Pp
If Git Annex requests to store a key on this remote, and a IPFS CID is known for that key, we attempt to pin this CID with Estuary, rather than uploading the key and potentially storing it under a different CID.
This is particularly useful if the key is already present on another IPFS special remote---reusing the same CID is more efficient and robust.
This approach relies on a URL/URI being associated with a key which contains its CID.
This URL/URI may be set by another remote or manually with
.Xr git-annex-registerurl 1 .
It is the user's responsibility to ensure any CID associated with a key is accurate.
.Pp
When retrieving a key from this remote we attempt to use a local IPFS daemon.
If we can't reach one we use a public gateway, such as
.Lk https://dweb.link/ .
To avoid having to trust this gateway to provide the correct content, we use another gateway to verify what we receive.
This approach isn't foolproof, so running a local IPFS daemon is recommended.
.Pp
Git Annex keys dropped from this remote may remain accessible via other IPFS nodes, due to the nature of IPFS. Keep this principle in mind when deciding whether this remote is appropriate for your data.
.Sh ENVIRONMENT
The following environment variables are used by
.Nm
when set:
.Bl -tag -width Ds
.It Ev AUTH_TOK
The JWT corresponding to the Estuary account in use.
Only used by the
.Xr git-annex-initremote 1
and
.Xr git-annex-enableremote 1
commands.
.It Ev IPFS_PATH
The location of the IPFS repo.
On Linux this defaults to:
.Pa $HOME/.ipfs .
If set, the file
.Pa $IPFS_PATH/api
is assumed to contain the
.Em multiaddress
of the local IPFS node, e.g.
.Em /ip4/127.0.0.1/tcp/5001 ,
from which IPFS data should be retrieved.
.El
.Sh EXAMPLES
Create a remote named
.Em est
for the Estuary account with API key
.Em api-key :
.Bd -literal
$ AUTH_TOK=api-key git annex initremote est \\
                   encryption=none type=external externaltype=estuary
.Ed
.Pp
Modify the existing remote
.Em est
to use the API key
.Em key2
and use the upload endpoint
.Em https://example.com/ :
.Bd -literal
$ AUTH_TOK=key2 git annex enableremote est \\
                uploadendpoint=https://example.com/ encryption=none \\
		type=external externaltype=estuary
.Ed
.Pp
Configure the existing Estuary remote named
.Em est
in this repository to download via a public IPFS gateway:
.Bd -literal
$ git annex enableremote est readonly=true
.Ed
.Sh SEE ALSO
.Xr git-annex 1 ,
.Xr git-annex-enableremote 1 ,
.Xr git-annex-initremote 1 ,
.Xr git-annex-registerurl 1 ,
.Xr ipfs 1
.Ss WWW
.Bl -bullet -compact -width 1
.It
.Lk https://git-annex.branchable.com/special_remotes/ "Special Remotes"
.It
.Lk https://estuary.tech/ "Estuary.tech"
.It
.Lk https://ipfs.github.io/pinning-services-api-spec/ "IPFS Pinning Service API"
.It
.Lk https://multiformats.io/multiaddr/ "multiaddresses"
.El
.Sh BUGS
To report a bug in this software or view the current known bugs see the
.Lk https://gitlab.com/gitannex/ai/-/issues "bug tracker" .
.Sh SECURITY
The default gateways and endpoints are communicated with via HTTPS; if you override these defaults to use HTTP URLS, transport security will be compromised.
.Pp
The JWT provided in the
.Ev AUTH_TOK
environment variable is visible to the root user when
.Xr git-annex-initremote 1
or
.Xr git-annex-enableremote 1
is invoked.
This value is then stored in plaintext in the Git Annex repository, so is visible to root and the user who owns the repository.
