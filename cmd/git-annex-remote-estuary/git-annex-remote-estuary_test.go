package main

import (
	"net/http"
	"testing"

	"ai.camelcase.link/estuary"
	"ai.camelcase.link/internal/mockannex"
)

func TestWithTokenOK(t *testing.T) {
	ma := new(mockannex.Annex)
	ma.GetCredsFn = func(name string) (string, string) {
		if name != "jwt" {
			t.Errorf("expected name=jwt; got %s", name)
		}
		return "", "pass"
	}
	tok := withToken(ma)
	if tok != "pass" {
		t.Errorf("expected tok = pass; tok = %s", tok)
	}
}

func TestWithTokenUser(t *testing.T) {
	ma := new(mockannex.Annex)
	ma.GetCredsFn = func(name string) (string, string) {
		if name != "jwt" {
			t.Errorf("expected name=jwt; got %s", name)
		}
		return "user", "pass"
	}
	var errorFCalled int
	ma.ErrorFFn = func(fmt string, args ...interface{}) {
		errorFCalled++
	}

	tok := withToken(ma)
	if tok != "pass" {
		t.Errorf("expected tok = pass; tok = %s", tok)
	}
	if errorFCalled != 1 {
		t.Errorf("expected error message; a.Errorf wasn't called")
	}
}

func TestPrepareOK(t *testing.T) {
	ma := new(mockannex.Annex)
	ma.GetCredsFn = func(name string) (string, string) {
		if name == "jwt" {
			return "", "j"
		}
		return "", ""
	}
	ma.GetConfigFn = func(name string) string {
		if name == "endpoint" {
			return "https://localhost/"
		}
		if name == "psaendpoint" {
			return "https://psa.example.org/"
		}
		if name == "uploadendpoint" {
			return "https://upload.example.com/"
		}
		return ""
	}
	ua := http.DefaultClient
	ir, err := (&unprepared{}).Prepare(ma, ua)
	if err != nil {
		t.Errorf("expected no error; got %v", err)
	}
	eClient, ok := ir.(*estuary.Client)
	if !ok {
		t.Fatal("expected IPFSRemote to be a *estuary.Client; it wasn't")
	}
	other, err := estuary.New(
		estuary.WithToken("j"),
		estuary.WithHTTPClient(ua),
		estuary.WithPSAEndpoint("https://psa.example.org/"),
		estuary.WithEndpoint("https://localhost/"),
		estuary.WithUploadEndpoint("https://upload.example.com/"),
	)
	if err != nil {
		t.Error(err)
	}

	if !eClient.Equal(other) {
		t.Errorf("expected %+#v to Equal %+#v", eClient, other)
	}
}

func TestPrepareNotOK(t *testing.T) {
	ma := new(mockannex.Annex)
	ma.GetCredsFn = func(name string) (string, string) {
		// Empty creds are a fatal error for New
		return "", ""
	}
	ma.GetConfigFn = func(name string) string {
		if name == "endpoint" {
			return "https://localhost/"
		}
		if name == "psaendpoint" {
			return "https://example.com/psa"
		}
		if name == "uploadendpoint" {
			return "https://upload.example.com/"
		}
		return ""
	}
	ua := http.DefaultClient
	_, err := (&unprepared{}).Prepare(ma, ua)
	if err == nil {
		t.Errorf("expected error; got nil")
	}
}
