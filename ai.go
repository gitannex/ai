package ai

import (
	"context"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strings"
	"sync"
	"time"

	"ai.camelcase.link/internal/progress"
	"ai.camelcase.link/internal/retrieve"

	"github.com/dzhu/go-git-annex-external/remote"
	"github.com/ipfs/go-cid"
)

type IPFSRemote interface {
	Upload(context.Context, fs.File, *progress.Tracker) (cid.Cid, error)
	IsPresent(context.Context, cid.Cid) (bool, error)
	Unpin(context.Context, cid.Cid) error
	Pin(context.Context, cid.Cid, string) error
}

type Preparer interface {
	Prepare(remote.Annex, *http.Client) (IPFSRemote, error)
}

var DefaultVerifier = func(ctx context.Context, bg retrieve.BlockGetter, c cid.Cid, rsa readerSeekerAt) (int64, error) {
	v := NewCIDVerifier(bg)
	return v.ExpectedSize(), v.Verify(ctx, c, rsa)
}

var DefaultPinTimeout = time.Minute * 5

type AI struct {
	I           IPFSRemote
	R           *retrieve.R
	UA          *http.Client
	LinkGateway GatewayURL
	Prep        Preparer
	PinTimeout  time.Duration
	prepOnce    sync.Once
	verifier    func(context.Context, retrieve.BlockGetter, cid.Cid, readerSeekerAt) (int64, error)
}

func (ai *AI) Extensions(a remote.Annex, want []string) []string {
	for _, wantExt := range want {
		if wantExt == remote.ExtAsync {
			return []string{remote.ExtAsync}
		}
	}
	return []string{}
}

var DefaultLinkGateway = DwebLink

const authTokName = "jwt"

var ErrCantPin = errors.New("client doesn't have access to the PSA endpoint")

func (ai *AI) Init(a remote.Annex) error {
	tok := os.Getenv("AUTH_TOK")
	a.SetCreds(authTokName, "", tok)
	return nil
}

var ErrUnprepared = errors.New("method called on unprepared remote")

// checkPrepared returns ErrUnprepared unless Prepare() was called and returned without error
func (ai *AI) checkPrepared() error {
	if ai.I != nil {
		return nil
	}
	return ErrUnprepared
}

// Prepare creates the IPFSRemote used in most of the other exported methods. If called more than once, it will merely return a nil error.
func (ai *AI) Prepare(a remote.Annex) error {
	var prepErr error
	ai.prepOnce.Do(func() {
		var ipfsRemote IPFSRemote
		ipfsRemote, prepErr = ai.Prep.Prepare(a, ai.UA)
		if prepErr != nil {
			return
		}
		ai.I = ipfsRemote
		ai.LinkGateway = DefaultLinkGateway
	})
	return prepErr
}

func (ai *AI) ListConfigs(a remote.Annex) (css []remote.ConfigSetting) {
	cfgs, ok := ai.Prep.(interface{ Configs() map[string]string })
	if !ok {
		return css
	}
	for name, des := range cfgs.Configs() {
		css = append(css, remote.ConfigSetting{Name: name, Description: des})
	}

	return
}

func (ai *AI) Store(a remote.Annex, key, file string) error {
	if err := ai.checkPrepared(); err != nil {
		return err
	}
	ctx := context.Background()
	cStr := a.GetState(key)
	// We believe this key is already stored, but Store has been
	// called again, so we re-upload the given file rather than
	// merely trying to pin an existing CID
	// TODO: Delete state?
	if cStr != "" {
		return ai.upload(ctx, a, key, file)
	}
	// If a CID is already known for this key, try pinning that
	for _, cStr := range eachCID(a.GetURLs(key, "")) {
		c, err := cid.Decode(cStr)
		if err != nil {
			a.Debugf("Store(%s): ignoring invalid CID %s", key, cStr)
			continue
		}
		err = ai.pin(ctx, a, c, key)
		if err == nil {
			return nil
		}
		if errors.Is(err, ErrCantPin) {
			a.Debugf("Store(%s): found existing CID, %s, but don't have access to the pinning API, so will have to upload", key, c)
			break
		}
	}
	// If there were no associated CIDs, or all attempts at
	// pinning failed, upload the file
	return ai.upload(ctx, a, key, file)
}

func (ai *AI) upload(ctx context.Context, a remote.Annex, key, file string) error {
	fh, err := os.Open(file)
	if err != nil {
		return fmt.Errorf("Store(%s,%s): can't open file: %w", key, file, err)
	}
	defer fh.Close()
	c, err := ai.uploadFile(ctx, a, key, fh)
	if err != nil {
		return err
	}
	ai.setPresent(a, key, c)
	return nil
}

func (ai *AI) uploadFile(ctx context.Context, a remote.Annex, key string, fh *os.File) (cid.Cid, error) {
	pro, err := ai.makeProgress(a.Progress, fh)
	if err != nil {
		return cid.Undef, err
	}
	c, err := ai.I.Upload(ctx, fh, pro)
	if err != nil {
		return cid.Undef, fmt.Errorf("uploadFile: failed for key %s: %w", key, err)
	}
	if err := fh.Close(); err != nil {
		return cid.Undef, fmt.Errorf("uploadFile: can't close filehandle for key %s: %w", key, err)
	}
	_ = pro.Close()
	return c, nil
}

func (ai *AI) makeProgress(proFn func(int), fh *os.File) (*progress.Tracker, error) {
	fi, err := fh.Stat()
	if err != nil {
		return nil, fmt.Errorf("makeProgress: can't stat filehandle: %w", err)
	}
	// TODO: Warn on overflow?
	return progress.NewTracker(func(b int64) { proFn(int(b)) }, fi.Size()), nil
}

func (ai *AI) pin(ctx context.Context, a remote.Annex, c cid.Cid, key string) error {
	dur := DefaultPinTimeout
	if int64(ai.PinTimeout) != 0 {
		dur = ai.PinTimeout
	}
	ctx, cancelFn := context.WithTimeout(ctx, dur)
	defer cancelFn()
	err := ai.I.Pin(ctx, c, key)
	if err != nil {
		a.Debugf("can't pin key %s with CID %s: %v", key, c, err)
		return fmt.Errorf("pin: Pin for key %s and CID %s failed: %w", key, c, err)
	}
	ai.setPresent(a, key, c)
	a.Infof("pinned key %s with CID %s", key, c)
	return nil
}

func (ai *AI) setPresent(a remote.Annex, key string, c cid.Cid) {
	a.SetState(key, c.String())
	a.SetURIPresent(key, "ipfs://"+c.String())
	a.SetURLPresent(key, ai.LinkGateway.Public(c))
}

// Retrieve attempts to downloads the content stored for _key_ into
// _file_. It can be called even if Prepare() hasn't been called.
func (ai *AI) Retrieve(a remote.Annex, key, file string) error {
	ctx := context.Background()
	cStr := a.GetState(key)
	if cStr == "" {
		return fmt.Errorf("Retrieve(%s): don't know any CIDs to retrieve", key)
	}
	c, err := cid.Decode(cStr)
	if err != nil {
		return fmt.Errorf("Retrieve(%s): CID associated with key is invalid: %w", key, err)
	}
	return ai.retrieveCID(ctx, a, c, key, file)
}

// Derived from https://git.kitenet.net/index.cgi/git-annex.git/tree/Backend/Hash.hs
// TODO: Generate programatically
// TODO: move to key.go
var cryptographicallyStrongKey = regexp.MustCompile(`^(SHA256|SHA512|SHA384|SHA224|SHA3_512|SHA3_384|SHA3_256|SHA3_224|SKEIN512|SKEIN256|BLAKE2B160|BLAKE2B224|BLAKE2B256|BLAKE2B384|BLAKE2B512|BLAKE2S160|BLAKE2S224|BLAKE2S256|BLAKE2BP512|BLAKE2SP224|BLAKE2SP256)E?-`)

func isCryptographicallyStrong(key string) bool {
	return cryptographicallyStrongKey.MatchString(key)
}

func (ai *AI) retrieveCID(ctx context.Context, a remote.Annex, c cid.Cid, key, file string) error {
	fh, err := os.OpenFile(file, os.O_RDWR|os.O_CREATE, os.ModePerm)
	if err != nil {
		return fmt.Errorf("retrieveCID(%s,%s,%s): can't open file: %w", c, key, file, err)
	}
	defer fh.Close()

	decodedKey, err := Decode(key)
	if err != nil {
		return fmt.Errorf("retrieveCID: can't decode key, %s: %c", key, err)
	}
	if decodedKey.Size <= 0 {
		return fmt.Errorf("retrieveCID: %s size is <= 0", key)
	}
	raw := ai.R.Raw(c)
	raw.ExpectedSize = decodedKey.Size
	if _, err := fh.Seek(0, io.SeekEnd); err != nil {
		return err
	}
	// FIXME: error on overflow
	pro := progress.NewTracker(func(p int64) { a.Progress(int(p)) }, raw.ExpectedSize)
	raw.Target = pro.NewWriter(fh)
	if !isCryptographicallyStrong(key) {
		verifier := DefaultVerifier
		if ai.verifier != nil {
			verifier = ai.verifier
		}
		raw.VerifyFn = func() (int64, error) { return verifier(ctx, ai.R, c, fh) }
	}

	err = raw.Download(ctx)
	if err != nil {
		return err
	}
	return pro.Close()
}

// TODO: Should we use SetURLPresent/SetURLMissing here?
func (ai *AI) Present(a remote.Annex, key string) (bool, error) {
	if err := ai.checkPrepared(); err != nil {
		return false, err
	}
	cStr := a.GetState(key)
	if cStr == "" {
		a.Debugf("Present(%s): not stored", key)
		return false, nil
	}
	c, err := cid.Decode(cStr)
	if err != nil {
		return false, fmt.Errorf("invalid CID: %w", err)
	}
	return ai.I.IsPresent(context.Background(), c)
}

func (ai *AI) Remove(a remote.Annex, key string) error {
	if err := ai.checkPrepared(); err != nil {
		return err
	}
	cStr := a.GetState(key)
	if cStr == "" {
		// Spec
		// (https://git-annex.branchable.com/design/external_special_remote_protocol/)
		// implies success should be returned if the key isn't present on the
		// remote
		// TODO: ia.setMissing?
		return nil
	}
	c, err := cid.Decode(cStr)
	if err != nil {
		return fmt.Errorf("Remove(%s): invalid CID (%s) stored: %w", key, cStr, err)
	}
	err = ai.I.Unpin(context.Background(), c)
	if err != nil {
		if errors.Is(err, ErrCantPin) {
			a.Infof("can't unpin CID %s because we don't have access to the pinning API", c)
		} else {
			return fmt.Errorf("Remove(%s): can't unpin CID %s: %w", key, c, err)
		}
	}
	ai.setMissing(a, key, c)
	return nil
}

func (ai *AI) setMissing(a remote.Annex, key string, c cid.Cid) {
	a.SetState(key, "")
	a.SetURIMissing(key, "ipfs://"+c.String())
	a.SetURLMissing(key, ai.LinkGateway.Public(c))
}

// TODO: this extracts based on string parsing. We should consider
// enhancing it by looking for the IPFS path header
// Look at https://github.com/ipfs-shipyard/is-ipfs
func extractCID(u string) (string, error) {
	if strings.HasPrefix(u, "ipfs:") && len(u) > 5 {
		if c, err := cid.Decode(u[5:]); err == nil {
			return c.String(), nil
		}
	}
	uri, err := url.Parse(u)
	if err != nil {
		return "", err
	}
	if uri.Scheme == "ipfs" {
		if c, err := cid.Decode(uri.Host); err == nil {
			return c.String(), nil
		}
	}
	if uri.Scheme != "ipfs" && uri.Scheme != "http" && uri.Scheme != "https" {
		return "", fmt.Errorf("bad scheme")
	}
	pathParts := strings.Split(uri.Path, "/")
	if len(pathParts) > 1 && pathParts[1] == "ipfs" {
		if c, err := cid.Decode(pathParts[2]); err == nil {
			return c.String(), nil
		}
	}
	hostParts := strings.Split(uri.Host, ".")
	if len(hostParts) > 3 && hostParts[1] == "ipfs" {
		if c, err := cid.Decode(hostParts[0]); err == nil {
			return c.String(), nil
		}
	}
	return "", errors.New("invalid URL")
}

func eachCID(urls []string) []string {
	seenCIDs := map[string]struct{}{}
	cids := []string{}
	for _, u := range urls {
		c, err := extractCID(u)
		if err != nil {
			continue
		}
		if _, seen := seenCIDs[c]; seen {
			continue
		}
		seenCIDs[c] = struct{}{}
		cids = append(cids, c)
	}
	return cids
}
