package ai

import (
	"bytes"
	"context"
	"crypto/sha512"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"math/rand"
	"net/http"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"testing"
	"time"

	"github.com/dnaeon/go-vcr/v2/recorder"
	blocks "github.com/ipfs/go-block-format"
	"github.com/ipfs/go-cid"
	"github.com/ipfs/go-merkledag"
	"github.com/ipld/go-car"
	"github.com/ipld/go-car/v2/blockstore"
)

type testCase struct {
	fn string // base name of file in testdata/car/
	ok bool   // the validation should succeed
}

// TODO: test CIDs that can't be CARRed. Ensure we test a root CID as a raw block.
// TODO: test how many times blocks are requested per test
// TODO: Be more precise about expected errors
func TestCIDVerifier(t *testing.T) {
	for _, tc := range []testCase{
		// The CID is v1. Raw-leaves were disabled, otherwise this couldn't be exported as a CAR via `ipfs dag export`.
		{fn: "hello", ok: true},
		// world.car is a CAR for a file with the contents "Worl"; world has the contents "World". The CID is v0
		{fn: "world", ok: false},
		// zoology.car is a CAR for a file with the contents "zoology"; zoology has the contents "zoolgy". The CID is v1.
		{fn: "zoology", ok: false},
		// This is a text from Project Gutenberg whose CAR was exported without modification
		{fn: "pg53474.txt", ok: true},
		// This is the CAR from above, but a single character was deleted from the text file
		{fn: "pg53474-corrupt.txt", ok: false},
		// This is the CAR from above with a single character in the text file changed in case
		{fn: "pg53474-corrupt2.txt", ok: false},
		// Project Gutenberg text with the CAR generated using the buzhash chunker
		{fn: "pg53474.txt.buzhash", ok: true},
		// Project Gutenberg text, but with a single character
		// in the CAR changed. We only use the CAR for the
		// metadata, and that hasn't changed, so this should
		// verify.
		{fn: "pg53474.txt.buzhash-corrupt", ok: true},
		// Project Gutenberg text with CAR metadata corrupted
		{fn: "pg53474.txt.corrupt", ok: false},
		// Project Gutenberg text with CAR created with chunker=size-5
		{fn: "pg53474.txt-size-5", ok: true},
		// Project Gutenberg text with CAR created with trickle=true and chunker=rabin-17-20-100
		{fn: "pg53474.txt-trickle-rabin-17-20-100", ok: true},
		// Project Guttenberg text with CAR created with inline=true and chunker=size-30; first character changed in text
		{fn: "pg53474.txt.size-30-inline", ok: false},
		// Project Gutenberg text with CAR created with CIDv0, raw-leaves=false, and chunker=size-25
		{fn: "pg53474.txt.no-raw-leaves", ok: true},
		// Small text file with raw-leaves=false and chunker=size-5
		{fn: "Az", ok: true},
		// Gutenberg text with CIDv1 and raw-leaves=false
		{fn: "pg53474.txt.cidv1.no-raw-leaves", ok: true},
	} {
		tc := tc
		if !t.Run(tc.fn, func(t *testing.T) { t.Parallel(); verifyOne(t, tc) }) {
			t.Errorf("subtest %s failed", tc.fn)
		}
	}
}

type countingBlockGetter struct {
	count, withLinks int64
	bg               blockGetter
}

func (cbg *countingBlockGetter) GetBlock(ctx context.Context, c cid.Cid) (blocks.Block, error) {
	cbg.count++
	bl, err := cbg.bg.GetBlock(ctx, c)
	if err != nil {
		return nil, err
	}
	if bl.Cid().Type() == cid.DagProtobuf {
		if node, err := merkledag.DecodeProtobufBlock(bl); err == nil && len(node.Links()) > 0 {
			cbg.withLinks++
		}
	}
	return bl, err
}

type accountingReaderAt struct {
	readerSeekerAt
	bytesRead [][]int64
}

func newAccountingReaderAt(rsa readerSeekerAt) *accountingReaderAt {
	return &accountingReaderAt{readerSeekerAt: rsa, bytesRead: [][]int64{}}
}

func (ara *accountingReaderAt) ReadAt(p []byte, off int64) (int, error) {
	n, err := ara.readerSeekerAt.ReadAt(p, off)
	if err == nil {
		ara.bytesRead = append(ara.bytesRead, []int64{off, off + int64(len(p))})
	}
	return n, err
}

func (ara *accountingReaderAt) Size() int64 {
	n, err := ara.Seek(0, io.SeekEnd)
	if err != nil {
		panic(err)
	}
	return n
}

func (ara *accountingReaderAt) Unread() (unread int64) {
	sort.Slice(ara.bytesRead, func(i, j int) bool {
		if ara.bytesRead[i][0] == ara.bytesRead[j][0] {
			return ara.bytesRead[i][1] < ara.bytesRead[j][1]
		}
		return ara.bytesRead[i][0] < ara.bytesRead[j][0]
	})
	unread = ara.bytesRead[0][0]
	unread += ara.Size() - ara.bytesRead[len(ara.bytesRead)-1][1]

	for i, pair := range ara.bytesRead {
		if i == 0 {
			continue
		}
		diff := pair[0] - ara.bytesRead[i-1][1]
		if diff > 1 {
			unread += diff
		}
	}
	return
}

type carBlockStore struct {
	car.ReadStore
}

func (cbs carBlockStore) GetBlock(ctx context.Context, c cid.Cid) (blocks.Block, error) {
	return cbs.Get(ctx, c)
}

func verifyOne(t *testing.T, tc testCase) {
	t.Helper()
	fn := "testdata/car/" + tc.fn
	carFn := fn + ".car"
	blockStore, err := blockstore.OpenReadOnly(carFn)
	if err != nil {
		t.Fatalf("verifyOne: can't open blockstore: %v", err)
	}
	defer blockStore.Close()
	rat, err := os.Open(fn)
	if err != nil {
		t.Fatalf("verifyOne: %v", err)
	}
	defer rat.Close()
	roots, err := blockStore.Roots()
	if err != nil {
		t.Fatalf("verifyOne: can't read roots of CAR: %v", err)
	}
	cbg := &countingBlockGetter{bg: carBlockStore{blockStore}}
	ara := newAccountingReaderAt(rat)
	cv := NewCIDVerifier(cbg)
	err = cv.Verify(context.Background(), roots[0], ara)
	if (cbg.count - cbg.withLinks) > 1 {
		t.Errorf("%d wasted block retrievals", cbg.count-cbg.withLinks)
	}
	if tc.ok {
		if err != nil {
			t.Errorf("expected to validate; got error: %v", err)
		}
		if err == nil {
			unread := ara.Unread()
			if unread > 0 {
				t.Errorf("bytes unread %d", unread)
			}
		}
	}
	if !tc.ok && err == nil {
		t.Fatal("expected to fail; err == nil")
	}
}

func TestVerifyIdentity(t *testing.T) {
	for cStr, s := range map[string]string{
		// base32, dag-pg, identity hash:
		"bafyaaatine": "hi",
		// base32, raw codec, identity hash:
		"bafyaadcimvwgy3zmeb3w64tmmq": "Hello, world",
	} {
		c, err := cid.Decode(cStr)
		if err != nil {
			t.Errorf("%s: %v", cStr, err)
			continue
		}
		cv := NewCIDVerifier(nil)
		if err := cv.Verify(context.Background(), c, strings.NewReader(s)); err != nil {
			t.Errorf("expected no error for %s;got %v", s, err)
		}
	}

	for s, k := range map[string]string{
		"hid":           "bafyaacykbeeaeeqdnbuqugad",
		"_Hello, world": "bafyaafikcmeaeeqnjbswy3dpfqqho33snrsaugan",
		"d":             "bafyaafikcmeaeeqnjbswy3dpfqqho33snrsaugan",
	} {
		c, err := cid.Decode(k)
		if err != nil {
			t.Error(err)
			continue
		}
		cv := NewCIDVerifier(nil)
		if err := cv.Verify(context.Background(), c, strings.NewReader(s)); err == nil {
			t.Errorf("expected error for %s; got nil", s)
		}
	}
}

func TestVerifySingleByte(t *testing.T) {
	fn := "testdata/car/pg53474.txt"
	carFn := fn + ".car"
	inBs, err := os.ReadFile(fn)
	if err != nil {
		t.Fatal(err)
	}
	rnd := rand.New(rand.NewSource(time.Now().UnixNano()))
	rndIdx := rnd.Intn(len(inBs))

	for {
		tmp := make([]byte, 1)
		// The below is documented to return len(p), nil, so we ignore its return values
		rnd.Read(tmp)
		if tmp[0] == inBs[rndIdx] {
			continue
		}
		inBs[rndIdx] = tmp[0]
		break
	}

	blockStore, err := blockstore.OpenReadOnly(carFn)
	if err != nil {
		t.Fatalf("verifyOne: can't open blockstore: %v", err)
	}
	defer blockStore.Close()
	roots, err := blockStore.Roots()
	if err != nil {
		t.Fatalf("verifyOne: can't read roots of CAR: %v", err)
	}
	cbg := &countingBlockGetter{bg: carBlockStore{blockStore}}
	cv := NewCIDVerifier(cbg)
	err = cv.Verify(context.Background(), roots[0], bytes.NewReader(inBs))
	if (cbg.count - cbg.withLinks) > 1 {
		t.Errorf("%d wasted block retrievals", cbg.count-cbg.withLinks)
	}
	if err == nil {
		t.Fatalf("expected validation error (corrupted byte %d to %v)", rndIdx, inBs[rndIdx])
	}
}

func FuzzVerifyMencken(f *testing.F) {
	f.Fuzz(func(t *testing.T, in []byte) {
		blockStore, err := blockstore.OpenReadOnly("testdata/car/pg53474.txt.car")
		if err != nil {
			t.Fatal(err)
		}
		defer blockStore.Close()
		br := bytes.NewReader(in)
		roots, err := blockStore.Roots()
		if err != nil {
			t.Fatalf("VerifyMencken: can't read roots of CAR: %v", err)
		}
		cv := NewCIDVerifier(carBlockStore{blockStore})
		if err := cv.Verify(context.Background(), roots[0], br); err == nil {
			t.Errorf("expected verification to fail for %s (%d bytes); got err==nil", roots[0], len(in))
		}
	})
}

func cacheTestData(t *testing.T, u, fn, sha512Hex string) error {
	t.Helper()
	_, err := os.Stat(fn)
	if err == nil {
		return nil
	}
	if !errors.Is(err, fs.ErrNotExist) {
		return err
	}
	t.Logf("downloading %s from %s...", fn, u)
	res, err := http.Get(u)
	if err != nil {
		return err
	}
	defer res.Body.Close()
	tmpFh, err := os.CreateTemp(filepath.Dir(fn), filepath.Base(fn))
	if err != nil {
		return err
	}
	defer os.Remove(tmpFh.Name())
	h := sha512.New()
	tmpHash := io.MultiWriter(h, tmpFh)
	if _, err := io.Copy(tmpHash, res.Body); err != nil {
		return err
	}
	gotHash := fmt.Sprintf("%x", h.Sum(nil))
	if gotHash != sha512Hex {
		return fmt.Errorf("%s: expected hash %s; got %s", fn, sha512Hex, gotHash)
	}
	return os.Rename(tmpFh.Name(), fn)
}

func TestVerifyWithGateway(t *testing.T) {
	t.Parallel()
	rec, err := recorder.NewAsMode("fixtures/cid-verify-with-gateway", recorder.ModeReplayingOrRecording, http.DefaultTransport)
	if err != nil {
		t.Fatal(err)
	}
	rec.SkipRequestLatency = true
	defer rec.Stop()
	bg := countingBlockGetter{
		bg: &Gateway{URL: IPFSIo, UA: &http.Client{Transport: rec}},
	}
	testdataURL := "https://www.archive.org/download/americanlanguage_1801_librivox/americanlanguage_1801_librivox_64kb_mp3.zip"
	// Pin of this URL's payload  (raw-leaves and blake2b-256)
	menckenCID, _ := cid.Decode("bafykbzacebddfhf2xnnnf5nczmwxe22afagzu57kesfyqss43y5bspahkxpey")
	fn := "testdata/car/americanlanguage_1801_librivox_64kb_mp3.zip"
	if err := cacheTestData(t, testdataURL, fn, "e0de687d3742a23b4e41db000eb0027fd9bca632e223f22b8e30afad973b9eafd8f0e5ee80390dae7e13961d3a6e98b612a19da6a4829cd8c57e759fee5c2c53"); err != nil {
		t.Fatal(err)
	}
	fh, err := os.Open(fn)
	if err != nil {
		t.Fatal(err)
	}
	defer fh.Close()
	ara := newAccountingReaderAt(fh)
	cv := NewCIDVerifier(&bg)
	ctx, cancelFn := context.WithTimeout(context.Background(), time.Minute*time.Duration(2))
	defer cancelFn()
	if err := cv.Verify(ctx, menckenCID, ara); err != nil {
		t.Errorf("expected verification to succeed; got err==%v", err)
	}
	if unread := ara.Unread(); unread > 0 {
		t.Errorf("bytes unread %d", unread)
	}
	if (bg.count - bg.withLinks) > 1 {
		t.Errorf("%d wasted block retrievals", bg.count-bg.withLinks)
	}
}
