package ai

import (
	"testing"

	"github.com/ipfs/go-cid"
)

func TestGatewayURL(t *testing.T) {
	t.Parallel()
	type testCase struct {
		gu GatewayURL
		c  string
		u  string
		b  string
	}
	for _, tc := range []testCase{
		{gu: DwebLink, c: "bafkreigoamtzilpekuolvght3fkaceorudpdrfsqkhnii7dv3coqxi6en4", u: "https://bafkreigoamtzilpekuolvght3fkaceorudpdrfsqkhnii7dv3coqxi6en4.ipfs.dweb.link/", b: "https://dweb.link/api/v0/block/get?arg=bafkreigoamtzilpekuolvght3fkaceorudpdrfsqkhnii7dv3coqxi6en4"},
		{gu: DwebLink, c: "QmWATWQ7fVPP2EFGu71UkfnqhYXDYH566qy47CnJDgvs8u", u: "https://dweb.link/ipfs/QmWATWQ7fVPP2EFGu71UkfnqhYXDYH566qy47CnJDgvs8u", b: "https://dweb.link/api/v0/block/get?arg=QmWATWQ7fVPP2EFGu71UkfnqhYXDYH566qy47CnJDgvs8u"},
		{gu: IPFSIo, c: "bafkreigoamtzilpekuolvght3fkaceorudpdrfsqkhnii7dv3coqxi6en4", u: "https://ipfs.io/ipfs/bafkreigoamtzilpekuolvght3fkaceorudpdrfsqkhnii7dv3coqxi6en4", b: "https://ipfs.io/api/v0/block/get?arg=bafkreigoamtzilpekuolvght3fkaceorudpdrfsqkhnii7dv3coqxi6en4"},
		{gu: IPFSIo, c: "QmWATWQ7fVPP2EFGu71UkfnqhYXDYH566qy47CnJDgvs8u", u: "https://ipfs.io/ipfs/QmWATWQ7fVPP2EFGu71UkfnqhYXDYH566qy47CnJDgvs8u", b: "https://ipfs.io/api/v0/block/get?arg=QmWATWQ7fVPP2EFGu71UkfnqhYXDYH566qy47CnJDgvs8u"},
		{gu: CloudflareIPFS, c: "bafkreigoamtzilpekuolvght3fkaceorudpdrfsqkhnii7dv3coqxi6en4", u: "https://cloudflare-ipfs.com/ipfs/bafkreigoamtzilpekuolvght3fkaceorudpdrfsqkhnii7dv3coqxi6en4"},
		{gu: CloudflareIPFS, c: "QmWATWQ7fVPP2EFGu71UkfnqhYXDYH566qy47CnJDgvs8u", u: "https://cloudflare-ipfs.com/ipfs/QmWATWQ7fVPP2EFGu71UkfnqhYXDYH566qy47CnJDgvs8u"},
		{gu: InfuraIPFSIo, c: "bafkreigoamtzilpekuolvght3fkaceorudpdrfsqkhnii7dv3coqxi6en4", u: "https://bafkreigoamtzilpekuolvght3fkaceorudpdrfsqkhnii7dv3coqxi6en4.ipfs.infura-ipfs.io/"},
		{gu: InfuraIPFSIo, c: "QmWATWQ7fVPP2EFGu71UkfnqhYXDYH566qy47CnJDgvs8u", u: "https://infura-ipfs.io/ipfs/QmWATWQ7fVPP2EFGu71UkfnqhYXDYH566qy47CnJDgvs8u"},
		{gu: PinataCloud, c: "bafkreigoamtzilpekuolvght3fkaceorudpdrfsqkhnii7dv3coqxi6en4", u: "https://gateway.pinata.cloud/ipfs/bafkreigoamtzilpekuolvght3fkaceorudpdrfsqkhnii7dv3coqxi6en4", b: "https://gateway.pinata.cloud/api/v0/block/get?arg=bafkreigoamtzilpekuolvght3fkaceorudpdrfsqkhnii7dv3coqxi6en4"},
		{gu: PinataCloud, c: "QmWATWQ7fVPP2EFGu71UkfnqhYXDYH566qy47CnJDgvs8u", u: "https://gateway.pinata.cloud/ipfs/QmWATWQ7fVPP2EFGu71UkfnqhYXDYH566qy47CnJDgvs8u", b: "https://gateway.pinata.cloud/api/v0/block/get?arg=QmWATWQ7fVPP2EFGu71UkfnqhYXDYH566qy47CnJDgvs8u"},
	} {
		cidObj, _ := cid.Decode(tc.c)
		gotU := tc.gu.Public(cidObj)
		if gotU != tc.u {
			t.Errorf("expected %s for %T.Public; got %s", tc.u, tc.gu, gotU)
		}
		gotB := tc.gu.Block(cidObj)
		if gotB != tc.b {
			t.Errorf("expected %s for %T.Block; got %s", tc.u, tc.gu, gotB)
		}

	}
}
