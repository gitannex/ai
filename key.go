package ai

import (
	"fmt"
	"regexp"
	"strconv"
)

type Key struct {
	Backend   string
	HasExt    bool
	Size      int64
	Mtime     int64
	ChunkSize int64
	ChunkNum  int64
	Name      string
}

var KeyPat = regexp.MustCompile(`^(?P<backend>[A-Z\d]+[^E])(?P<ext>E)?(-s(?P<size>\d+))?(-m(?P<mtime>\d+))?(-S(?P<chunkSize>\d+)-C(?P<chunkNum>\d+))?--(?P<name>[^/\n]+)$`)

func Decode(key string) (Key, error) {
	k := Key{}
	matches := KeyPat.FindStringSubmatch(key)
	if matches == nil {
		return k, fmt.Errorf("invalid key")
	}
	names := KeyPat.SubexpNames()
	for i, m := range matches {
		if i == 0 {
			continue
		}
		switch names[i] {
		case "backend":
			k.Backend = m
		case "ext":
			k.HasExt = (m == "E")
		case "size", "mtime", "chunkSize", "chunkNum":
			if m == "" {
				continue
			}
			n, err := strconv.ParseInt(m, 10, 64)
			if err != nil {
				return k, err
			}
			switch names[i] {
			case "size":
				k.Size = n
			case "mtime":
				k.Mtime = n
			case "chunkSize":
				k.ChunkSize = n
			case "chunkNum":
				k.ChunkNum = n
			}
		case "name":
			k.Name = m
		}
	}
	return k, nil
}
