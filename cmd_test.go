//go:build e2e

package ai_test

import (
	"bytes"
	"errors"
	"io/fs"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"testing"
)

func TestRemote(t *testing.T) {
	for _, remote := range []string{"w3s", "estuary", "pinata"} {
		remote := remote
		t.Run(remote, func(t *testing.T) {
			t.Parallel()
			testRemote(t, remote)
		})
	}
}

func testRemote(t *testing.T, name string) {
	t.Helper()
	var bb bytes.Buffer
	tmpDir := t.TempDir()
	defer os.RemoveAll(tmpDir)
	gitCmd := exec.Command("git", "init")
	gitCmd.Dir = tmpDir
	if err := gitCmd.Run(); err != nil {
		t.Fatal(err)
	}
	gitAnnexInit := exec.Command("git", "annex", "init")
	gitAnnexInit.Dir = tmpDir
	if err := gitAnnexInit.Run(); err != nil {
		t.Fatal(err)
	}
	goBuild := exec.Command("go", "build", "-race", "-o", tmpDir+"/git-annex-remote-"+name, "cmd/git-annex-remote-"+name+"/git-annex-remote-"+name+".go")
	goBuild.Stderr = &bb
	if err := goBuild.Run(); err != nil {
		t.Fatal(err)
	}
	initRemoteCmd := exec.Command("git", "annex", "initremote", "rem", "encryption=none", "type=external", "externaltype="+name)
	if name == "w3s" {
		initRemoteCmd.Args = append(initRemoteCmd.Args, "canpin=true")
	}
	initRemoteCmd.Dir = tmpDir
	initRemoteCmd.Env = os.Environ()
	for i, kv := range initRemoteCmd.Env {
		if strings.HasPrefix(kv, "PATH=") {
			initRemoteCmd.Env[i] = "PATH=" + tmpDir + ":" + kv[5:]
			break
		}
	}
	authTokEnvVar := strings.ToUpper(name + "_TOK")
	authTok := os.Getenv(authTokEnvVar)
	if authTok == "" {
		t.Fatalf("%s not set", authTokEnvVar)
	}
	initRemoteCmd.Env = append(initRemoteCmd.Env, "AUTH_TOK="+authTok)
	initRemoteCmd.Stderr = &bb
	if err := initRemoteCmd.Run(); err != nil {
		t.Fatalf("%v: %s", err, bb.String())
	}
	testRemoteCmd := exec.Command("git", "annex", "testremote", "--debug", "--fast", "rem")
	testRemoteCmd.Dir = tmpDir
	testRemoteCmd.Env = initRemoteCmd.Env
	testRemoteCmd.Stderr = &bb
	testRemoteCmd.Stdout = &bb
	if err := testRemoteCmd.Run(); err != nil {
		t.Fatalf("%v: %s", err, bb.String())
	}
}

func TestReadOnly(t *testing.T) {
	for _, remote := range []string{"estuary", "w3s", "pinata"} {
		remote := remote
		t.Run(remote, func(t *testing.T) {
			t.Parallel()
			testReadOnly(t, remote)
		})
	}
}

func prependPath(env *[]string, dir string) {
	for i, v := range *env {
		name, value, found := strings.Cut(v, "=")
		if !found || name != "PATH" {
			continue
		}

		(*env)[i] = "PATH=" + dir + ":" + value
	}
}

func testReadOnly(t *testing.T, name string) {
	tmpDir := t.TempDir()
	remoteName := name[:3]
	goBuild := exec.Command("go", "build", "-race", "-o", tmpDir+"/git-annex-remote-"+name, "cmd/git-annex-remote-"+name+"/git-annex-remote-"+name+".go")
	if err := goBuild.Run(); err != nil {
		t.Fatal(err)
	}
	// This tarred repo contains two files, both copied to the
	// remote _remoteName_ (externaltype=estuary), and _1234_
	// subsequently dropped. Accordingly, in a readonly clone we
	// expect to be able to fetch the 2nd file, _56789_ from
	// _remoteName_, but not fetch _1234_.
	// This repo contains an API key in plain text, but it
	// was revoked immediately after tarring.
	tarCmd := exec.Command("tar", "-C", tmpDir, "--extract", "-f", "testdata/readonly-a-repo-"+remoteName+".tar")
	tarOut, err := tarCmd.CombinedOutput()
	if err != nil {
		t.Fatalf("tar failed: %#+v (%s)", err, tarOut)
	}

	gitCloneCmd := exec.Command("git", "clone", "a", "b")
	gitCloneCmd.Dir = tmpDir
	if err := gitCloneCmd.Run(); err != nil {
		t.Fatal(err)
	}
	bDir := filepath.Join(tmpDir, "b")
	aDir := filepath.Join(tmpDir, "a")
	t.Cleanup(func() {
		gaUninit := exec.Command("git", "annex", "uninit")
		gaUninit.Dir = bDir
		gaUninit.Run()
		gaUninit = exec.Command("git", "annex", "uninit")
		gaUninit.Dir = aDir
		gaUninit.Run()
		os.RemoveAll(tmpDir)
	})
	gaEnableRemote := exec.Command("git", "annex", "enableremote", remoteName, "readonly=true")
	gaEnableRemote.Dir = bDir
	gaEnableRemote.Env = os.Environ()
	prependPath(&gaEnableRemote.Env, tmpDir)
	if err := gaEnableRemote.Run(); err != nil {
		t.Fatal(err)
	}
	gaGet1234 := exec.Command("git", "annex", "get", "--from="+remoteName, "1234")
	gaGet1234.Dir = bDir
	gaGet1234.Env = os.Environ()
	prependPath(&gaGet1234.Env, tmpDir)

	if err := gaGet1234.Run(); err != nil {
		t.Errorf("expected get --from=%s 1234 to succeed, even though 1234 was dropped from %s; it failed: %v", remoteName, remoteName, err)
	}
	if _, err := os.ReadFile(filepath.Join(bDir, "1234")); !errors.Is(err, fs.ErrNotExist) {
		t.Errorf("expected 1234 to be a broken symlink; got error %v", err)
	}
	gaGet56789 := exec.Command("git", "annex", "get", "--from="+remoteName, "56789")
	gaGet56789.Dir = bDir
	gaGet56789.Env = os.Environ()
	prependPath(&gaGet56789.Env, tmpDir)
	if err := gaGet56789.Run(); err != nil {
		t.Errorf("expected get --from=%s 56789 to succeed; it failed: %v", remoteName, err)
	}
	f, err := os.ReadFile(filepath.Join(bDir, "56789"))
	if err != nil {
		t.Errorf("expected to read file 56789; got error %v", err)
	}
	if !bytes.Equal(f, []byte("56789\n")) {
		t.Errorf("expected 56789 to have the right contents; got %v", f)
	}
	_ = os.Remove(filepath.Join(bDir, "1234"))
	if err := os.WriteFile(filepath.Join(bDir, "1234"), []byte("1234\n"), os.ModePerm); err != nil {
		t.Errorf("expected to recreate 1234 without error; got %v", err)
	}
	gaAdd := exec.Command("git", "annex", "add")
	gaAdd.Dir = bDir
	gaAdd.Env = os.Environ()
	prependPath(&gaAdd.Env, tmpDir)
	if err := gaAdd.Run(); err != nil {
		t.Errorf("expected to be able to add 1234 without error; got %v", err)
	}
	gaCopy1234 := exec.Command("git", "annex", "copy", "--to="+remoteName, "1234")
	gaCopy1234.Dir = bDir
	gaCopy1234.Env = os.Environ()
	prependPath(&gaCopy1234.Env, tmpDir)

	out, err := gaCopy1234.CombinedOutput()
	if err == nil {
		t.Errorf("expected copy --to=%s 1234 to fail because est is readonly; it succeeded", remoteName)
	}
	if !bytes.Contains(out, []byte("this remote is readonly")) {
		t.Errorf("expected copy --to=%s to fail because remote is readonly; got: %s", remoteName, out)
	}
}

func TestHelp(t *testing.T) {
	for _, remote := range []string{"estuary", "w3s", "pinata"} {
		remote := remote
		t.Run(remote, func(t *testing.T) {
			t.Parallel()
			testHelp(t, remote)
		})
	}
}

func testHelp(t *testing.T, name string) {
	t.Helper()
	tmpDir := t.TempDir()
	goBuild := exec.Command("go", "build", "-race", "-o", tmpDir+"/git-annex-remote-"+name, "cmd/git-annex-remote-"+name+"/git-annex-remote-"+name+".go")
	if err := goBuild.Run(); err != nil {
		t.Fatal(err)
	}
	garHelp := exec.Command(tmpDir+"/git-annex-remote-"+name, "-h")
	out, err := garHelp.CombinedOutput()
	if err != nil {
		t.Errorf("expected cmd not to error; got %v", err)
	}
	if !bytes.Contains(out, []byte("external remote")) {
		t.Errorf("expected -h to give a help message; got %s", out)
	}
}
