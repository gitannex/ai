package ai

import (
	"testing"

	"github.com/ipfs/go-cid"
)

func TestPublic(t *testing.T) {
	type testCase struct {
		gu GatewayURL
		c  string
		u  string
	}
	for _, tc := range []testCase{
		{gu: DwebLink, c: "bafkreigoamtzilpekuolvght3fkaceorudpdrfsqkhnii7dv3coqxi6en4", u: "https://bafkreigoamtzilpekuolvght3fkaceorudpdrfsqkhnii7dv3coqxi6en4.ipfs.dweb.link/"},
	} {
		cidObj, _ := cid.Decode(tc.c)
		gotURL := tc.gu.Public(cidObj)
		if gotURL != tc.u {
			t.Errorf("expected %s for %T.Public; got %s", tc.u, tc.gu, gotURL)
		}
	}
}
