package pinata

import (
	"bytes"
	"context"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
	"time"

	"ai.camelcase.link/internal/progress"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/ipfs/go-cid"
)

func setupClient(t *testing.T, opts ...NewOpt) *Client {
	t.Helper()
	rh := retryablehttp.NewClient()
	rh.RetryWaitMax = time.Millisecond
	cl, err := New(append(opts, WithHTTPClient(rh.StandardClient()), WithToken("sekrit"))...)
	if err != nil {
		t.Fatal(err)
	}
	zero := time.Duration(0)
	cl.rp.PollWait = &zero
	return cl
}

func TestUpload429(t *testing.T) {
	var reqNum int
	srcText, err := os.ReadFile("../testdata/upload.txt")
	if err != nil {
		t.Error(err)
	}
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			reqNum++
		}()
		if reqNum < 3 {
			w.WriteHeader(http.StatusTooManyRequests)
			return
		}
		body, err := io.ReadAll(r.Body)
		if err != nil {
			t.Error(err)
		}
		if !bytes.Contains(body, srcText) {
			t.Errorf("expected uploaded body to contain srcText; it is: %s", body)
		}
		w.Write([]byte(`{"IpfsHash": "bafkreigoamtzilpekuolvght3fkaceorudpdrfsqkhnii7dv3coqxi6en4", "PinSize": 10227, "Timestamp": "2022-05-12T13:05:00.816Z"}`))
	}))
	defer server.Close()
	p := setupClient(t, WithEndpoint(server.URL))
	expectedCID, _ := cid.Decode("bafkreigoamtzilpekuolvght3fkaceorudpdrfsqkhnii7dv3coqxi6en4")
	fh, err := os.Open("../testdata/upload.txt")
	if err != nil {
		t.Fatal(err)
	}
	defer fh.Close()

	c, err := p.Upload(context.Background(), fh, &progress.Tracker{})
	if err != nil {
		t.Errorf("Upload: %v", err)
	}
	if !c.Equals(expectedCID) {
		t.Errorf("expected upload to have CID: %s; got %s", expectedCID, c)
	}
}

func TestUploadOptions(t *testing.T) {
	var reqNum int
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		body, err := io.ReadAll(r.Body)
		if err != nil {
			t.Error(err)
		}
		if !bytes.Contains(body, []byte("form-data; name=\"pinataOptions\"\r\n\r\n{\"cidVersion\":1,\"wrapWithDirectory\":false}\r\n-")) {
			t.Errorf("expected uploaded body to contain pinataOptions; it is: %s", body)
		}
		reqNum = 1
	}))
	defer server.Close()
	p := setupClient(t, WithEndpoint(server.URL))
	fh, err := os.Open("../testdata/upload.txt")
	if err != nil {
		t.Fatal(err)
	}
	defer fh.Close()

	_, _ = p.Upload(context.Background(), fh, &progress.Tracker{})
	if reqNum != 1 {
		t.Errorf("expected 1 request to be made; it wasn't")
	}
}

func TestUploadMetadata(t *testing.T) {
	var reqNum int
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		body, err := io.ReadAll(r.Body)
		if err != nil {
			t.Error(err)
		}
		if !bytes.Contains(body, []byte("form-data; name=\"pinataMetadata\"\r\n\r\n{\"name\":\"upload.txt\"}\r\n")) {
			t.Errorf("expected uploaded body to contain pinataMetadata; it is: %s", body)
		}
		reqNum = 1
	}))
	defer server.Close()
	p := setupClient(t, WithEndpoint(server.URL))
	fh, err := os.Open("../testdata/upload.txt")
	if err != nil {
		t.Fatal(err)
	}
	defer fh.Close()

	_, _ = p.Upload(context.Background(), fh, &progress.Tracker{})
	if reqNum != 1 {
		t.Errorf("expected 1 request to be made; it wasn't")
	}
}

func TestUploadProgress(t *testing.T) {
	var progressBytes int64
	fileSize := int64(2459520)
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := io.ReadAll(r.Body)
		if err != nil {
			t.Error(err)
		}
	}))
	defer server.Close()
	p := setupClient(t, WithEndpoint(server.URL))

	fh, err := os.Open("../testdata/JSB_BWV1047_1.xml")
	if err != nil {
		t.Fatal(err)
	}
	defer fh.Close()
	var proFnCalled int
	pro := progress.NewTracker(func(b int64) {
		proFnCalled++
		if b < progressBytes {
			t.Errorf("expected b>=progressBytes; b = %d, progressBytes = %d", b, progressBytes)
		}
		if b > fileSize || b < 0 {
			t.Errorf("expected b>=fileSize, b >= 0; b = %d, fileSize = %d", b, fileSize)
		}
		progressBytes = b
	}, fileSize)
	_, _ = p.Upload(context.Background(), fh, pro)
	if err := pro.Close(); err != nil {
		t.Error(err)
	}
	if progressBytes != fileSize {
		t.Errorf("expected Progress to report %d bytes; got %d", fileSize, progressBytes)
	}
	if proFnCalled < 2 {
		t.Errorf("expected progress func to be called multiple times; got %d", proFnCalled)
	}
}

func TestUploadRequest(t *testing.T) {
	var reqNum int
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		reqNum += 1
		if r.URL.Path != "/pinning/pinFileToIPFS" {
			t.Errorf("expected path to be /pinning/pinFileToIPFS; got %s", r.URL.Path)
		}
		if r.Method != http.MethodPost {
			t.Errorf("expected method POST; got %s", r.Method)
		}
		if ct := r.Header.Get("Content-Type"); !strings.HasPrefix(ct, "multipart/form-data; boundary=") {
			t.Errorf("expected Content-Type of multipart/form-data; boundary=; got %s", ct)
		}
		if auth := r.Header.Get("Authorization"); auth != "Bearer sekrit" {
			t.Errorf("expected Authorization header to have JWT bearer; got %s", auth)
		}
		written, err := io.Copy(io.Discard, r.Body)
		if err != nil {
			t.Error(err)
		}
		const expectedBodySz = 2460084
		if written != expectedBodySz {
			t.Errorf("expected body of %d bytes; got %d", expectedBodySz, written)
		}
	}))
	defer server.Close()
	p := setupClient(t, WithEndpoint(server.URL))

	fh, err := os.Open("../testdata/JSB_BWV1047_1.xml")
	if err != nil {
		t.Fatal(err)
	}
	defer fh.Close()
	_, _ = p.Upload(context.Background(), fh, &progress.Tracker{})
	if reqNum != 1 {
		t.Errorf("expected to make 1 request; made %d", reqNum)
	}
}
