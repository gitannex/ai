//go:build e2e

package pinata

import (
	"context"
	"io"
	"os"
	"path/filepath"
	"testing"
	"time"

	"ai.camelcase.link/internal/progress"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/ipfs/go-cid"
)

func setupE2EClient(t *testing.T) *Client {
	t.Helper()
	jwt := os.Getenv("PINATA_TOK")
	if jwt == "" {
		t.Fatal("PINATA_TOK not set")
	}
	rh := retryablehttp.NewClient()
	cl, err := New(WithHTTPClient(rh.StandardClient()), WithToken(jwt))
	if err != nil {
		t.Fatal(err)
	}
	twoSecs := 2 * time.Second
	cl.rp.PollWait = &twoSecs
	return cl
}

func TestUpload(t *testing.T) {
	p := setupE2EClient(t)
	expectedCID, _ := cid.Decode("bafkreigoamtzilpekuolvght3fkaceorudpdrfsqkhnii7dv3coqxi6en4")
	fh, err := os.Open("../testdata/upload.txt")
	if err != nil {
		t.Fatal(err)
	}
	defer fh.Close()
	_ = p.Unpin(context.Background(), expectedCID)
	c, err := p.Upload(context.Background(), fh, &progress.Tracker{})
	if err != nil {
		t.Errorf("Upload: %v", err)
	}
	if !c.Equals(expectedCID) {
		t.Errorf("expected upload to have CID: %s; got %s", expectedCID, c)
	}
	if err := p.Unpin(context.Background(), c); err != nil {
		t.Errorf("expected to unpin %s without error; got %v", c, err)
	}
}

func TestUploadCanBeRetrieved(t *testing.T) {
	p := setupE2EClient(t)
	fh, err := os.Create(filepath.Join(t.TempDir(), "ucbr"))
	if err != nil {
		t.Fatal(err)
	}
	fileContents := time.Now().String()
	if _, err := fh.WriteString(fileContents); err != nil {
		t.Fatal(err)
	}
	if _, err := fh.Seek(0, 0); err != nil {
		t.Fatal(err)
	}
	defer fh.Close()

	c, err := p.Upload(context.Background(), fh, &progress.Tracker{})
	if err != nil {
		t.Errorf("Upload: %v", err)
	}
	t.Logf("Uploaded with CID: %s", c)
	res, err := p.ua.Get("https://gateway.ipfs.io/ipfs/" + c.String())
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()
	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}
	if string(body) != fileContents {
		t.Errorf("expected file fetched from gateway for CID %s to have contents '%s'; it had contents of '%s'", c, fileContents, body)
	}
	if err := p.Unpin(context.Background(), c); err != nil {
		t.Errorf("expected to unpin %s without error; got %v", c, err)
	}
}

func TestPinIsPresent(t *testing.T) {
	t.Parallel()
	// QmVmBnuqByeEqoLu1HkCn2zYeMZ2L3ooPDKyWV5pfaiydJ is an image from ipfs.io
	c, _ := cid.Decode("QmVmBnuqByeEqoLu1HkCn2zYeMZ2L3ooPDKyWV5pfaiydJ")
	p := setupE2EClient(t)
	defer p.Unpin(context.Background(), c)
	err := p.Unpin(context.Background(), c)
	if err != nil {
		t.Fatal(err)
	}
	var isPresentInitially bool
	for i := 0; i < 10; i++ {
		isPresentInitially, err = p.IsPresent(context.Background(), c)
		if err != nil {
			t.Fatal(err)
		}
		if !isPresentInitially {
			break
		}
		time.Sleep(time.Second)
	}
	if isPresentInitially {
		t.Fatalf("expected CID %s to be unpinned initially; it isn't", c)
	}
	if err := p.Pin(context.Background(), c, "annex-key"); err != nil {
		t.Fatalf("can't Pin %s: %v", c, err)
	}
	isPresent, err := p.IsPresent(context.Background(), c)
	if err != nil {
		t.Fatalf("IsPresent failed for %s: %v", c, err)
	}
	if !isPresent {
		t.Fatal("expected CID to be pinned; it isn't")
	}
	if err := p.Unpin(context.Background(), c); err != nil {
		t.Fatalf("can't Unpin %s: %v", c, err)
	}
}

func TestUnpinIsPresent(t *testing.T) {
	t.Parallel()
	// QmVmBnuqByeEqoLu1HkCn2zYeMZ2L3ooPDKyWV5pfaiydJ is an image from ipfs.io
	c, _ := cid.Decode("QmVmBnuqByeEqoLu1HkCn2zYeMZ2L3ooPDKyWV5pfaiydJ")
	p := setupE2EClient(t)
	defer p.Unpin(context.Background(), c)
	isPresent, err := p.IsPresent(context.Background(), c)
	if err != nil {
		t.Fatal(err)
	}
	if isPresent {
		t.Fatal("expected CID to be unpinned initially; it isn't")
	}
	if err := p.Unpin(context.Background(), c); err != nil {
		t.Fatal(err)
	}
	isPresent, err = p.IsPresent(context.Background(), c)
	if err != nil {
		t.Fatal(err)
	}
	if isPresent {
		t.Fatal("expected CID not to be pinned; it is")
	}
	if err := p.Pin(context.Background(), c, "key"); err != nil {
		t.Fatalf("can't Pin %s: %v", c, err)
	}
	if err := p.Unpin(context.Background(), c); err != nil {
		t.Fatalf("can't Unpin %s: %v", c, err)
	}
	for i := 0; i < 10; i++ {
		isPresent, err = p.IsPresent(context.Background(), c)
		if err != nil {
			t.Fatal(err)
		}
		if !isPresent {
			return
		}
		time.Sleep(time.Second)
	}
	if isPresent {
		t.Fatal("expected CID not to be pinned; it is")
	}
}
