package pinata

import (
	"context"
	"errors"
	"fmt"
	"io/fs"
	"net/http"

	"ai.camelcase.link"
	"ai.camelcase.link/internal/progress"

	"github.com/hashicorp/go-retryablehttp"
	"github.com/ipfs/go-cid"
)

type Client struct {
	rp *ai.RemotePinner
	config
}

type config struct {
	ua          *http.Client
	jwt         string
	endpoint    string
	psaEndpoint string
}

// NewOpt is an option for the New() function.
type NewOpt func(options *config) error

// WithToken is an option for the New() function which sets the JSON Web Token to authenticate with the endpoint.
func WithToken(tok string) NewOpt {
	return func(cfg *config) error {
		cfg.jwt = tok
		return nil
	}
}

// WithHTTPClient is an option for the New() function which sets the http.Client with which to access the endpoint. By default, a new retryablehttp.Client is used.
func WithHTTPClient(c *http.Client) NewOpt {
	return func(cfg *config) error {
		if c != nil {
			cfg.ua = c
		}
		return nil
	}
}

// DefaultEndpoint is the default base URL for the Pinata API. It can be overridden with the WithEndpoint option.
const DefaultEndpoint = "https://api.pinata.cloud"

// WithEndpoint is an option for the New() function which sets the URL of the pinata.cloud endpoint. By default, DefaultEndpoint is used.
func WithEndpoint(e string) NewOpt {
	return func(cfg *config) error {
		if e != "" {
			cfg.endpoint = e
		}
		return nil
	}
}

// DefaultPSAEndpoint is the default base URL for the Pinata Pinning Service API. It can be overridden with the WithPSAEndpoint option.
const DefaultPSAEndpoint = "https://api.pinata.cloud/psa"

// WithPSAEndpoint is an option for the New() function which sets the URL of the pinata.cloud Pinning Service API endpoint. By default, DefaultPSAEndpoint is used.
func WithPSAEndpoint(e string) NewOpt {
	return func(cfg *config) error {
		if e != "" {
			cfg.psaEndpoint = e
		}
		return nil
	}
}

// New creates a new Client with the given options. The only mandatory option is WithToken; the others have sensible defaults.
func New(opts ...NewOpt) (*Client, error) {
	cl := Client{config: config{
		endpoint:    DefaultEndpoint,
		psaEndpoint: DefaultPSAEndpoint,
		// TODO: We always overwrite this UA; should we construct it at a higher level?
		ua: retryablehttp.NewClient().StandardClient(),
	}}
	for _, opt := range opts {
		if err := opt(&cl.config); err != nil {
			return nil, err
		}
	}
	if cl.jwt == "" {
		return nil, errors.New("JWT not set")
	}
	cl.rp = ai.NewRemotePinner(cl.psaEndpoint, cl.jwt, cl.ua)
	return &cl, nil
}

func (cl *Client) Upload(ctx context.Context, fi fs.File, pro *progress.Tracker) (cid.Cid, error) {
	up := &uploader{
		jwt:      cl.jwt,
		ua:       cl.ua,
		endpoint: cl.endpoint,
	}
	c, err := up.Upload(ctx, fi, pro)
	if err != nil {
		return c, fmt.Errorf("Upload: %w", err)
	}
	return c, nil
}

func (cl *Client) IsPresent(ctx context.Context, c cid.Cid) (bool, error) {
	return cl.rp.IsPinned(ctx, c)
}

func (cl *Client) Unpin(ctx context.Context, c cid.Cid) error {
	return cl.rp.Unpin(ctx, c)
}

func (cl *Client) Pin(ctx context.Context, c cid.Cid, k string) error {
	return cl.rp.Pin(ctx, c, k)
}

func (cl *Client) Equal(other *Client) bool {
	if cl == nil || other == nil {
		return false
	}
	return cl.config == other.config
}
