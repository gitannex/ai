package pinata

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"strings"
	"time"

	"ai.camelcase.link/internal/multiconc"
	"ai.camelcase.link/internal/progress"
	"github.com/ipfs/go-cid"
)

// TODO: A lot of this code duplicates that in ../estuary/upload.go,
// intentionally. I've yet to merge it because I'm not sure
// exactly how to.

type uploader struct {
	mcr      multiconc.Reader
	ua       *http.Client
	endpoint string
	jwt      string

	pipeReader io.ReadCloser
}

// fileFieldName is the name of the field in the multipart request
// whose contents is the file to be uploaded
const fileFieldName = "file"

func (up *uploader) Upload(ctx context.Context, fi fs.File, pro *progress.Tracker) (cid.Cid, error) {
	stat, err := fi.Stat()
	if err != nil {
		return cid.Undef, fmt.Errorf("can't stat fi: %w", err)
	}
	if err := up.payloadIsSane(stat); err != nil {
		return cid.Undef, fmt.Errorf("payloadIsSane: %w", err)
	}
	up.mcr = multiconc.Reader{
		File:      fi,
		FileInfo:  stat,
		Progress:  pro,
		FileField: fileFieldName,
	}

	nff, err := up.nonFileFields()
	if err != nil {
		return cid.Undef, fmt.Errorf("nonFileFields: %w", err)
	}
	up.mcr.Fields = nff

	up.pipeReader = up.mcr.Start()
	// up.pipeReader.Close() has its error checked in
	// up.readResponse(); this defer is a failsafe
	defer up.pipeReader.Close()
	c, err := up.doUpload(ctx)
	if err != nil {
		return cid.Undef, fmt.Errorf("doUpload: %w", err)
	}
	return c, nil
}

const MaxFileSize = 1024 * 1024 * 1024 * 25

func (up *uploader) payloadIsSane(stat fs.FileInfo) error {
	if !stat.Mode().IsRegular() {
		return fmt.Errorf("only regular files can be uploaded (mode: %s)", stat.Mode())
	}
	if stat.Size() >= MaxFileSize {
		return fmt.Errorf("file size (%d bytes) of %s exceeds Pinata's limits", stat.Size(), stat.Name())
	}
	return nil
}

func (up *uploader) nonFileFields() (map[string]string, error) {
	opts, err := up.optionsJSON()
	if err != nil {
		return nil, fmt.Errorf("optionsJSON: %w", err)
	}
	met, err := up.metadataJSON()
	if err != nil {
		return nil, fmt.Errorf("metadataJSON: %w", err)
	}
	return map[string]string{
		"pinataOptions":  string(opts),
		"pinataMetadata": string(met),
	}, nil
}

type uploadOptions struct {
	CIDVersion        int  `json:"cidVersion"`
	WrapWithDirectory bool `json:"wrapWithDirectory"`
}

func (up *uploader) optionsJSON() ([]byte, error) {
	uOpts := uploadOptions{CIDVersion: 1, WrapWithDirectory: false}
	uOptsBs, err := json.Marshal(uOpts)
	if err != nil {
		return nil, fmt.Errorf("can't marshal JSON (%v): %w", uOpts, err)
	}
	return uOptsBs, nil
}

type uploadMetadata struct {
	Name string `json:"name"`
}

func (up *uploader) metadataJSON() ([]byte, error) {
	uMet := uploadMetadata{Name: up.mcr.FileInfo.Name()}
	uMetBs, err := json.Marshal(uMet)
	if err != nil {
		return nil, fmt.Errorf("can't marshal JSON (%v): %w", uMet, err)
	}
	return uMetBs, nil
}

func (up *uploader) doUpload(ctx context.Context) (cid.Cid, error) {
	req, err := up.buildRequest(ctx)
	if err != nil {
		return cid.Undef, fmt.Errorf("buildRequest: %w", err)
	}
	res, err := up.ua.Do(req)
	if err != nil {
		return cid.Undef, fmt.Errorf("can't make request: %w", err)
	}
	defer res.Body.Close()
	c, err := up.readResponse(res)
	if err != nil {
		return cid.Undef, fmt.Errorf("readResponse: %w", err)
	}
	return c, nil
}

func (up *uploader) endpointURL() string {
	return strings.TrimSuffix(up.endpoint, "/") + "/pinning/pinFileToIPFS"
}

func (up *uploader) buildRequest(ctx context.Context) (*http.Request, error) {
	req, err := http.NewRequestWithContext(ctx, "POST", up.endpointURL(), up.pipeReader)
	if err != nil {
		return nil, fmt.Errorf("can't construct HTTP request: %w", err)
	}
	req.Header.Set("Transfer-Encoding", "chunked")
	req.Header.Set("Content-Type", up.mcr.ContentType())
	req.Header.Set("Authorization", "Bearer "+up.jwt)
	req.Header.Set("Accept", "application/json")
	return req, nil
}

const maxResBodySz = 1024 * 1024

func (up *uploader) readResponse(res *http.Response) (cid.Cid, error) {
	body, err := io.ReadAll(&io.LimitedReader{N: maxResBodySz, R: res.Body})
	if err != nil {
		return cid.Undef, fmt.Errorf("io.ReadAll: can't read response body: %w", err)
	}
	if err := res.Body.Close(); err != nil {
		return cid.Undef, fmt.Errorf("can't close HTTP response body: %w", err)
	}
	if err := up.pipeReader.Close(); err != nil {
		return cid.Undef, fmt.Errorf("can't close multipart pipe reader: %w", err)
	}
	c, err := up.handleResponse(body, res)
	if err != nil {
		return cid.Undef, fmt.Errorf("handleResponse: %w", err)
	}
	return c, nil
}

func (up *uploader) handleResponse(body []byte, res *http.Response) (cid.Cid, error) {
	if res.StatusCode != http.StatusOK {
		// TODO: Check what kind of JSON Pinata returns on error
		return cid.Undef, fmt.Errorf("bad status (%s): %s", res.Status, body)
	}
	c, err := up.parseResponse(body)
	if err != nil {
		return cid.Undef, fmt.Errorf("parseResponse: %w", err)
	}
	return c, nil
}

type uploadResponse struct {
	IpfsHash  string
	PinSize   int64
	Timestamp time.Time
}

func (up *uploader) parseResponse(body []byte) (cid.Cid, error) {
	ur := new(uploadResponse)
	if err := json.Unmarshal(body, ur); err != nil {
		return cid.Undef, fmt.Errorf("can't unmarshal JSON (%s) from response: %w", body, err)
	}
	c, err := cid.Decode(ur.IpfsHash)
	if err != nil {
		return cid.Undef, fmt.Errorf("can't decode CID from %s: %w", body, err)
	}
	return c, nil
}
