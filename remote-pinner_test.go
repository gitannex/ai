package ai

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/ipfs/go-cid"
	pinclient "github.com/ipfs/go-pinning-service-http-client"
)

// TestIsPinned_1Result confirms that IsPinned returns true when the
// endpoint returns a single result
func TestIsPinned_1Result(t *testing.T) {
	t.Parallel()
	c, err := cid.Decode("QmTgUdFazfzRNF2gz5FPxBm2rQbohMXDiAdmhRxcezGs4m")
	if err != nil {
		t.Error(err)
	}
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/pins" || r.URL.Query().Get("cid") != c.String() || r.URL.Query().Get("status") != "pinned" || r.URL.Query().Get("limit") != "1" {
			t.Errorf("request had wrong URL: %s", r.URL.String())
		}
		if r.Header.Get("Authorization") != "Bearer jwt.token" {
			t.Errorf("Authorization header invalid: %s", r.Header.Get("Authorization"))
		}
		// Response based on example in https://ipfs.github.io/pinning-services-api-spec/#tag/pins/paths/~1pins/get
		w.Header().Add("Content-Type", "application/json")
		fmt.Fprint(w, `{
  "count": 1,
  "results": [
    {
      "requestid": "UniqueIdOfPinRequest",
      "status": "pinned",
      "created": "2020-07-27T17:32:28Z",
      "pin": {
        "cid": "`+c.String()+`",
        "name": "PreciousData.pdf",
        "origins": [
          "/ip4/203.0.113.142/tcp/4001/p2p/QmSourcePeerId",
          "/ip4/203.0.113.114/udp/4001/quic/p2p/QmSourcePeerId"
        ],
        "meta": {
          "app_id": "99986338-1113-4706-8302-4420da6158aa"
        }
      },
      "delegates": [
        "/ip4/203.0.113.1/tcp/4001/p2p/QmSV6ArV3eVLCBgdvURZShi85hHn21xau5CURMZGz6qb47"
      ]
    }
  ]
}`)
	}))
	pc := pinclient.NewClient(server.URL, "jwt.token")
	rp := RemotePinner{Client: pc}
	is, err := rp.IsPinned(context.Background(), c)
	if err != nil {
		t.Error(err)
	}
	if !is {
		t.Errorf("expected is to be true")
	}
}

// TestIsPinned_0Results confirms that IsPinned returns false when the
// endpoint returns 0 results
func TestIsPinned_0Results(t *testing.T) {
	t.Parallel()
	c, err := cid.Decode("QmTgUdFazfzRNF2gz5FPxBm2rQbohMXDiAdmhRxcezGs4m")
	if err != nil {
		t.Error(err)
	}
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/pins" || r.URL.Query().Get("cid") != c.String() || r.URL.Query().Get("status") != "pinned" || r.URL.Query().Get("limit") != "1" {
			t.Errorf("request had wrong URL: %s", r.URL.String())
		}
		if r.Header.Get("Authorization") != "Bearer jwt.token" {
			t.Errorf("Authorization header invalid: %s", r.Header.Get("Authorization"))
		}
		// Response based on example in https://ipfs.github.io/pinning-services-api-spec/#tag/pins/paths/~1pins/get
		w.Header().Add("Content-Type", "application/json")
		fmt.Fprint(w, `{
  "count": 0,
  "results": []
}`)
	}))
	pc := pinclient.NewClient(server.URL, "jwt.token")
	rp := RemotePinner{Client: pc}
	is, err := rp.IsPinned(context.Background(), c)
	if err != nil {
		t.Error(err)
	}
	if is {
		t.Errorf("expected is to be false")
	}
}

// TestPinNoPoll confirms that when the endpoint returns status=pinned or status=failed straight away, no polling is done
func TestPinNoPoll(t *testing.T) {
	c, err := cid.Decode("QmTgUdFazfzRNF2gz5FPxBm2rQbohMXDiAdmhRxcezGs4m")
	if err != nil {
		t.Error(err)
	}
	pinName := "SHA256E-s32121420--c5cdc7c10f7a3fa4ac038add74067f2dd3cafd72f9a944924224fe2749346f72.flac"
	for _, wantStatus := range []string{"pinned", "failed"} {
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.Method != "POST" {
				t.Errorf("bad method %s; expected POST", r.Method)
			}
			if r.URL.Path != "/pins" || len(r.URL.Query()) != 0 {
				t.Errorf("request had wrong URL: %s", r.URL.String())
			}
			if r.Header.Get("Authorization") != "Bearer jwt.token" {
				t.Errorf("Authorization header invalid: %s", r.Header.Get("Authorization"))
			}
			body, err := ioutil.ReadAll(r.Body)
			if err != nil {
				t.Errorf("got error when reading request body: %v", err)
				return
			}
			if !bytes.Contains(body, []byte(`"cid":"`+c.String()+`"`)) {
				t.Errorf("expected body to contain CID; got %s", body)
			}
			if !bytes.Contains(body, []byte(`"name":"`+pinName+`"`)) {
				t.Errorf("expected body to contain pin name; got %s", body)
			}
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusAccepted)

			fmt.Fprint(w, `{
  "requestid": "UniqueIdOfPinRequest",
  "status": "`+wantStatus+`",
  "created": "2020-07-27T17:32:28Z",
  "pin": {
    "cid": "`+c.String()+`",
    "name": "`+pinName+`",
    "origins": [
      "/ip4/203.0.113.142/tcp/4001/p2p/QmSourcePeerId",
      "/ip4/203.0.113.114/udp/4001/quic/p2p/QmSourcePeerId"
    ],
    "meta": {
      "app_id": "99986338-1113-4706-8302-4420da6158aa"
    }
  },
  "delegates": [
     "/ip4/203.0.113.1/tcp/4001/p2p/QmSV6ArV3eVLCBgdvURZShi85hHn21xau5CURMZGz6qb47"
  ]
}`)
		}))
		pc := pinclient.NewClient(server.URL, "jwt.token")
		rp := RemotePinner{Client: pc}
		err := rp.Pin(context.Background(), c, pinName)
		if wantStatus == "pinned" {
			if err != nil {
				t.Errorf("expected to pin without error for wantStatus == pinned; err = %#v", err)
			}
		}
		if wantStatus == "failed" {
			if err == nil {
				t.Errorf("expected to pin with error for wantStatus == failed")
			}
		}
	}
}

// TestPinPoll confirms that when the endpoint initially returns status=queued, status=pinning that we poll until the status changes to pinned or failed.
func TestPinPoll(t *testing.T) {
	c, err := cid.Decode("QmTgUdFazfzRNF2gz5FPxBm2rQbohMXDiAdmhRxcezGs4m")
	if err != nil {
		t.Error(err)
	}
	pinName := "SHA256E-s32121420--c5cdc7c10f7a3fa4ac038add74067f2dd3cafd72f9a944924224fe2749346f72.flac"
	wantRequestID := "UniqueIDOfRequest"
	type testCase struct {
		states []string
		ok     bool
	}
	for _, tc := range []testCase{
		{states: []string{"queued", "queued", "pinned"}, ok: true},
		{states: []string{"queued", "pinning", "pinned"}, ok: true},
		{states: []string{"pinning", "pinning", "queued", "pinning", "pinned"}, ok: true},
		{states: []string{"queued", "pinning", "queued", "pinned"}, ok: true},
		{states: []string{"pinning", "pinning", "failed"}, ok: false},
		{states: []string{"queued", "queued", "queued", "queued", "failed"}, ok: false},
	} {
		var requestNum int
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			defer func() {
				requestNum += 1
			}()
			if requestNum >= len(tc.states) {
				t.Errorf("unexpected request (%d); expected %d", requestNum, len(tc.states))
				return
			}
			if requestNum == 0 {
				if r.Method != "POST" {
					t.Errorf("bad method %s; expected POST", r.Method)
				}
				if r.URL.Path != "/pins" {
					t.Errorf("request had wrong URL: %s", r.URL.String())
				}
				if len(r.URL.Query()) != 0 {
					t.Errorf("request had wrong URL: %s", r.URL.String())
				}
			} else {
				if r.Method != "GET" {
					t.Errorf("bad method %s; expected GET", r.Method)
				}
				if r.URL.Path != "/pins/"+wantRequestID {
					t.Errorf("expected path of /pins/%s; got request URL of %s", wantRequestID, r.URL)
				}

			}
			if r.Header.Get("Authorization") != "Bearer jwt.token" {
				t.Errorf("Authorization header invalid: %s", r.Header.Get("Authorization"))
			}
			w.Header().Add("Content-Type", "application/json")
			if requestNum == 0 {
				w.WriteHeader(http.StatusAccepted)
			} else {
				w.WriteHeader(http.StatusOK)
			}

			fmt.Fprint(w, `{
  "requestid": "`+wantRequestID+`",
  "status": "`+tc.states[requestNum]+`",
  "created": "2020-07-27T17:32:28Z",
  "pin": {
    "cid": "`+c.String()+`",
    "name": "`+pinName+`",
    "origins": [
      "/ip4/203.0.113.142/tcp/4001/p2p/QmSourcePeerId",
      "/ip4/203.0.113.114/udp/4001/quic/p2p/QmSourcePeerId"
    ],
    "meta": {
      "app_id": "99986338-1113-4706-8302-4420da6158aa"
    }
  },
  "delegates": [
     "/ip4/203.0.113.1/tcp/4001/p2p/QmSV6ArV3eVLCBgdvURZShi85hHn21xau5CURMZGz6qb47"
  ]
}`)
		}))
		pc := pinclient.NewClient(server.URL, "jwt.token")
		rp := RemotePinner{Client: pc}
		nano := time.Nanosecond
		rp.PollWait = &nano
		err := rp.Pin(context.Background(), c, pinName)
		if tc.ok {
			if err != nil {
				t.Errorf("expected to pin without error for tc %#v; got err = %#v", tc, err)
			}
		} else {
			if err == nil {
				t.Errorf("expected to pin with error for tc %#v; got nil error", tc)
			}
		}
		if requestNum != len(tc.states) {
			t.Errorf("expected to make %d requests; made %d", len(tc.states), requestNum)
		}
	}
}

// TestPinCancel confirms that when the provided context is cancelled, the Pin method returns
func TestPinCancel(t *testing.T) {
	c, err := cid.Decode("QmTgUdFazfzRNF2gz5FPxBm2rQbohMXDiAdmhRxcezGs4m")
	if err != nil {
		t.Error(err)
	}
	pinName := "SHA256E-s32121420--c5cdc7c10f7a3fa4ac038add74067f2dd3cafd72f9a944924224fe2749346f72.flac"
	wantRequestID := "UniqueIDOfRequest"
	var requestNum int
	ctx, cancelFn := context.WithCancel(context.Background())
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if requestNum == 0 {
			if r.Method != "POST" {
				t.Errorf("bad method %s; expected POST", r.Method)
			}
			if r.URL.Path != "/pins" {
				t.Errorf("request had wrong URL: %s", r.URL.String())
			}
			if len(r.URL.Query()) != 0 {
				t.Errorf("request had wrong URL: %s", r.URL.String())
			}
		} else {
			if r.Method != "GET" {
				t.Errorf("bad method %s; expected GET", r.Method)
			}
			if r.URL.Path != "/pins/"+wantRequestID {
				t.Errorf("expected path of /pins/%s; got request URL of %s", wantRequestID, r.URL)
			}

		}
		if r.Header.Get("Authorization") != "Bearer jwt.token" {
			t.Errorf("Authorization header invalid: %s", r.Header.Get("Authorization"))
		}
		w.Header().Add("Content-Type", "application/json")
		if requestNum == 0 {
			w.WriteHeader(http.StatusAccepted)
		} else {
			w.WriteHeader(http.StatusOK)
		}

		fmt.Fprint(w, `{
  "requestid": "`+wantRequestID+`",
  "status": "pinning",
  "created": "2020-07-27T17:32:28Z",
  "pin": {
    "cid": "`+c.String()+`",
    "name": "`+pinName+`",
    "origins": [
      "/ip4/203.0.113.142/tcp/4001/p2p/QmSourcePeerId",
      "/ip4/203.0.113.114/udp/4001/quic/p2p/QmSourcePeerId"
    ],
    "meta": {
      "app_id": "99986338-1113-4706-8302-4420da6158aa"
    }
  },
  "delegates": [
     "/ip4/203.0.113.1/tcp/4001/p2p/QmSV6ArV3eVLCBgdvURZShi85hHn21xau5CURMZGz6qb47"
  ]
}`)
		if requestNum == 10 {
			cancelFn()
			return
		}
		requestNum += 1
	}))
	pc := pinclient.NewClient(server.URL, "jwt.token")
	rp := RemotePinner{Client: pc}
	nano := time.Nanosecond
	rp.PollWait = &nano

	err = rp.Pin(ctx, c, pinName)
	if err == nil {
		t.Errorf("expected non-nil error when ctx is cancelled; got %v", err)
	}
	if requestNum != 10 {
		t.Errorf("expected 10 requests to have been made; %d were made", requestNum)
	}
}

// TestUnpin exercises the Unpin method
func TestUnpin(t *testing.T) {
	c, err := cid.Decode("QmTgUdFazfzRNF2gz5FPxBm2rQbohMXDiAdmhRxcezGs4m")
	if err != nil {
		t.Error(err)
	}
	type testCase struct {
		states []string
	}
	for _, tc := range []testCase{
		{states: []string{"queued", "pinned", "queued", "pinned"}},
		{states: []string{"pinned", "pinned", "pinned"}},
		{states: []string{"pinning", "pinning", "queued", "pinning"}},
		{states: []string{"pinned"}},
		{states: []string{}},
	} {
		var requestNum int
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			defer func() {
				requestNum += 1
			}()
			if requestNum > (len(tc.states) + 1) {
				t.Errorf("unexpected request (%d); expected %d", requestNum, len(tc.states)+1)
				return
			}
			if requestNum == 0 {
				if r.URL.Path != "/pins" {
					t.Errorf("request had wrong URL: %s", r.URL.String())
				}
				if r.URL.Query().Get("cid") != c.String() {
					t.Errorf("request had wrong URL: %s", r.URL.String())
				}
			} else {
				if r.Method != "DELETE" {
					t.Errorf("bad method %s; expected DELETE", r.Method)
				}
				if expectedPath := fmt.Sprintf("/pins/req%d", requestNum); expectedPath != r.URL.Path {
					t.Errorf("expected path of %s; got request URL of %s", expectedPath, r.URL)
				}
			}
			if r.Header.Get("Authorization") != "Bearer jwt.token" {
				t.Errorf("Authorization header invalid: %s", r.Header.Get("Authorization"))
			}
			w.Header().Add("Content-Type", "application/json")
			if requestNum > 0 {
				w.WriteHeader(http.StatusAccepted)
				return
			}
			w.WriteHeader(http.StatusOK)
			fmt.Fprint(w, `{

    "count": `+fmt.Sprintf("%d", len(tc.states))+`,
    "results": 
[`)
			for i, state := range tc.states {
				fmt.Fprint(w, `{

    "requestid": "req`+fmt.Sprintf("%d", i+1)+`",
    "status": "`+state+`",
    "created": "2020-07-27T17:32:28Z",
    "pin": 

{

    "cid": "QmCIDToBePinned`+fmt.Sprintf("%d", i)+`",
    "name": "`+state+`",
    "origins": 

[

    "/ip4/203.0.113.142/tcp/4001/p2p/QmSourcePeerId",
    "/ip4/203.0.113.114/udp/4001/quic/p2p/QmSourcePeerId"

],
"meta": 

    {
        "app_id": "99986338-1113-4706-8302-4420da6158aa"
    }

},
"delegates": 
[
     "/ip4/203.0.113.1/tcp/4001/p2p/QmSV6ArV3eVLCBgdvURZShi85hHn21xau5CURMZGz6qb47"

],
"info": 

    {
        "status_details": "i = `+fmt.Sprintf("%d", i)+`"
    }

}`)
				if i != len(tc.states)-1 {
					fmt.Fprint(w, ",")
				}
			}
			fmt.Fprint(w, "]}")
		}))
		rp := NewRemotePinner(server.URL, "jwt.token", http.DefaultClient)
		err := rp.Unpin(context.Background(), c)
		if err != nil {
			t.Errorf("expected to unpin without error for tc %#v; got #%v", tc, err)
		}
		if requestNum != len(tc.states)+1 {
			t.Errorf("expected to make %d requests; made %d", len(tc.states)+1, requestNum)
		}
	}
}

func TestDeleteByID200(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != "DELETE" {
			t.Errorf("bad method %s; expected DELETE", r.Method)
		}
		if r.URL.Path != "/pins/requestID" {
			t.Errorf("request had wrong URL: %s", r.URL.String())
		}
		w.WriteHeader(http.StatusOK)
	}))
	defer server.Close()
	rp := NewRemotePinner(server.URL, "jwt.token", http.DefaultClient)
	err := rp.deleteByID(context.Background(), "requestID")
	if err != nil {
		t.Errorf("expected nil error; got %v", err)
	}
}

func TestDeleteByID400(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != "DELETE" {
			t.Errorf("bad method %s; expected DELETE", r.Method)
		}
		if r.URL.Path != "/pins/requestID" {
			t.Errorf("request had wrong URL: %s", r.URL.String())
		}
		w.WriteHeader(http.StatusBadRequest)
	}))
	defer server.Close()
	rp := NewRemotePinner(server.URL, "jwt.token", http.DefaultClient)
	err := rp.deleteByID(context.Background(), "requestID")
	ed, isErrD := err.(*errDelete)
	if !isErrD {
		t.Errorf("expected err to be errDelete; got %v", err)
	}
	if ed.httpStatus != 400 {
		t.Errorf("expected httpStatus = 400; got %d", ed.httpStatus)
	}
}

func TestDeleteByIDReason(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != "DELETE" {
			t.Errorf("bad method %s; expected DELETE", r.Method)
		}
		if r.URL.Path != "/pins/requestID" {
			t.Errorf("request had wrong URL: %s", r.URL.String())
		}

		w.Header().Add("Content-Type", "application/json; charset=utf-8")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{
"error": {
"reason": "ERR_CODE",
"details": "Can't"
}}`))
	}))
	defer server.Close()
	rp := NewRemotePinner(server.URL, "jwt.token", http.DefaultClient)
	err := rp.deleteByID(context.Background(), "requestID")
	ed, isErrD := err.(*errDelete)
	if !isErrD {
		t.Errorf("expected err to be errDelete; got %v", err)
	}
	if ed.httpStatus != 500 {
		t.Errorf("expected httpStatus = 400; got %d", ed.httpStatus)
	}
	if ed.Reason != "ERR_CODE" {
		t.Errorf("expected Reason to be ERR_CODE; got %s", ed.Reason)
	}
	if ed.Details != "Can't" {
		t.Errorf("expected Details to be Can't; got %s", ed.Details)
	}
}

func TestUnpinDelete404s(t *testing.T) {
	c, err := cid.Decode("QmTgUdFazfzRNF2gz5FPxBm2rQbohMXDiAdmhRxcezGs4m")
	if err != nil {
		t.Error(err)
	}
	wantRequestIDs := []string{"UniqueIDOfRequest", "UniqueIDOfRequest2"}
	var requestNum int
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			requestNum += 1
		}()
		if requestNum == 0 {
			if r.Method != "GET" {
				t.Errorf("bad method %s; expected GET", r.Method)
			}
			if r.URL.Path != "/pins" {
				t.Errorf("request had wrong URL: %s", r.URL.String())
			}
		} else {
			if r.Method != "DELETE" {
				t.Errorf("bad method %s; expected DELETE", r.Method)
			}
			if requestNum == 1 {
				if r.URL.Path != "/pins/"+wantRequestIDs[0] {
					t.Errorf("expected path of /pins/%s; got request URL of %s", wantRequestIDs[0], r.URL)
				}
			} else {
				if r.URL.Path != "/pins/"+wantRequestIDs[1] {
					t.Errorf("expected path of /pins/%s; got request URL of %s", wantRequestIDs[1], r.URL)
				}
			}
		}
		if r.Header.Get("Authorization") != "Bearer jwt.token" {
			t.Errorf("Authorization header invalid: %s", r.Header.Get("Authorization"))
		}
		w.Header().Add("Content-Type", "application/json")
		if requestNum == 0 {
			w.WriteHeader(http.StatusOK)
			listJSON := `{
  "count": 2,
  "results": [
    {
      "requestid": "` + wantRequestIDs[0] + `",
      "status": "pinned",
      "created": "2020-07-27T17:32:28Z",
      "pin": {
        "cid": "` + c.String() + `",
        "name": "PreciousData.pdf",
        "origins": [
          "/ip4/203.0.113.142/tcp/4001/p2p/QmSourcePeerId",
          "/ip4/203.0.113.114/udp/4001/quic/p2p/QmSourcePeerId"
        ],
        "meta": {
          "app_id": "99986338-1113-4706-8302-4420da6158aa"
        }
      },
      "delegates": [
        "/ip4/203.0.113.1/tcp/4001/p2p/QmSV6ArV3eVLCBgdvURZShi85hHn21xau5CURMZGz6qb47"
      ]
    },
    {
      "requestid": "` + wantRequestIDs[1] + `",
      "status": "pinned",
      "created": "2020-07-27T17:32:29Z",
      "pin": {
        "cid": "` + c.String() + `",
        "name": "PreciousData2.pdf",
        "origins": [
          "/ip4/203.0.113.142/tcp/4001/p2p/QmSourcePeerId",
          "/ip4/203.0.113.114/udp/4001/quic/p2p/QmSourcePeerId"
        ]
      },
      "delegates": [
        "/ip4/203.0.113.1/tcp/4001/p2p/QmSV6ArV3eVLCBgdvURZShi85hHn21xau5CURMZGz6qb47"
      ]
    }
  ]
}`
			if _, err := w.Write([]byte(listJSON)); err != nil {
				t.Fatal(err)
			}
			return
		} else if requestNum == 1 {
			w.WriteHeader(202)
			return
		} else {
			w.WriteHeader(http.StatusNotFound)
			return
		}
	}))
	defer server.Close()
	rp := NewRemotePinner(server.URL, "jwt.token", http.DefaultClient)
	err = rp.Unpin(context.Background(), c)
	if err != nil {
		t.Errorf("expected nil error; got %v", err)
	}
	if requestNum != 3 {
		t.Errorf("expected 3 requests to have been made; %d were made", requestNum)
	}
}
