package ai

import (
	"context"
	"io"

	"github.com/ipfs/go-cid"
	shell "github.com/ipfs/go-ipfs-api"
)

type LocalNode struct {
	*shell.Shell
}

func (ln *LocalNode) Get(ctx context.Context, c cid.Cid, pos int64) (io.ReadCloser, error) {
	resp, err := ln.Request("cat", c.String()).Option("offset", pos).Send(ctx)
	if err != nil {
		return nil, err
	}
	if resp.Error != nil {
		return nil, resp.Error
	}
	return resp.Output, nil
}

// IsTrusted: implements retrieve.IsTrusteder
func (ln *LocalNode) IsTrusted() bool { return true }

func (ln *LocalNode) String() string {
	return "LocalNode"
}
