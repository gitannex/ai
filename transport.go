package ai

import (
	"net"
	"net/http"
	"time"
)

type Transport struct {
	*http.Transport
}

func NewTransport() *Transport {
	ht := &http.Transport{
		DialContext: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
		}).DialContext,
		TLSHandshakeTimeout: 30 * time.Second,
		//	ResponseHeaderTimeout: 10 * time.Second, // TODO: Implement this for non-upload paths
		ExpectContinueTimeout: time.Second,
	}
	return &Transport{Transport: ht}
}
