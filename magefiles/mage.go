//go:build mage

package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/bitfield/script"
	"github.com/joho/godotenv"
	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
	"github.com/pantheon-systems/autotag"
)

func Test() error {
	return sh.RunWith(nil, "go", "test", "-race", "-shuffle", "on", "./...")
}

func E2E() error {
	err := godotenv.Load()
	if err != nil {
		return err
	}
	return sh.RunWith(map[string]string{}, "go", "test", "-race", "-timeout", "20m", "-v", "-tags=e2e", "-shuffle", "on", "./...")
}

func bumpVersion() error {
	wd, err := os.Getwd()
	if err != nil {
		return err
	}
	grc := autotag.GitRepoConfig{RepoPath: wd, Branch: "master", Prefix: true}
	gr, err := autotag.NewRepo(grc)
	if err != nil {
		return err
	}
	if err := gr.AutoTag(); err != nil {
		return err
	}
	log.Printf("latest version: %s", gr.LatestVersion())
	return nil
}

func modTidy() error {
	return sh.RunV("go", "mod", "tidy")
}

func gitRepoClean() error {
	out, err := sh.Output("git", "status", "--porcelain")
	if err == nil && out == "" {
		return nil
	}
	return fmt.Errorf("dirty wd: %s %w", out, err)
}

func gitPush() error {
	return sh.RunV("git", "push")
}

func makeBuild() error {
	return sh.RunV("mkdir", "-p", "build")
}

func CopyMan() error {
	mg.Deps(makeBuild)
	manSources, err := filepath.Glob("cmd/*/*.1")
	if err != nil {
		return err
	}
	for _, src := range manSources {
		if err := sh.Copy(filepath.Join("build", filepath.Base(src)), src); err != nil {
			return err
		}
	}
	return nil
}

func LintMan() error {
	mg.Deps(makeBuild)
	mg.Deps(CopyMan)
	msgs, err := script.ListFiles("build/*.1").
		ExecForEach("mandoc -Tlint {{.}}").
		Reject("STYLE: referenced manual not found: Xr ipfs 1").
		String()
	if err != nil {
		return err
	}
	if len(msgs) == 0 {
		return nil
	}
	return fmt.Errorf("LintMan: %s", msgs)
}

func CompressMan() error {
	mg.Deps(makeBuild)
	mg.Deps(LintMan)
	_, err := script.ListFiles("build/*.1").
		ExecForEach("gzip -k {{.}}").
		Bytes()
	return err
}

func HTMLMan() error {
	mg.Deps(makeBuild)
	mg.Deps(LintMan)
	manSources, err := filepath.Glob("build/*.1")
	if err != nil {
		return err
	}
	for _, src := range manSources {
		_, err := script.Exec("mandoc -Thtml " + src).
			WriteFile(filepath.Join("build", strings.TrimSuffix(filepath.Base(src), "1")+"html"))
		if err != nil {
			return err
		}
	}
	return nil
}

func Release() error {
	mg.Deps(modTidy)
	mg.Deps(gitRepoClean)
	mg.Deps(CompressMan)
	mg.Deps(HTMLMan)
	if err := bumpVersion(); err != nil {
		return err
	}
	if err := gitPush(); err != nil {
		return err
	}
	// TODO: delete this new tag if goreleaser fails
	return sh.RunV("dotenv", "goreleaser", "--rm-dist")
}
