package ai

import (
	"context"
	"fmt"
	"io"
	"net/http"

	"ai.camelcase.link/internal/retrieve"
	blocks "github.com/ipfs/go-block-format"
	"github.com/ipfs/go-cid"

	"github.com/multiformats/go-multihash"
)

type Gateway struct {
	URL GatewayURL
	UA  *http.Client
}

func (gw *Gateway) Get(ctx context.Context, c cid.Cid, pos int64) (io.ReadCloser, error) {
	if gw.UA == nil {
		gw.UA = http.DefaultClient
	}
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, gw.URL.Public(c), http.NoBody)
	if err != nil {
		return nil, fmt.Errorf("Get(%s): can't make HTTP request: %w", c, err)
	}
	if pos > 0 {
		req.Header.Set("Range", fmt.Sprintf("bytes=%d-", pos))
	}
	res, err := gw.UA.Do(req)
	if err != nil {
		return nil, fmt.Errorf("Get(%s): HTTP request failed: %w", c, err)
	}
	switch res.StatusCode {
	case http.StatusOK:
		if pos > 0 {
			_ = res.Body.Close()
			return nil, retrieve.ErrCantResume
		}
	case http.StatusRequestedRangeNotSatisfiable:
		_ = res.Body.Close()
		return nil, retrieve.ErrCantResume
	case http.StatusPartialContent:
		if pos == 0 {
			_ = res.Body.Close()
			return nil, fmt.Errorf("Get(%s): HTTP status %s, but pos == 0", c, res.Status)
		}
	default:
		_ = res.Body.Close()
		return nil, fmt.Errorf("Get(%s): HTTP status %d", c, res.StatusCode)
	}
	return res.Body, nil
}

func (gw *Gateway) GetBlock(ctx context.Context, c cid.Cid) (blocks.Block, error) {
	if gw.UA == nil {
		gw.UA = http.DefaultClient
	}
	req, err := http.NewRequestWithContext(ctx, "GET", gw.URL.Block(c), http.NoBody)
	if err != nil {
		return nil, err
	}
	res, err := gw.UA.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("bad status: %d", res.StatusCode)
	}
	bs, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	// NewBlockWithCid creates a block without verifying bs
	// matches c. This allows us to compare the CID supplied by
	// the caller with the actual CID of the block, even if the
	// two CIDs are of different versions
	bl, err := blocks.NewBlockWithCid(bs, c)
	if err != nil {
		return nil, err
	}

	m, err := multihash.Sum(bl.RawData(), bl.Cid().Prefix().MhType, -1)
	if err != nil {
		return nil, err
	}
	var gotCID cid.Cid
	if c.Version() == 0 {
		gotCID = cid.NewCidV0(m)
	} else if c.Version() == 1 {
		gotCID = cid.NewCidV1(c.Prefix().Codec, m)
	} else {
		return nil, fmt.Errorf("CID version %d is unsupported", c.Version())
	}
	if !c.Equals(gotCID) {
		return nil, fmt.Errorf("we requested %s, but returned block had CID %s", c, gotCID)
	}
	return bl, nil
}

func (gw *Gateway) String() string {
	return "Gateway[" + gw.URL.Base + "]"
}
